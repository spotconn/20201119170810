## Create a branch
    git checkout -b new-branch

## Push the code
    git push origin new-branch

## Delete the remote branch
    git push origin :new-branch

## Delete the branch
    git checkout master
    git branch -D new-branch