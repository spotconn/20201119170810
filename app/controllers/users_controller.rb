class UsersController < ApplicationController
  before_filter :authenticate_user!, :only => [:edit_photo, :update_photo, :edit, :update]

  # Filters
  before_filter :instruments, :only => [:edit, :update]

  # Define specifics layouts
  layout "modal", :only => [:details, :edit, :update, :edit_photo]
  #layout false, :only => [:status]

  #Cache
  #caches_page :index


  # Show the user details
  def details
    @user = User.find(params[:user_id])
  end

  def status
    render layout: false
  end

  def index
    @order_by_options = {'highlight'=>'users.internal_rating desc', 'sounds' => 'users.product_count desc', 'name'=>'users.name asc'}
    @order_by = params[:order_by] || 'highlight'
    @users = User.includes(:products).where(:products => { status: 'active', deleted_at: nil }).order(@order_by_options[@order_by])
    @page_title = "#{t(:page_title_prefix)} #{t(:author, :count => 2)} - #{t(:by)} #{t(@order_by.to_sym)}"
  end

  def show
    @user = User.find(params[:id])
    @similar_users = @user.similar_users(4)
    @bought_products = current_user ? current_user.bought_products : []
    @sort_by = params[:sort_by] || :created_at

    if current_user && current_user == @user
      @page_title = "#{t(:page_title_prefix)} #{t(:my_account)}"
      @products = @user.products.order(@sort_by).paginate(page: params[:products_page], per_page: 10)
      @collections = @user.collections.order('created_at desc')
      @orders = current_user.orders
        .joins(:product).includes(:product) # Use joins too to circumvents the Paranoia scope and include deleted products in the query.
        .order('orders.id desc')
        .paginate(page: params[:orders_page], per_page: 10)
    else
      @products = @user.products.is_public.order(@sort_by).paginate(page: params[:products_page], per_page: 10)
      raise AbstractController::ActionNotFound if @products.size==0 && !params[:preview]
      @title_page = params[:products_page] ? "p. #{params[:products_page]}" : ""
      @page_title = "#{t(:page_title_prefix)} #{@user.name} #{@title_page}"
    end

    # Set the breadcrumb
    add_breadcrumb_home
    add_breadcrumb @user.name, user_path(@user)

    @edit_profile = (@user.musician && @user.photo.blank? && @user==current_user) ? true : false

    @purchases_css_class = @user.is_musician? ? '' : 'active'
  end

  # Edit user informations
  def edit
    @user = current_user
    @page_title = "#{t(:page_title_prefix)} #{t(:edit_my_account)}"
  end

  def edit_password
    @user = current_user
    @page_title = "#{t(:page_title_prefix)} #{t(:change_my_password)}"
  end

  def edit_photo
    #render :layout => false
    @user = current_user
    @page_title = "#{t(:page_title_prefix)} #{t(:change_photo)}"
  end

  def update_photo
    @user = current_user
    @user.photo = params[:files].first
    @user.save
    if @user.save!(validate: false)
      render json: {result: true, photourl: @user.photo.url}
    else
      render json: {result: false}
    end
  end


  def update_password
    @user = current_user
    if @user.update_attributes(params[:user])
      # Sign in the user by passing validation in case his password changed
      sign_in @user, :bypass => true
      redirect_to user_path(@user)
    else
      render "edit_password"
    end
  end

  # Update user informations
  def update
    @user = current_user
    if @user.update_attributes(params[:user])
      if request.xhr?
        # Close the ajax modal area by javascript
        ajax_modal_close(user_url(@user))
      else
        redirect_to user_path(@user)
        # $('.blue_container h1').css('height')
      end
    else
      render :action => 'edit', :layout => false
    end
  end

  def set_currency
    cookies[:currency] = { :value => params[:currency], :expires => 1.year.from_now}
    render json: {result: true}
  end

  # Private methods
  private

  # Get list of available instruments
  def instruments
    @all_instruments = Category.all_instruments
  end
end
