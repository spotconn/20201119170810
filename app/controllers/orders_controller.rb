require 'o_ritmo/ordering'

class OrdersController < ApplicationController
  before_filter :authenticate_user!, :only => [:new, :create]
  before_filter :accepted_buyer_terms?, :only => [:new, :create]
  include ORitmo::OrderHelper
  
  def new
    @product = Product.find(params[:product_id])
    @user = current_user
    
    # check if user has enough balance and redirect if not
    remainder = @user.account.balance - @product.price
    redirect_to purchase_payment_path(@product.id) and return if remainder < 0
    
    @has_credit = @user.account.balance>0 ? true : false
    place_order if @product.price == 0
  end
  
  def create
    @product = Product.find(params[:product_id])
    
    if params[:cancel_button]
      flash[:error] = 'Order cancelled!'
      redirect_to user_product_path(@product.user, @product) and return
    end    
    
    # check if user has enough balance and redirect if not
    remainder = current_user.account.balance - @product.price
    redirect_to purchase_payment_path(-1*remainder, @product.id) and return if remainder < 0
    
    @has_credit = true
    
    place_order
  end
  
  def accepted_buyer_terms?
    if !current_user.accepted_buyer_terms?
      store_location()
      flash.keep
      redirect_to new_buyer_term_path
    end   
  end
  
end
