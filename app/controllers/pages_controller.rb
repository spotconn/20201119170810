class PagesController < ApplicationController

  #caches_page :index
  #caches_page :termos
  #caches_page :about_us

  def index
    @highlights = HomepageHighlight.where(section: 'MAIN').shuffle
    @spotlight_authors = HomepageHighlight.where(section: 'AUTHOR').shuffle[0..11] #.order(:position)
    @page_title = t(:page_title)
    @banner = Banner.first
  end

  def index_v1
    @products = Product.is_public
    @highlights = @products.where(highlight: true)
    @categories = Category.where(ancestry: nil)
    @spotlight_authors = User.featured.where("product_count > 0").sample(4)

    @moods = Category.find_by_name('mood')
    if !@moods.nil?
      @moods = @moods.children
      @product_by_mood = @moods.each_with_object({}) do |mood, hash|
        hash[mood] = mood.products.is_public.order("rand()").first
      end
      @product_by_mood = @product_by_mood.select { |t,p| p != nil }
      @product_by_mood = @product_by_mood.first(4)
    end

    @rhythms = Category.find_by_name('ritmo')
    if !@rhythms.nil?
      @rhythms = @rhythms.children
      @product_by_rhythm = @rhythms.each_with_object({}) do |type, hash|
        hash[type] = type.products.is_public.order("rand()").first
      end
      # alterado para que lista nao contenha produtos repetidos da lista anterior
      @product_by_rhythm = @product_by_rhythm.select { |r,p| p != nil && @product_by_mood.select{|r1,p1| p1==p}.length==0  }
      @product_by_rhythm = @product_by_rhythm.first(8)
    end
  end


  def termos
    @page_title = "#{t(:page_title_prefix)} #{t(:terms)}"
  end

  def about_us
    @page_title = "#{t(:page_title_prefix)} #{t(:about_us)}"
  end

  def faq
    @page_title = "#{t(:page_title_prefix)} FAQ"
  end

  def sitemap
    @categories = Category.all
    @users = User.artists
    @products = Product.is_public
  end
end
