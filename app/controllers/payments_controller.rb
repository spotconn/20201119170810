require 'o_ritmo/ordering'

class PaymentsController < ApplicationController
  include ActionView::Helpers::NumberHelper
  include ProductsHelper
  include ApplicationHelper
  include ORitmo::OrderHelper
  
  def new
    @payment = current_user.payments.new
    @credit_amounts = [100, 250, 500]
  end
  
  def purchase
    self.new
    @product = Product.find(params[:product_id])
    @surcharge = Settings.ordering.buynow_surcharge unless @product.price==0
    @subtotal = @product.price + @surcharge
  end
  
  def create
    @payment = current_user.payments.new(params[:payment])
    @product = Product.find(@payment.product_id)
    @payment.status = "N"
    
    if @payment.mode=="purchase"
      surcharge = @product.price>0 ? Settings.ordering.buynow_surcharge.to_f : 0
      @payment.amount = @product.price+surcharge
      name = @product.name
      description = @product.user.name
      
      items = [{:name => name, :number => 1, :quantity => 1, :amount => @product.price*100, :description => description, :category => "Digital" }]
      items << {:name => "Surcharge", :number => 2, :quantity => 1, :amount => surcharge*100, :description => "Single purchase surcharge", :category => "Digital" } if surcharge > 0 
    else
      surcharge = 0
      name = "ORitmo credits"
      description = ""
      items = [{:name => name, :number => 1, :quantity => 1, :amount => @payment.amount*100, :description => description, :category => "Digital" }]
    end
    
    if @payment.save
      
      setup_response = gateway.setup_purchase(@payment.amount*100,
       :ip                => request.remote_ip,
       :allow_guest_checkout => true, 
       :brand_name        => "ORitmo",
       :header_image      => "#{request.protocol}#{request.host_with_port}/assets/oritmo_logo.png",
       :items             => items,
       :subtotal          => @payment.amount*100,
       :shipping          => 0,
       :handling          => 0,
       :tax               => 0,
       :no_shipping       => 1,
       :allow_note        => 0,
       :order_id          => @payment.id,
       :email             => current_user.email,
       :return_url        => confirm_payment_url(@payment, :product_id => @payment.product_id, :mode => @payment.mode ), #url_for(:action => 'confirm', :only_path => false),
       :cancel_return_url => cancel_payment_url(@payment, :product_id => @payment.product_id ) #url_for(:action => 'index', :only_path => false)
      )
      
      redirect_to gateway.redirect_url_for(setup_response.token) and return
 
    end
  end
  
  def cancel
    @payment = current_user.payments.find(params[:id])
    @payment.message = "Cancelled by user"
    @payment.status = "C"
    @payment.amount_paid = 0
    @payment.save!
    
    @product = Product.find(params[:product_id])
    flash[:alert]='Pagamento cancelado'
    redirect_to user_product_path(@product.user, @product)
  end

  def confirm
    
    redirect_to :action => 'index' unless params[:token]
  
    details_response = gateway.details_for(params[:token])
  
    if !details_response.success?
      @message = details_response.message
      render :action => 'error'
      return
    end
    
    @payment = current_user.payments.find(params[:id])
    
    purchase = gateway.purchase(@payment.amount*100,
      :ip       => request.remote_ip,
      :payer_id => params[:PayerID],
      :token    => params[:token]
    )
    
    #require 'pp'
    #pp purchase  
    
    if !purchase.success?
      @message = purchase.message
      @payment.message = purchase.message
      @payment.status = "D"
      @payment.amount_paid = 0
      @payment.save!
      
      render :action => 'error'
      return
    end
    
    @payment.status = "A"
    @payment.amount_paid = purchase.params['PaymentInfo']['GrossAmount']
    @payment.fee = purchase.params['PaymentInfo']['FeeAmount']
    @payment.merchant_transaction_id = purchase.authorization
    @payment.merchant_data = purchase.params
    @payment.save!
    
    #send payment confirmation email
    @payment.send_emails
    
    @purchase = purchase
    
    if params[:product_id].blank?
      flash[:notice]='Credit added'
      redirect_to user_path(current_user)
    else
      @product = Product.find(params[:product_id])
      if params[:mode] == 'purchase'
        @has_credit = false
        place_order
      else
        redirect_to new_product_order_path(@product) 
      end
    end
    
  end
  
  def withdraw
    if current_user.paypal_account.blank?
      @message = t(:paypal_account_not_registered)
      render :action => 'error'
      return
    end
    
    if current_user.account.balance <=0
      @message = "You don't have any balance to withdraw"
      render :action => 'error'
      return
    end
    
    @payment = current_user.payments.new(params[:payment])
    @payment.status = "N"
    @payment.amount = current_user.account.balance
    if @payment.save!
      transfer_response = gateway.transfer @payment.amount*100, current_user.paypal_account, :subject => 'O Ritmo Withdrawal', :note => 'Thank you for your business'
      
      @payment.merchant_transaction_id = transfer_response.params['correlation_id']
      @payment.merchant_data = transfer_response.params
      
        @payment.status='A'
      if transfer_response.success?
        @payment.amount_paid = -1*@payment.amount
        @payment.save
        flash[:notice] = 'transaction success'
      else
        @payment.status='D'
        @payment.save
        @message = "Withdrawal transaction was not accepted by paypal. #{transfer_response.message}"
        render :action => 'error'
        return
      end
      
      redirect_to user_path(current_user)
      
    end
    
  end

=begin  
  def exchange_rate
    @currency ||= currency_gateway.convert_currency(
      :currency_list => [{:amount => 1.00, :code => 'USD'}],
      :to_currencies => {:code => 'BRL'}
    )
    @image = "#{request.protocol}#{request.host_with_port}/assets/oritmo_logo.png"
    begin
      @exchange_rate ||= @currency.estimated_amount_table.currency_conversion_list[0].currency_list.currency[0].amount
    rescue
    end
  end
  
  def currency_gateway
    @currency_gateway ||=  ActiveMerchant::Billing::PaypalAdaptivePayment.new(PAYPAL_OPTIONS)
  end
=end

    
  def gateway
     @gateway ||= ActiveMerchant::Billing::PaypalExpressGateway.new(PAYPAL_OPTIONS)
     #@gateway ||= ActiveMerchant::Billing::PaypalDigitalGoodsGateway.new(PAYPAL_OPTIONS)
  end
end
