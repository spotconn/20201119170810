class RegistrationsController < Devise::RegistrationsController
  def update
    @user = User.find(current_user.id)

    params[:user].delete(:current_password)
    successfully_updated =   @user.update_without_password(params[:user])

    if successfully_updated
      if request.xhr?
        # Close the ajax modal area by javascript
        ajax_modal_close(user_url(@user))
      else
        redirect_to user_path(@user)
        # $('.blue_container h1').css('height')
      end
    else
      if request.xhr?
        render :action => 'edit', :layout => false
      else
        render :action => 'edit'
      end
    end

  end

end