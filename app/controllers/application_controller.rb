require "o_ritmo/sections/static_section"

class ApplicationController < ActionController::Base
  # Protect from forgery access
  protect_from_forgery

  # Filters
  before_filter :set_locale_from_url
  before_filter :set_locale
  #before_filter :load_sections

  # Default layout
  layout :layout_by_resource

  # Protected methods
  protected

  # Redirect to after user sign in
  def after_sign_in_path_for(resource)
    return request.env['omniauth.origin'] || stored_location_for(resource) || user_path(current_user)
  end

  # Add breadcrumb for home page
  def add_breadcrumb_home
    # Set default breadcrumb
    add_breadcrumb I18n.t(:breadcrumb_home), :root_path    
  end

  # Close the current modal
  def ajax_modal_close(url = nil)
    if url
      render :js => "$('#ajax-modal').modal('hide');window.location.href='#{url}';"
    else
      render :js => "$('#ajax-modal').modal('hide');window.location.reload();"
    end
  end
  
  def layout_by_resource
    if devise_controller? && devise_mapping.name == :admin_user
      "admin/layouts/admin"
    else
      "application"
    end
  end
  
  #def load_sections
  #  #@sections = Category.menu_sections #Category.roots.arrange(:order => :sort_by).to_a.flatten.delete_if(&:blank?)
  #  o_ritmo = ORitmo::Sections::StaticSection.new("ORitmo")
  #  o_ritmo.children << ORitmo::Sections::StaticCategory.new('Home', root_path)
  #  o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:about_us), about_us_path) 
  #  o_ritmo.children << ORitmo::Sections::StaticCategory.new("Brazilian sounds", "http://braziliansounds.com") 
  #  o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:sign_up), new_registration_path('user'))
  #  o_ritmo.children << ORitmo::Sections::StaticCategory.new("FAQ", faq_path) 
  #  #o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:how_to_make_money), "/how_do_you_make_money")
  #  #o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:forum), "/forum")
  #  #o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:top_authors), "/top_authors")   
  #  
  #  @footer_sections = [o_ritmo] #+ @sections
  #  
  #  my_o_ritmo = ORitmo::Sections::StaticSection.new(t(:my_oritmo))
  #  if user_signed_in?
  #    my_o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:my_profile), user_path(current_user) )
  #    #my_o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:my_earnings), "/my_earnings")
  #    my_o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:upload_your_sound), "/upload")
  #    #my_o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:make_deposit), "/make_deposit")
  #    #my_o_ritmo.children << ORitmo::Sections::StaticCategory.new(t(:my_downloads), "/my_downloads")
  #  end
  #  
  #  @footer_sections << my_o_ritmo
  #end
  
  unless Rails.application.config.consider_all_requests_local
    rescue_from Exception, with: :render_500
    rescue_from ActionController::RoutingError, with: :render_404
    rescue_from ActionController::UnknownController, with: :render_404
    rescue_from ActionController::UnknownAction, with: :render_404
    rescue_from ActiveRecord::RecordNotFound, with: :render_404
  end
  
  # Private methods
  private

  def render_404(exception)
    @not_found_path = exception.message
    respond_to do |format|
      format.html { render template: 'errors/error_404', layout: 'layouts/application', status: 404 }
      format.all { render nothing: true, status: 404 }
    end
  end

  def render_500(exception)
    @error = exception
    
    ExceptionNotifier::Notifier
      .exception_notification(request.env, exception)
      .deliver
      
    respond_to do |format|
      format.html { render template: 'errors/error_500', layout: 'layouts/application', status: 500 }
      format.all { render nothing: true, status: 500}
    end
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    clear_return_to
  end

  def store_location
    session[:return_to] = request.fullpath
  end

  def clear_return_to
    session[:return_to] = nil
  end

  def extract_locale_from_path
    parsed_locale = request.path.split("/")[1]
    I18n.available_locales.include?(parsed_locale.to_sym) ? parsed_locale  : nil if parsed_locale
  end
  
  def set_locale
    I18n.locale = extract_locale_from_path || 'pt' #((lang = request.env['HTTP_ACCEPT_LANGUAGE']) && lang[/^[a-z]{2}/])
  end
  
end
