class BuyerTermsController < ApplicationController
  before_filter :authenticate_user!
  
  def new
    
  end
  
  def create
    
    if params[:decline_button]
      flash[:error] = 'Buyer terms declined'
      redirect_to :back and return
    end 
    
    current_user.accept_buyer_terms!
    redirect_back_or(user_path(current_user))
  end
end
