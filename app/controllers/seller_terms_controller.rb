class SellerTermsController < ApplicationController
  before_filter :authenticate_user!
  
  def new
    
  end
  
  def create
    current_user.accept_seller_terms!
    redirect_back_or(user_path(current_user))
  end
end
