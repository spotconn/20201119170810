class MultipleProductsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :create
  before_filter :authenticate_user!, :product_ids
  
  def index
    
    # multiple upload doesnt work on crappy browsers
    user_agent = UserAgent.parse(request.user_agent)
    if user_agent.browser == "Internet Explorer"
      redirect_to new_product_path
    end
    
    session["product_ids"] = []
    @product = Product.new
  end
  
  def edit
    @product = Product.find(params[:id])
  end
  
  def update
    @product = Product.find(params[:id])
    
    if @product.update_attributes(params[:product])
      session["product_ids"].shift
      if params["commit"] == "Finalizar"
        redirect_to user_path(current_user)
      else
        redirect_to edit_multiple_product_path(session["product_ids"].first)
      end
    else
      render :edit
    end
  end
  
  def create
    mp = MultipleUpload.new(current_user, params)
    if mp.save
      session["product_ids"] << mp.product.id
      render json: mp.to_json
    else  
      #render :nothing => true, status: 500
      render json: mp.to_json
    end
  end
  
  protected
  def product_ids
    session["product_ids"] ||= []
  end
end
