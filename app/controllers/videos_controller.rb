class VideosController < ApplicationController
  # Filters
  before_filter :user

  # Define specifics layouts
  layout "modal"

  # Show video informations
  def show
    @video = @user.videos.find(params[:id])
  end

  # The new video form
  def new
    @video = current_user.videos.new
  end
  
  # Create video
  def create
    @video = current_user.videos.new(params[:video])
  
    if @video.save
      flash[:notice] = t(:video_added)
      # Close the ajax modal area by javascript
      ajax_modal_close
    else
      render :new, :layout => false
    end
  end
  
  def destroy
    @video = current_user.videos.find(params[:id])
    @video.destroy
    render :json => true
  end

  # Private methods
  private

  # Get user's informations
  def user
    @user = User.find(params[:user_id])
  end
end
