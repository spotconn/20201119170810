class CollectionsController < ApplicationController
  before_filter :authenticate_user!, :except => [:show, :details]
  layout 'modal', only: [:details]

  def show
    @collection      = Collection.find(params[:id])
    @user            = @collection.user
    @similar_users   = @user.similar_users(4)
    @bought_products = current_user ? current_user.bought_products : []

    @products = @collection.products.is_public.order(:created_at)
    @title_page = params[:products_page] ? "p. #{params[:products_page]}" : ""
    @page_title = "#{t(:page_title_prefix)} #{@user.name} #{@title_page}"

    # Set the breadcrumb
    add_breadcrumb_home
    add_breadcrumb @user.name, user_path(@user)

    @edit_profile = (@user.musician && @user.photo.blank? && @user==current_user) ? true : false

    @purchases_css_class = @user.is_musician? ? '' : 'active'
  end

  def new
    @collection = current_user.collections.new
    # @categories = ancestry_options(Category.scoped.arrange(:order => 'name')) {|i| "#{'-' * i.depth} #{i.name}" }
    # @preset_images = Image.all
  end

  def create
    @collection = current_user.collections.new(params[:collection])

    if @collection.save
      respond_to do |format|
        format.html do
          flash[:notice]= t(:collection_added)
          redirect_to user_path(current_user, anchor: 'collections')
        end

        format.json do
          render :json => @collection
        end
      end
    else
      respond_to do |format|
        format.html do
          # @preset_images = Image.all
          render :new
        end

        format.json do
          render :nothing => true, status: 500
        end
      end
    end
  end

  def edit
    @collection = current_user.collections.find(params[:id])
    # @categories = ancestry_options(Category.scoped.arrange(:order => 'name')) {|i| "#{'-' * i.depth} #{i.name}" }
    # @preset_images = Image.all
    # @sections = Category.menu_sections
  end

  # Update the product information
  def update
    @collection = current_user.collections.find(params[:id])
    # @position = params[:position] if request.xhr?
    # @preset_images = Image.all if request.xhr?
    # @sections = Category.menu_sections

    if @collection.update_attributes(params[:collection])
      respond_to do |format|
        format.html { redirect_to user_path(current_user, anchor: 'collections') }
        format.js { render :action => 'update_success', :layout => false,  :success => "success", :status_code => "200" }
      end
    else
      respond_to do |format|
        format.html do
          # @preset_images = Image.all
          render :action => 'edit'
        end
        format.js { render :action => 'update_error', :layout => false,  :success => "success", :status_code => "200" }
      end
    end
  end

  def destroy
    @collection = current_user.collections.find(params[:id])
    @collection.destroy
    render :json => true
  end

  # Show the user details
  def details
    @collection = Collection.find(params[:collection_id])
  end
end
