class CategoriesController < ApplicationController
  # List the children elements of father element
  
  #caches_page :all
  
  def index
    @category = category
    @children = @category.children.
      order('product_count desc').
      page(params[:page])
      
    @title_page = params[:page] ? "p. #{params[:page]}" : ""
    if @children.size == 0
      @page_title = "#{t(:page_title_prefix)} #{@category.parent.name} - #{@category.name} #{@title_page}"
      show
      render :show 
    else
      @category_name = @category.name==t(:region).capitalize ? t(:regions) : @category.name.pluralize
      @page_title = "#{t(:page_title_prefix)} #{@category_name.capitalize}"
    end
  end

  def show
    @category = category #Category.find(params[:id])
    @sort_by = params[:sort_by] || :created_at
    @products = @category.products.is_public.order(@sort_by).paginate(page: params[:page], per_page: 24)
    @top_authors = User.where("exists (\
      select 1 from products a \
      join categories_products b on a.id = b.product_id \
      where a.user_id = users.id \
      and a.status=\'active\' \
      and b.category_id=#{@category.id}) \
      ").order('internal_rating desc')[0..3]
  end
  
  protected
  
    def category
      @category ||= Category.find(slug) 
    end
  
    def slug
      @slug ||= params[:categories].split('/').last.gsub(".json", "")
    end
  
end
