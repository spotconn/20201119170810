class ProductsController < ApplicationController
  before_filter :authenticate_user!, :only => [:new, :create, :process, :form, :upload, :upload_callback]
  before_filter :accepted_seller_terms?, :only => [:new, :create]

  cache_sweeper :product_sweeper

  # Returns the file form for the product
  def form
    # Only accept xhr requests
    if request.xhr?
      @position = params[:position]
      @product = Product.find(params[:product_id])
      @preset_images = Image.all
      @sections = Category.menu_sections
      render :form, :layout => false
    else
      redirect_to root_path
    end
  end

  def index
    @products = Product.is_public
    if params[:q]
      @products = @products.search { fulltext params[:q] }.results
    end
    if request.xhr?
      render :json => @products.collect { |p| p.to_jq_upload }.to_json
    end
  end

  def show
    @product = Product.find(params[:id])
    @user = @product.user
    @categories = Category.sort_by_ancestry(@product.categories)
    # @categories = @product.categories.subtree.all(:order => :name)
    @page_title = "#{t(:page_title_prefix)} #{@user.name} - #{@product.name}"
    @similar_user = @user.similar_user

    if !@product.archive_data.nil?
      @archive_data = ActiveSupport::JSON.decode @product.archive_data
    end

    begin
      @purchased = current_user.bought_products.find(@product.id) unless current_user.nil?
    rescue ActiveRecord::RecordNotFound => e
      @purchased = nil
    end
  end

  def new
    @product = current_user.products.new
    @categories = ancestry_options(Category.scoped.arrange(:order => 'name')) {|i| "#{'-' * i.depth} #{i.name}" }
    @preset_images = Image.all
  end

  def create
    @product = current_user.products.new(params[:product])

    if @product.save
      respond_to do |format|
        format.html do
          flash[:notice]= t(:product_added)
          redirect_to user_path(current_user)
        end

        format.json do
          render :json => @product
        end
      end
    else
      respond_to do |format|
        format.html do
          @preset_images = Image.all
          render :new
        end

        format.json do
          render :nothing => true, status: 500
        end
      end
    end

  end

  def edit
    @product = current_user.products.find(params[:id])
    @categories = ancestry_options(Category.scoped.arrange(:order => 'name')) {|i| "#{'-' * i.depth} #{i.name}" }
    @preset_images = Image.all
    @sections = Category.menu_sections
  end

  # Update the product information
  def update
    @product = current_user.products.find(params[:id])
    @position = params[:position] if request.xhr?
    @preset_images = Image.all if request.xhr?
    @sections = Category.menu_sections

    if @product.update_attributes(params[:product])
      respond_to do |format|
        format.html { redirect_to user_path(current_user) }
        format.js { render :action => 'update_success', :layout => false,  :success => "success", :status_code => "200" }
      end
    else
      respond_to do |format|
        format.html do
          @preset_images = Image.all
          render :action => 'edit'
        end
        format.js { render :action => 'update_error', :layout => false,  :success => "success", :status_code => "200" }
      end
    end
  end

  def accepted_seller_terms?
    if !current_user.accepted_seller_terms?
      store_location()
      redirect_to new_seller_term_path
    end
  end

  def destroy
    @product = current_user.products.find(params[:id])
    sample = @product.get_sample
    File.delete(@product.sample_path) if sample
    @product.remove_archive
    @product.remove_photo
    @product.destroy
    render :json => true
  end

  def ancestry_options(items, &block)
    return ancestry_options(items){ |i| "#{'-' * i.depth} #{i.name}" } unless block_given?

    result = []
    items.map do |item, sub_items|
      result << [yield(item), item.id]
      #this is a recursive call:
      result += ancestry_options(sub_items, &block)
    end
    result
  end

  def create_sample
    @product = current_user.products.find(params[:id])
    if @product.status.nil?
      @product.create_sample
      p "sample create called"
      redirect_to user_product_path(@product.user, @product)
      return
    end
    p "sample create NOT called"
    redirect_to user_product_path(@product.user, @product)
  end

  def download
    product = current_user.bought_products.find(params[:id])
    ext = File.extname(product.archive.file.path)
    filename = product.name
    filename += ext unless filename.end_with? ext

    if ::Rails.env.production?
      redirect_to product.archive_url(:query => {"response-content-disposition" => "attachment; filename=\"#{filename}\""})
    else
      send_file product.archive.path, filename: filename
    end
  end

  def license
    @product = current_user.bought_products.find(params[:id])
    @order = current_user.orders.where(product_id: @product.id)[0]

    if @order.allow_commercial_use
      @template = "license.regular"
    else
      @template = "license.comercial"
    end

    respond_to do |format|
      format.txt do
        response.headers['Content-Type'] = 'text/plain'
        response.headers['Content-Disposition'] = "attachment; filename=oritmo_license_#{@product.slug}.txt"
      end
    end
  end

  def upload
    # multiple upload doesnt work on crappy browsers
    user_agent = UserAgent.parse(request.user_agent)
    if user_agent.browser == "Internet Explorer"
      redirect_to new_product_path
    end

    session["product_ids"] = []
    @product = Product.new
    @page_title = "#{t(:page_title_prefix)} #{t(:upload_your_sound)}"
  end

  def upload_callback
    # move temporary file to definitive location
    filekey = Product.correct_s3_key(params[:filepath])
    file = Product.move_uploaded_file(filekey)
    fname = file[:original_filename]

    # save product
    product = Product.new
    product[:archive] = file[:filename]
    product.name = fname[/.*(?=\..+$)/]
    product.user = current_user
    product.price = 0
    product.status = 'new'
    product.send :set_slug
    if product.save!(validate: false)
      render :json => ["filename" => file[:filename], "product_id" => product.id, "unique_id" => params[:unique_id]].to_json
    else
      render :json => ["error" => product.errors].to_json
    end

  end

  private
    def x_accel_url(url, filename = nil)
      uri = "/internal_redirect/#{url.gsub(/https?:\/\//, '')}"
      #uri << "?#{filename}" if filename
      return uri
    end
end
