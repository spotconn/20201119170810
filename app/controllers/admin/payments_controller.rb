class Admin::PaymentsController < Admin::ApplicationController
  def index
    @statuses = Payment.find_by_sql('SELECT status, count(*) AS status_count FROM payments WHERE deleted_at IS NULL GROUP BY status UNION SELECT "all", count(*) FROM payments WHERE deleted_at IS NULL')
    @status = params[:status] || 'all'
    Payment.per_page = 15
    @payments = Payment.includes(:user).
      order("#{sort_column} #{sort_direction}").
      page(params[:page])
    unless @status == 'all'
      @payments = @payments.where(status: @status)
    end

    from = (params[:from] || 1.month.ago).to_date.beginning_of_day
    to = (params[:to] || Date.today).to_date.end_of_day

    @payments = @payments.where(created_at: (from..to))
  end
end
