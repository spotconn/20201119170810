class Admin::ApplicationController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_filter :authenticate_admin_user!
  layout "admin/layouts/admin"
  #skip_before_filter :load_sections
  
  
  
  def sort_column
    #Product.column_names.include?(params[:sort]) ? params[:sort] : "id"
    @sort_column ||= params[:sort] || "id"
  end
  
  def sort_direction
    @sort_direction ||= %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end

  def param_to_date(param, name)
    Date.new(*(1..3).map { |i| param["#{name}(#{i}i)"].to_i })
  end
  
  def become
    return unless current_admin_user
    @user = User.find(params[:id])
    @user.impersonate!
    sign_in(:user, @user, :bypass => true)
    redirect_to root_url # or user_root_url
  end
  
end
