class Admin::UsersController < Admin::ApplicationController

  def index
    User.per_page = 15
    @user_count = User.count
    @filters = ['All', 'Artists', 'Buyers']
    @filtered = params['filter'] || 'All'
    if params[:q]
      @filtered='All'
      @users = User.search do
        fulltext params[:q]
        paginate page: params[:page], per_page: User.per_page
      end.results
    else
      case @filtered
        when 'Artists'
          then @users = User.artists.page(params[:page]).order(sort_column + " " + sort_direction)
        when 'Buyers'
          then @users = User.buyers.page(params[:page]).order(sort_column + " " + sort_direction)
        else
          @users = User.page(params[:page]).order(sort_column + " " + sort_direction)
      end
    end

  end

  def show
    @user = User.find(params[:id])
    @products = @user.products.order(sort_column + " " + sort_direction)
    @bought_products = @user.bought_products
    @transactions = @user.account.transactions
  end

  def edit
    @user = User.find(params[:id])
  end

  # Update user informations
  def update
    @user = User.find(params[:id])
    result = @user.update_attributes(params[:user])

    respond_to do |format|
      format.html do
        if result
          redirect_to admin_user_path(@user)
        else
          render :action => 'edit'
        end
      end
      format.js do
        render :json => {result: result, id: @user.id, params: params}
      end
    end

  end

  def reprocess_avatars
    result="OK"
    User.all.each do |user|
      begin
        user.photo.cache_stored_file!
        user.photo.retrieve_from_cache!(user.photo.cache_name)
        user.photo.recreate_versions!
        user.save!
      rescue => e
        result += "ERROR: #{User}: #{user.id} -> #{e.to_s}"
      end
    end
    render json: {result: result}
  end

  def withdraw
    user = User.find(params[:id])
    result = WithdrawService.perform(user)

    if result.errors.any?
      flash[:error] = result.errors.first
    else
      flash[:notice] = "Withdraw for #{user.name} completed."
    end

    redirect_to admin_users_path
  end
end
