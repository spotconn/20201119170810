class Admin::AdminUsersController < Admin::ApplicationController
  
  def index
    @admin_users = AdminUser.all
  end
  
  def show
    @admin_user = AdminUser.find(params[:id])
  end
  
  def edit
    @admin_user = AdminUser.find(params[:id])
  end
  
  def update
    @admin_user = AdminUser.find(params[:id])
    
    if @admin_user.update_attributes(params[:admin_user])
      flash[:notice] = 'administrator edited'
      redirect_to admin_admin_users_path
    else
      render :action => 'edit'
    end
  end
  
  def new
    @admin_user = AdminUser.new
  end
  
  def create
    @admin_user = AdminUser.new(params[:admin_user])
  
    if @admin_user.save
      flash[:notice] = 'administrator added'
      
      redirect_to admin_admin_users_path
    else
      render :new
    end
  end
  
  def destroy
    
  end
  
end
