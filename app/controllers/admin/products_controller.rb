class Admin::ProductsController < Admin::ApplicationController
  #before_filter :load_sections, :only => [:edit, :update]
  
  cache_sweeper :product_sweeper
  
  def index
    # @statuses = ['new','review','active','rejected','error']
    @statuses = Product.find_by_sql('select status, count(*) as status_count from products where deleted_at is null group by status union select "all", count(*) from products where deleted_at is null')
    @status = params[:status] || 'all'
    Product.per_page = 15
    @products = Product.includes(:user).
      order("#{sort_column} #{sort_direction}")
    if @status != 'all'
      @products = @products.where(status: @status)
    end
    if params[:q]
      @products = @products.search do
        fulltext params[:q]
        paginate page: params[:page], per_page: Product.per_page
      end.results
    else
      @products = @products.page(params[:page])
    end
  end

  def edit
    @product = Product.find(params[:id])
    @sections = Category.menu_sections
  end

  # Update user informations
  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      redirect_to admin_product_path(@product)
    else
      flash[:error]=@product.errors.full_messages
      render :action => 'edit'
    end
  end
  
  def show
    
    @product = Product.find(params[:id])
    
    if !@product.archive_data.nil?
      @archive_data = ActiveSupport::JSON.decode @product.archive_data
    end
  end
  
  def destroy
    @product = Product.find(params[:id])
    sample = @product.get_sample
    File.delete(@product.sample_path) if sample
    @product.remove_archive
    @product.remove_photo
    @product.destroy
    render :json => true
  end
  
  def reprocess
    @product = Product.find(params[:id])
    render json: {create_sample: @product.create_sample}
  end
  
  def approve
    @product = Product.find(params[:id])
    if @product.change_status('active')
      Notifier.product_aproved(@product).deliver
      render json: {product: @product.id, status: @product.status}
    else
      render json: {product: @product.id, status: 'error updating'}
      p @product.errors
    end
  end

  def reject_confirm
    @product = Product.find(params[:id])
    @options = Product::REJECT_REASONS
    #@options.unshift ['Select','']
    render :layout => false
  end

  def reject
    @product = Product.find(params[:id])
    if @product.change_status('rejected')

      #send author an email explaining why his upload was rejected
      Notifier.product_rejected(@product, params[:product][:reason]).deliver

      render json: {product: @product.id, status: @product.status}
    else
      render json: {product: @product.id, status: 'error updating'}
      #p @product.errors
    end
  end
  
  def highlight
    product = Product.find(params[:id])
    if product.update_column(:highlight, params[:highlight] == "true")
      render json: {product: product.id, status: "success"}
    else
      render json: {product: product.id, status: "error"}
      p product.errors
    end
  end  
  
end
