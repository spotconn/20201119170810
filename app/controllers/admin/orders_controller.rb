class Admin::OrdersController < Admin::ApplicationController
  def index
    Order.per_page = 15
    @sort_column ||= params[:sort] || "orders.id"
    
    @orders = Order.joins(:product).
      includes(:user).
      includes(:product).
      order("#{@sort_column} #{sort_direction}").
      page(params[:page])

    from = (params[:from] || 1.month.ago).to_date.beginning_of_day
    to = (params[:to] || Date.today).to_date.end_of_day

    @orders = @orders.where(created_at: (from..to))

  end
end
