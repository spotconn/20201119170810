class Admin::HomepageHighlightsController < Admin::ApplicationController

  cache_sweeper :homepage_highlight_sweeper

  def index
    @homepage_highlights = HomepageHighlight.all
    @sections = {}
    @homepage_highlights.each do |hh|
      @sections[hh.section] ||= []
      @sections[hh.section]<<hh
    end
  end
  
  def new
    @homepage_highlight = HomepageHighlight.new
  end

  def edit
    @homepage_highlight = HomepageHighlight.find(params[:id])
  end

  def create
    @homepage_highlight = HomepageHighlight.new(params[:homepage_highlight])
    @homepage_highlight.object_type = HomepageHighlight::HOME_SECTIONS[@homepage_highlight.section]

    respond_to do |format|
      if @homepage_highlight.save
        format.html { redirect_to admin_homepage_highlights_path, notice: 'Homepage highlight was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def update
    @homepage_highlight = HomepageHighlight.find(params[:id])

    respond_to do |format|
      if @homepage_highlight.update_attributes(params[:homepage_highlight])
        format.html { redirect_to admin_homepage_highlights_path, notice: 'Homepage highlight was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @homepage_highlight = HomepageHighlight.find(params[:id])
    @homepage_highlight.destroy

    respond_to do |format|
      format.html { redirect_to admin_homepage_highlights_path }
    end
  end
  
end
