class Admin::CategoriesController < Admin::ApplicationController
  helper_method :model, :model_name
  
  def index
    @categories = model.order(:name)
  end
  
  def roots
    @categories = Category.roots
  end
  
  def new
    @category = model.new(:parent_id => params[:parent_id])
  end
  
  def create
    @category = model.new(params[model_name])
  
    if @category.save
      flash[:notice]="#{model_name} added"
      redirect_to send("admin_#{model_name.downcase.pluralize}_path") 
    else
      render :new
    end
    
  end
  
  def edit
    @category = model.find(params[:id])
  end
  
  def update
    @category = model.find(params[:id])
    if @category.update_attributes(params[model_name])
      respond_to do |format|
        format.html do
          redirect_to send("admin_#{model_name.downcase.pluralize}_path") 
        end
        format.js do
          render :nothing => true
        end
      end
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @category = model.find(params[:id])
    if @category.destroy
      redirect_to send("admin_#{model_name.downcase.pluralize}_path") 
    end
  end
  
  
  private 
  def model
    @model ||= controller_name.classify.constantize
  end
  
  def model_name
    @model_name ||= controller_name.singularize
  end
end
