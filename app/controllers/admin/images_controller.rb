class Admin::ImagesController < Admin::ApplicationController

  def index
    @images = Image.all
  end
  
  def new
    @image = Image.new
  end
  
  def create
    @image = Image.new(params[:image])
  
    if @image.save
      flash[:notice]="Image added"
      redirect_to send("admin_images_path") 
    else
      render :new
    end
  end
  
  def edit
    @image = Image.find(params[:id])
  end
  
  def update
    @image = Image.find(params[:id])
    result = @image.update_attributes(params[:image])
      
    respond_to do |format|
      format.html do
        if result
          redirect_to admin_images_path
        else
          render :action => 'edit'
        end
      end
    end
  end
  
  def destroy
    @image = Image.find(params[:id])
    if @image.destroy
      redirect_to send("admin_images_path") 
    end
  end
  
end