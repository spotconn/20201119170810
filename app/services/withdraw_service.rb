# frozen_string_literal: true

class WithdrawService
  attr_accessor :errors

  def initialize
    @errors = []
  end

  def self.perform(*args)
    return new.perform(*args)
  end

  def perform(user)
    if user.paypal_account.blank?
      self.errors << "User's PayPal account is not registered."
      return self
    end

    if !user.confirmed?
      self.errors << "User's account is not confirmed."
      return self
    end

    if user.account.balance <= 0
      self.errors << "Insufficient balance"
      return self
    end

    payment = user.payments.new
    payment.status = 'N'
    payment.amount = user.account.balance

    if payment.save!
      gateway = ActiveMerchant::Billing::PaypalExpressGateway.new(PAYPAL_OPTIONS)

      transfer_response = gateway.transfer(
        (payment.amount * 100).to_i,
        user.paypal_account,
        subject: 'O Ritmo Withdrawal', note: 'Thank you for your business'
      )

      payment.merchant_transaction_id = transfer_response.params['correlation_id']
      payment.merchant_data = transfer_response.params
      payment.status = 'A'

      if transfer_response.success?
        payment.amount_paid = -1 * payment.amount
        payment.message = 'Withdraw'
        payment.save
        logger.info("[Withdraw] Performed for #{user.name}. Amount: #{payment.amount}")
      else
        payment.status = 'D'
        self.errors << payment.merchant_data['message']
        logger.warn("[Withdraw] Transfer failed for #{user.name}. Amount: #{payment.amount}")
        payment.save
      end
    end

    return self
  end

  private

  def logger
    Rails.logger
  end
end
