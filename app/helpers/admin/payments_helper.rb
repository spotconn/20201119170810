module Admin
  module PaymentsHelper
    def formatted_payment_status(status)
      case status
      when 'N' then 'Incomplete'
      when 'C' then 'Canceled'
      when 'A' then 'Approved'
      when 'D' then 'Error'
      else status
      end
    end
  end
end

