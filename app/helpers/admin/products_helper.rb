module Admin
  module ProductsHelper
    def formatted_date(date)
      date.in_time_zone('Brasilia').strftime("%b, %d %Y - %H:%M")
    end


    # TODO: time_ago_in_words is broken
    #
    # def formatted_time_ago(date)
    #   time_ago_in_words(date.in_time_zone('Brasilia'))
    # end
  end
end

