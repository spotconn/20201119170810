module Admin
  module CategoriesHelper
    def nested_categories(category)
      content_tag(:ul) do
        content_tag(:li) do
          concat category.name 
          html = " [#{link_to "Add", Rails.application.routes.url_helpers.new_admin_category_path(:parent_id => category.id)}]"
          html += "<ul>"
          if category.children.present?
            category.children.sort_by(&:name).each do |subcategory|
              html += content_tag(:li) do
                concat link_to subcategory.name, edit_admin_category_path(subcategory)
                concat ' '
                concat link_to 'x', admin_category_path(subcategory), method: :delete, confirm: 'Are you sure?'
                # concat subcategory.name
              end
            end
          end
          html += "</ul>"
          concat html.html_safe
        end  
      end
    end
  end
end
