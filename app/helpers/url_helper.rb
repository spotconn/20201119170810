module UrlHelper
  
  def current_url_at(locale)
    locale = locale.to_sym==I18n.default_locale ? "" : "/#{locale}"
    port = request.port==80 ? "" : ":#{request.port}"
    domain = request.domain || request.host
    base_url = "#{request.protocol}#{domain}#{port}"
    regex = /^\/(en|pt)/
    path = '' #request.path
    url = base_url + if path =~ regex
      path.sub(regex, locale)
    else
      locale + path
    end
    url
  end
  
end
