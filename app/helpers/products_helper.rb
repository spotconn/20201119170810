module ProductsHelper
  # Returns more informations about the product
  
  def more_info(product)
    sample_rate = display_sample_rate(product)
    duration = display_duration(product)

    info = []
    info << "#{t(:sample_rate)} #{sample_rate}" if sample_rate
    info << "#{t(:duration)} #{duration}" if duration
    info << "#{t(:extended_license)}" if product.allow_commercial_use
    info.join(", ")
  end
  
  def apply_currency(value)
    value = exchange(currency)*value if currency!='BRL'
    value = number_with_precision(value, :precision => 2)
  end

  # Defines the format for currency
  def price_format(value)
    value = apply_currency(value)
    locale = case cookies[:currency]
             when "BRL" then "pt"
             when "USD" then "en"
             else "pt"
             end
    separator = I18n.t("number.currency.format.separator", locale: locale)
    currency = number_to_currency(value, :precision => 2, :format => "%u%n", locale: locale)
    value, cents = currency.split(separator)
    cents = nil if cents=="00"
    (content_tag(:span, value, :class => "value") + content_tag(:span, cents, :class => "cents")).html_safe
  end

  # Defines the css price class for products
  def price_css_class(product)
    price_class = "default"
    price_class = "full-track" if product.is_full_track?
    price_class = "free" if product.price == 0

    price_class
  end

  # Formats the price for small display
  def price_small(product)
    content = ""

    if product.price.to_f > 0
        content = price_format(product.price.to_f)
    else
        content = t(:free)
    end

    content_tag(:div, content_tag(:div, link_to(content, user_product_path(product.user, product)), :class => "price_tag"), :class => "product_price #{price_css_class(product)}")
  end

  # Formats the price for big display
  def price_big(product)
    content = ""

    if product.price > 0
        content = price_format(product.price.to_f)
        link_text = t(:buy)
    else
        content = t(:free)
        link_text = t(:download)
    end

    suffix_content = link_to(link_text, new_product_order_path(product), :class => "buy", rel: "nofollow")
    content_tag(:div, link_to(content, new_product_order_path(product), rel: "nofollow") + content_tag(:hr) + suffix_content.html_safe, :class => "price #{price_css_class(product)}" )
  end
  
  # Display the sample rate information
  def display_sample_rate(product)
    archive_data = extract_archive_data(product)
    return archive_data if archive_data.nil?
    sample_rate = "#{archive_data["Precision"]} " unless archive_data["Precision"].nil?
    channels = archive_data["Channels"] == "2" ? "Stereo" : "Mono" unless archive_data["Channels"].nil?
    sample_rate += "#{channels} " unless channels.nil?
    sample = archive_data["Sample Rate"].to_i/1000 unless archive_data["Sample Rate"].nil?
    sample_rate += "#{sample} KHz" unless sample.nil?
    return sample_rate
  end
  
  # Display the duration information
  def display_duration(product)
    archive_data = extract_archive_data(product)
    return archive_data if archive_data.nil?
    return nil if archive_data["Duration"].nil? || archive_data["Duration"].length<2
    d = archive_data["Duration"][1].split(':')
    return nil if d.length<3
    return "#{d[1]}:#{d[2].split('.').first}"
  end
  
  # Extract the archive information about the product
  def extract_archive_data(product)
    if !product.archive_data.nil?
      return ActiveSupport::JSON.decode product.archive_data
    end
    nil
  end

  def category_hash(product)
    lehash = Category.build_hash
    cats = product.categories.sort_by(&:sort_by).map{|cat| cat.id }
    prod_hash = Hash.new 
    lehash.each do |k, v|
      prod_hash[k] ||= Array.new
      v.each do |obj|
        prod_hash[k] << obj if cats.index(obj.id)
      end
    end
    prod_hash
  end
  
  def get_subcategories(product, parentid)
    instruments = Array.new
    product.categories.sort_by(&:name).each do |category|
      instruments << category.name if category.ancestry==parentid.to_s
    end
    instruments
  end
  
  def display_product_name(name)
    name = name.capitalize
    name = name.sub(/\.mp3/,'')
    name = name.sub(/\.wav/,'')
    name = name.sub(/\.aif/,'')
  end

  def product_info(product)
    archive_data = extract_archive_data(product)
    "Bit Rate: #{archive_data["Bit Rate"]}
    #{t(:length)}: #{archive_data["Duration"][1]}
    #{t(:type).capitalize}: #{get_subcategories(product, 5).first }
    Sample Rate: #{archive_data["Precision"]}, #{archive_data["Sample Rate"].to_f/1000} kHz"
  end

end
