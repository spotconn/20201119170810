# encoding: utf-8

module ApplicationHelper

  def page_title
    if content_for(:title).present?
      content_for(:title)
    else
      @page_title ||= t(:page_title)
    end
  end

  def body_css_classes
    result = []

    result << controller_name.dasherize
    result << action_name.dasherize
    result << @additional_body_css_classes

    result = result.reject(&:blank?)
    result.map!{|css_class| "body-#{css_class}"}
    result.join(' ')
  end

  def footer_group(section)
    content_tag(:div, class: 'grid_2') do
      html = "<h4>#{section.name}</h4>"
      html += "<ul>"
        if section.is_a?(Category)
          children = section.children.order('product_count desc')
        else
          children = section.children
        end
        children[0..10].each do |category| #.sort_by(&:name)
          html += "<li>"
          html += link_to category.name, (category.is_a?(Category) ? category_path(category) : category.url)
          html += "</li>"
        end
      html +=  "</ul>"
      concat html.html_safe
    end
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    params[:sort]=column
    params[:direction]=direction
    link_to title, params, {:class => css_class}
  end

  def asset_url asset
    "#{request.protocol}#{request.host_with_port}#{asset_path(asset)}"
  end

  def currency
    @currency ||= cookies[:currency] ? cookies[:currency] : "BRL"
  end

  def exchange(currency)
    session[:exchange] ||= {}
    exchange = session[:exchange][currency] ? session[:exchange][currency] : convert_currency('BRL', currency, 1)
    session[:exchange][currency] = exchange
  end

  def convert_currency(from_curr, to_curr, amount)
  	host = "www.google.com"
  	http = Net::HTTP.new(host, 80)
  	url = "/finance/converter?a=#{amount}&from=#{from_curr}&to=#{to_curr}"
  	response = http.get(url)
  	#puts response.body
  	result = response.body
  	regexp = Regexp.new("(\\d+\\.{0,1}\\d*)\\s+#{to_curr}")
  	regexp.match result
  	return $1.to_f
  end

  def locales(options = {})
    languages = {
      en: "English",
      pt: "Português"
    }
    flags = {
      en: "usa-small.png",
      pt: "brazil-small.png"
    }
    content = lambda do |l|
      flag = image_tag(flags[l])
      language = content_tag(:span, languages[l], class: "language")
      if options[:selected]
        flag + language
      else
        link_to(current_url_at(l)) do
          flag + language
        end
      end
    end
    tags = {}
    [:en, :pt].each do |l|
      tags[l] = content_tag(:div, id: l) do
        content.call(l)
      end
    end
    if options[:selected]
      tags[I18n.locale]
    else
      tags.values
    end
  end

  def currencies(options = {})
    units = {
      "BRL" => "R$",
      "USD" => "US$"
    }

    tags = {}
    units.keys.each do |u|
        content = content_tag(:span, t(:prices_in) + " ", class: :unit_text) + units[u]
        tags[u] = options[:selected] ? content : link_to(content, set_currency_path(u), rel: u, class: "currency-change", remote: true)
    end

    if options[:selected]
        tags[currency]
    else
        tags.values
    end
  end
end
