module UsersHelper
  # List the user categories name
  def user_list_categories_name(user, with_link = false, css_class = "support")
    user.sort_categories_by('name').collect do |c| 
      if with_link
        link_to(c.name, category_path(c), :class => css_class)
      else
        c.name
      end
    end.join(", ").html_safe
  end

  def author_instruments(author)
    author.categories.where(type: "Instrument")
  end

  def author_instruments_names(author)
    instruments = author_instruments(author)
    if instruments.size<=3
      author_instruments(author).map(&:name).join(", ")
    else
      "#{author_instruments(author)[0..2].map(&:name).join(", ")}..."
    end
  end

end
