module CategoriesHelper
  # List the elements in check box elements format
  def categories_check_box_list(elements, model, field_name)
    total = elements.size
    cols = 3
    lines = (total / cols).ceil

    content = ""
    col = ""

    i = 0
    for e in elements
      class_name = model.class.to_s.underscore
      e_id = "#{class_name}_#{field_name}_#{e.id}"
      input = check_box_tag("#{class_name}[#{field_name}][]", e.id, model.categories.include?(e) ? true : false, :id => e_id)
      label = label_tag(e_id, e.name)

      span = content_tag(:span, (input + label), :class => "checkbox").html_safe
      # p span
      col += content_tag(:div, span, :class => "table-cell").html_safe
      i += 1

      if i == lines
        content += content_tag(:div, col.html_safe, :class => "table-col").html_safe
        i = 0
        col = ""
      end
    end

    content_tag(:div, (content + content_tag(:div, "", :class => "clear")).html_safe, :class => "table-list")
  end

  # Format the category name with the number of products elements
  def category_name_for_list(category)
    "#{category.name} (#{category.product_count})"
  end

  def regions_for_user(user)
    #city = Category.where(name: user.city).first
    #link_to user.city, category_path(city), class: "support"
  end
end
