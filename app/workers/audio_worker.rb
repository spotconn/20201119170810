class AudioWorker
  include Sidekiq::Worker

  def perform(product_id)
    Rails.logger.warn "performing product #{product_id}"
    product = Product.find(product_id)
    product.create_sample if product.sample.blank?
    Notifier.product_for_review(product).deliver
  end
end