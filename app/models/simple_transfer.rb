class SimpleTransfer < BaseTransaction
  validates :destination, :source, :amount, presence: true
  attr_accessor :created_at
  
  def process
    ActiveRecord::Base.transaction do
      create_debit
      create_credit
    end
  end
  
  def create_debit
    # debit buyer's account
    t1 = Transaction.new
    t1.account = self.source
    t1.debit_credit = "D"
    t1.description = self.description
    t1.amount = self.amount
    t1.transaction_type = self.class.to_s
    t1.save!
  end

  def create_credit
    # credit seller's account
    t2 = Transaction.new
    t2.account = self.destination 
    t2.debit_credit = "C"
    t2.description = self.description
    t2.amount = self.amount
    t2.transaction_type = self.class.to_s
    t2.save!
    
  end
   
end