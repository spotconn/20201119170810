class Rhythm < Category
  # attr_accessible :title, :body

  # scope :all, where(:ancestry => 2)

  attr_accessible :region_ids, :instrument_ids

  has_and_belongs_to_many :instruments
  has_and_belongs_to_many :regions

  def parent_id
    2
  end

end
