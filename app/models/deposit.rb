class Deposit < BaseTransaction
  validates :destination, :payment, presence: true
  attr_accessor :payment
  
  before_validation :assign_amount
  
  def process
    ActiveRecord::Base.transaction do
      create_credit
      create_fee_credit
      create_fee_debit
    end
  end
  
  def create_credit
    # credit customer's account
    t1 = Transaction.new
    t1.account = self.destination
    t1.debit_credit = "C"
    t1.description = self.description
    t1.amount = self.payment.amount_paid
    t1.transaction_type = self.class.to_s
    t1.referable = self.payment
    t1.save!
  end
   
  def create_fee_credit
    if self.fee.to_f > 0
      # credit fee to payment processor's account
      t3 = Transaction.new
      t3.account = Account::paypal
      t3.debit_credit = "C"
      t3.description = "Deposit Fee"
      t3.amount = self.fee
      t3.transaction_type = "Fee"
      t3.referable = self.payment
      t3.save!
    end
  end
   
  def create_fee_debit
    if self.fee.to_f > 0
      # debit fee to network's account
      t2 = Transaction.new
      t2.account = Account::marketplace
      t2.debit_credit = "D"
      t2.description = "Deposit Fee "+self.source.to_s
      t2.amount = self.fee
      t2.transaction_type = "Fee"
      t2.referable = self.payment
      t2.save!
    end
  end
  
  protected
    def assign_amount
      self.amount = payment.amount_paid if payment.present?
    end
end