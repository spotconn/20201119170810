class Collection < ActiveRecord::Base
  extend FriendlyId

  friendly_id :name, use: :slugged

  mount_uploader :cover, AvatarUploader
  mount_uploader :background, BackgroundUploader

  attr_accessible :name, :description, :spotify, :rdio, :itunes, :deezer,
                  :cover, :background, :product_ids

  belongs_to :user
  has_and_belongs_to_many :products

  validates :name, :description, :user, :cover, :product_ids, presence: true

  # Canditate to a form object
  def products_to_form_options
    user.products.order('created_at desc')
  end
end
