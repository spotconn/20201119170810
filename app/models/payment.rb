class Payment < ActiveRecord::Base
  has_many :transactions, as: :referable
  belongs_to :user
  after_save :create_transactions
  
  validates :amount, :user, :status, presence: true
  validates :amount, numericality: { :greater_than_or_equal_to => 0}
  
  attr_accessor :product_id, :mode
  
  def send_emails
    Notifier.new_payment_received(self).deliver
    Notifier.new_payment_confirmation(self).deliver
  end
    
  protected
    def create_transactions
      if self.status == "A" && self.transactions.count == 0 && self.amount_paid.to_i > 0 
        deposit = Deposit.new
        deposit.destination = user.account
        deposit.fee = fee
        deposit.description = "Deposit"
        deposit.payment = self
        deposit.create
      elsif self.status == "A" && self.transactions.count == 0 && self.amount_paid.to_i < 0 
        withdrawal= Withdrawal.new
        withdrawal.source = user.account
        withdrawal.fee = fee
        withdrawal.description = "Withdrawal"
        withdrawal.payment = self
        withdrawal.create
      end
    end

end
