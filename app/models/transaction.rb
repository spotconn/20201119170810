class Transaction < ActiveRecord::Base
  after_create :update_account
  before_destroy :revert_account
  
  acts_as_paranoid
  
  belongs_to :account
  belongs_to :referable, polymorphic: true
  
  validates :account, :transaction_type, :debit_credit, :amount, :description, presence: true
  validates :amount, numericality: { :greater_than_or_equal_to => 0 }
  
  def update_account
    sign = 1
    sign = -1 if debit_credit == "D"
    self.account.update_balance!( sign * amount )
  end
  
  def revert_account
    sign = 1
    sign = -1 if debit_credit == "C"
    self.account.update_balance!( sign * amount )
  end
  
end
