class BaseTransaction
  include ActiveModel::Validations
  include ActiveModel::Validations::Callbacks
  attr_accessor :source, :destination, :amount, :description, :fee
  validates :amount, presence: true
  validates :amount, numericality: { greater_than: 0 }

  def initialize(attributes = {})
    attributes.each do |key, value|
      send("#{key}=", value)
    end
  end

  def create
    if valid?
      process
    end
  end

  protected
    def process
      raise "Not yet implemented"
    end
end