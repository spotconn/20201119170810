class Withdrawal < BaseTransaction
  validates :source, :payment, presence: true
  attr_accessor :payment
  
  before_validation :assign_amount
  
  def process
    ActiveRecord::Base.transaction do
      create_debit
    end
  end
  
  def create_debit
    # debit buyer's account
    t1 = Transaction.new
    t1.account = self.source
    t1.debit_credit = "D"
    t1.description = self.description
    t1.amount = self.amount
    t1.transaction_type = self.class.to_s
    t1.save!
  end
  
  protected
    def assign_amount
      self.amount = -1*payment.amount_paid if payment.present?
    end

end
