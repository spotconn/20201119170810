#encoding: utf-8
# Requires libs
require 'o_ritmo/audio_file'
class Product < ActiveRecord::Base
  # Extends
  extend FriendlyId
  #extend Resque::Plugins::Retry

  # Behaviors
  acts_as_paranoid
  friendly_id :name, :use => :slugged
  mount_uploader :archive, ArchiveUploader
  mount_uploader :photo, PhotoUploader

  # Filters
  before_save :make_key
  after_create :enqueue
  after_save :index_user, :update_product_counts

  # Attributes
  attr_accessor :sample_path, :key_note, :key_sus, :key_maj, :reason
  attr_accessible :name, :description, :price, :archive, :status, :category_ids, :photo,
                  :remove_photo, :archive_data, :is_public, :allow_commercial_use, :bpm,
                  :key, :key_note, :key_sus, :key_maj, :status_count, :image_id

  #@queue = :products
  #@retry_limit = 1
  #@retry_delay = 15

  # Associations
  belongs_to :user
  has_one :account
  has_and_belongs_to_many :categories
  belongs_to :image
  has_many :orders
  has_many :homepage_highlights, dependent: :destroy, as: :object
  has_and_belongs_to_many :collections

  # Validations
  validate :validate_have_child_in_the_roots_categories, :on => :update
  validates :category_ids, :presence => true, :on => :update
  validates :name, :price, :user, :archive, :presence => true
  validates :bpm, :numericality => true, :length => { :maximum => 1000 }, :allow_blank => true, :allow_nil => true
  validates_numericality_of :price, :greater_than_or_equal_to => 0
  validates_numericality_of :price, :less_than_or_equal_to => 200

  # Scopes
  scope :is_public, where(:status => "active")

  # Constants
  REJECT_REASONS = [['Qualidade da gravação','qualidade'],['Volume','volume'],['Qualidade técnica','tecnica'],['Composição de outro artista','plagio'],['Inadequado para o nosso perfil de comercialização','inadequado'],['Classificação inconsistente','classificacao']]

  # Methods

  # Validate the product have any child in the each root categories
  def validate_have_child_in_the_roots_categories
    product_categories = self.categories

    for root_category in Category.roots
      if (product_categories & root_category.children).empty?
        errors.add(:category_ids, I18n.t('activerecord.errors.models.product.have_child_in_the_roots_categories'))
        return false
      end
    end
  end

  # Check the product is full track
  def is_full_track?
    self.categories.any? {|category| category.name.downcase == "full track" }
  end

  def self.perform(id)
    Rails.logger.warn "performing product #{id}"
    product = Product.find(id)
    product.create_sample if product.sample.blank?
    Notifier.product_for_review(product).deliver
    #begin
    #rescue ActiveRecord::RecordNotFound
    #  product = Product.find(id)
    #  product.create_sample if sample.blank?
    #end
  end

  def enqueue
    Rails.logger.warn "queued product #{self.id}"
    #Resque.enqueue(Product, self.id)
    AudioWorker.perform_async(self.id)
  end

  def sample
    @sample ||= self.get_sample
  end

  def get_sample
    return nil if self.archive_filename.nil?

    self.sample_path = "#{Settings.audio.sox_root_dir}/#{Settings.audio.samples_dir}/#{self.sample_filename(self.archive_filename)}"
    return "#{Settings.audio.samples_root_url}/#{sample_filename(self.archive_filename)}" if File.exists? self.sample_path
    return nil
  end

  def sample_filename(name)
    return name if name.nil?
    @sample_filename = "#{name.split('.').first}.mp3"
  end

  def archive_filename
    return self.archive.url if self.archive.url.nil?
    @archive_filename ||= File.basename(self.archive.url.split('?').first)
  end

  def create_sample

    self.change_status('processing')

    info = self.generate_sample
    if info
      #self.update_attributes(:status => 'review', :archive_data => info.to_json)
      self.change_status('review')
      Rails.logger.warn "SUCCESS"
      true
    else
      self.change_status('error')
      Rails.logger.warn "FAIL"
      false
    end

  end

  def generate_sample opts={}
    Rails.logger.warn "SOX #{self.id} / #{self.archive_filename} --------------------------------------"
    sample = ORitmo::AudioFile.new(self.archive_filename, opts)
    info = sample.process
    self.archive_data = info.to_json if info
    info
  end

  def thumburl
    @thumb ||= self.photo.blank? ? (self.image ? self.image.photo.url(:thumb) : '/assets/no_image.jpg') : self.photo.url(:thumb)
  end

  def photourl
    self.photo.blank? ? (self.image ? self.image.photo.url : '/assets/no_image.jpg') : self.photo.url
  end

  def change_status(new_status)
    if self.status != new_status
      self.status = new_status
      save!(validate: false)
    else
      false
    end
  end

  def file_exists?
    if ArchiveUploader::storage == CarrierWave::Storage::Fog
      self.archive.size # this fails if file doesnt exist
    else
      File.exists? "#{Settings.audio.sox_root_dir}#{self.archive.to_s}"
    end

    rescue
    nil
  end

  def index_user
    user.index!
  end

  def update_product_counts
    self.user.update_product_count
    self.categories.each do |category|
      category.update_product_count
    end
  end

  def make_key
    self.key = "#{key_note}#{key_sus}#{key_maj}"
  end

  def key_note
    @key_note || key.try(:first)
  end

  def key_sus
    if @key_sus
      @key_sus
    else
      case key.try(:size)
        when 3 then key[1]
        when 2 then key[1] =~ /b|\#/ ? key[1] : ''
        else ''
      end
    end
  end

  def key_maj
    if @key_maj
      @key_maj
    else
      case key.try(:size)
        when 3 then key[2]
        when 2 then key[1] == 'm' ? key[1] : ''
        else ''
      end
    end
  end

  def self.correct_s3_key(key)
    filekey = CGI::unescape(key)
    filekey[0]='' if filekey[0]=='/'
    filekey
  end

  def self.move_uploaded_file(key)
    s3 = AWS::S3.new
    obj = s3.buckets[Settings.audio.s3_bucket].objects[key]

    # generate new filename
    file_extension = key.split('.').last
    rand = (0...6).map{65.+(rand(25)).chr}.join
    digest = Digest::SHA1.hexdigest("#{key}-#{Time.now.to_i}-#{rand}")
    filename = "#{digest}.#{file_extension}"
    target = "uploads/product/archive/#{filename}"
    original_filename = key.split('-_-').last

    begin
      #move the file
      obj.move_to(target)
      rescue
    end

    {filename: filename, original_filename: original_filename, filepath: target}

  end

  searchable do
    text :name
    text :categories do
      categories.collect(&:name)
    end
    text :user do
      user.try :name
    end
    text :description
  end
end
