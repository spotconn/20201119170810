class Account < ActiveRecord::Base
  acts_as_paranoid
  
  MarketplaceName = "ORitmo"
  
  def self.marketplace
    find_or_create_by_name(MarketplaceName)
  end
  
  def self.paypal
    find_or_create_by_name("Paypal")
  end
  
  belongs_to :user
  has_many :transactions
  
  validates :balance, presence: true
  validates :balance, numericality: true
  #validate :validate_balance
  
  def update_balance! amount
    self.balance += amount
    save!
  end
  
  #def validate_balance
  #  errors.add(:balance, :invalid) if self.name != MarketplaceName && balance < 0
  #end
end
