class Image < ActiveRecord::Base
  # attr_accessible :title, :body
  
  acts_as_paranoid
  mount_uploader :photo, PhotoUploader
  attr_accessible :name, :photo
  
  validates :photo, :presence => true
  
  has_many :products
  
end
