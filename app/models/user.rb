class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable

  # Extends
  extend FriendlyId

  # Attributes
  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :name, :username, :country, :city, :password, :password_confirmation,
  :remember_me, :archive, :paypal_account, :bio, :photo, :remove_photo, :accept_terms,
  :accepted_buyer_terms_at, :accepted_seller_terms_at, :category_ids, :cpf, :cae, :full_name,
  :filiation, :product_count, :referred_by, :featured, :internal_rating, :musician, :current_password

  attr_accessor :current_password


  # Filters
  after_create :create_account
  after_create :notify_admins
  after_save :index_user

  # Associations
  has_many :products, :dependent => :destroy
  has_many :collections, :dependent => :destroy
  has_many :orders
  has_many :bought_products, through: :orders, source: :product
  has_many :payments
  has_one :account, :dependent => :destroy
  has_many :videos, :dependent => :destroy
  has_many :homepage_highlights, dependent: :destroy, as: :object
  has_and_belongs_to_many :categories

  # Validations
  validates :name, :city, :country, :accepted_seller_terms_at, :presence => true
  validates :full_name, :presence => true, :on => :create
  validates :cpf, :presence => true, :if => "country=='Brazil' && musician?" #, :on => :create

  usar_como_cpf :cpf

  # Others
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :omniauthable
  acts_as_paranoid
  mount_uploader :photo, AvatarUploader
  friendly_id :name, use: :slugged

  searchable do
    integer :id
    text :name
    integer :product_category_ids, multiple: true, stored: true
  end

  scope :featured, where(featured: true)
  scope :artists, where("exists (select 1 from products where products.user_id = users.id and products.status='active')")
  scope :buyers, where("not exists (select 1 from products where products.user_id = users.id and products.status='active')")

  # Methods

  # Find all public products
  def public_products
    self.products.is_public
  end

  # Get 3 last videos
  def last_videos
    self.videos.all :order => "created_at desc", :limit => 3
  end

  # Get user's categories sort by parameter
  def sort_categories_by(term)
    self.categories.all :order => "#{term} asc"
  end

  def accept_buyer_terms!
    update_attribute(:accepted_buyer_terms_at, Time.now())
  end

  def accepted_buyer_terms?
    accepted_buyer_terms_at.present?
  end

  def accept_seller_terms!
    update_attribute(:accepted_seller_terms_at, Time.now())
  end

  def accepted_seller_terms?
    accepted_seller_terms_at.present?
  end

  def create_account
    acct = Account.new
    acct.user = self
    acct.save!
  end

  def notify_admins
    Notifier.new_signup(self).deliver
    Notifier.new_signup_confirmation(self).deliver
  end

  def product_category_ids
    @product_category_ids ||= products.select(:id).includes(:categories).map { |p| p.categories.map(&:id) }.flatten.uniq
  end

  def similar_users(n = 1)
    return [] if product_category_ids.size==0

    search = User.search do
      with(:product_category_ids).any_of product_category_ids
      without :id, id
    end

    hits = search.hits.sort_by { |hit| (hit.stored("product_category_ids") & product_category_ids).size }
    hits.reverse[0..n-1].map(&:result)
  end

  def similar_user
    similar_users[0]
  end

  def update_product_count
    self.update_attributes(:product_count => self.products.is_public.length)
  end

  def index_user
    self.index!
  end

  def first_name
    name.split(' ')[0]
  end

  def impersonate!
    @impersonated = true
  end

  def confirmation_required?
    !@impersonated || super
  end

  def is_musician?
    (products.size > 0 || musician?) ? true : false
  end
end
