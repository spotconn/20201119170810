class MultipleUpload
  
  attr_accessor :error
  
  def initialize(user, params)
    @params = params
    @user = user
  end
  
  def file
    @file ||= @params[:files].first
  end
  
  def product
    @product ||= Product.new
  end
  
  def save
    product.archive = file
    product.name = file.original_filename
    product.user = @user
    product.price = 0
    product.status = 'new'
    
    if product.save #!(validate: false)
      true
    else
      self.error = product.errors
      false
    end
  end
  
  def to_json
    [
      {
        :id => product.id,
        :url => "",
        :thumbnail_url => "",
        :name => product.name,
        :type => file.content_type,
        :size => 0,
        :delete_url => "",
        :delete_type => "DELETE",
        :error => self.error
      }
    ]
  end
end