class Order < ActiveRecord::Base

  acts_as_paranoid

  belongs_to :user
  belongs_to :product
  #has_one :payment
  has_many :transactions, as: :referable, :dependent => :destroy

  validates :price, :amount_paid, :status, :user, :discount, :surcharge, presence: true
  validates :price, :amount_paid, numericality: { :greater_than_or_equal_to => 0}
  validate :has_enough_balance
  validate :check_amount_paid

  attr_accessible :price, :amount_paid, :payment_method, :status, :product, :discount, :surcharge

  after_create :create_transfer

  # sucharge for buy now
  def buynow_surcharge
    # BRL
    Settings.ordering.buynow_surcharge.to_f
  end

  def license_id
    Digest::MD5.hexdigest("#{id}-#{user.id}-#{product.id}")
  end

  def send_emails
    Notifier.new_order_received(self).deliver
    Notifier.new_order_confirmation(self).deliver
    Notifier.new_sale(self).deliver
  end

  # If product is deleted (with Paranoia), order can be able to find the product.
  def product
    Product.unscoped { super }
  end

  protected

    # commission charged by marketplace
    def marketplace_commission
      # percent
      Settings.ordering.marketplace_commission.to_f
    end

    def create_transfer
      transfer = Transfer.new
      transfer.source = user.account
      transfer.destination = product.user.account
      transfer.fee = (price * marketplace_commission/100.0)
      transfer.description = "Item Purchase #{product.name}"
      transfer.order = self
      transfer.create
    end

    def has_enough_balance
      return if !user.present?
      errors.add(:base, "You don't have enough credit to complete this transaction") if self.user.account.balance < product.price
    end

    def check_amount_paid
      errors.add(:base, "Invalid payment amount") if amount_paid.to_f != (price.to_f + surcharge.to_f - discount.to_f)
    end


end
