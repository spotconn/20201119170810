class Region < Category
  # attr_accessible :title, :body
  
  scope :all, where(:ancestry => 4)
  
  has_and_belongs_to_many :rhythms
  
  def parent_id
    4
  end
end
