class Video < ActiveRecord::Base
  # Attributes
  # Setup accessible (or protected) attributes for your model
  attr_accessible :url

  # Associations
  belongs_to :user

  # Validations
  validates :url, :presence => true
  after_validation :validate_youtube_or_vimeo

  # Check the video is from youtube or vimeo
  def validate_youtube_or_vimeo
    errors.add(:url, I18n.t("activerecord.errors.models.video.youtube_or_vimeo")) unless info
  end

  # Returns the informations about the video
  def info
    VideoInfo.get(self.url)
  end
end
