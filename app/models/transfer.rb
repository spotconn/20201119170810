class Transfer < BaseTransaction
  validates :destination, :source, :order, presence: true
  attr_accessor :order, :buyer_debit, :seller_credit

  before_validation :assign_amount

  def process
    ActiveRecord::Base.transaction do
      create_debit
      create_credit
      create_fee_credit
      create_fee_debit
      create_surcharge_credit
    end
  end

  def create_debit
    # debit buyer's account
    t1 = Transaction.new
    t1.account = self.source
    t1.debit_credit = "D"
    t1.description = self.description
    t1.amount = self.order.amount_paid
    t1.transaction_type = self.class.to_s
    t1.referable = self.order
    t1.save!
  end

  def create_credit
    # credit seller's account
    t2 = Transaction.new
    t2.account = self.destination
    t2.debit_credit = "C"
    t2.description = self.description
    t2.amount = self.order.price
    t2.transaction_type = self.class.to_s
    t2.referable = self.order
    t2.save!

  end

  def create_fee_credit
    if self.fee > 0
      # credit marketplace's commision do marketplace account
      t3 = Transaction.new
      t3.account = Account::marketplace
      t3.debit_credit = "C"
      t3.description = "Commission " + self.description
      t3.amount = self.fee
      t3.transaction_type = "Commission"
      t3.referable = self.order
      t3.save!
    end
  end

  def create_fee_debit
    if self.fee > 0
      # debit market place commission from seller's account
      t4 = Transaction.new
      t4.account = self.destination
      t4.debit_credit = "D"
      t4.description = "Commission "+self.description
      t4.amount = self.fee
      t4.transaction_type = "Commission"
      t4.referable = self.order
      t4.save!
    end
  end

  def create_surcharge_credit
    if self.order.surcharge > 0
      # credit marketplace account with surcharge amount
      t5 = Transaction.new
      t5.account = Account::marketplace
      t5.debit_credit = "C"
      t5.description = "Buy Now Surcharge "+self.description
      t5.amount = self.order.surcharge
      t5.transaction_type = "Surcharge"
      t5.referable = self.order
      t5.save!
    end
  end

  protected
    def assign_amount
      self.amount = self.order.amount_paid if order.present?
      #if order.present?
      #  self.buyer_debit = self.order.amount_paid
      #  self.seller_credit = self.order.price
      #end
    end
end