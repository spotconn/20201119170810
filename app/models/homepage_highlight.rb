class HomepageHighlight < ActiveRecord::Base
  
  HOME_SECTIONS = {"MAIN" => 'Product', "RHYTHM" => 'Product', "MOOD" => 'Product', "AUTHOR" => 'User'}
  
  # attr_accessible :title, :body
  belongs_to :object, polymorphic: true
  
  validates :section, :object, presence: true
  validates :section, inclusion: { in: HOME_SECTIONS.keys }
  
  attr_accessible :section, :object_id, :object_type, :position
  
  def allowed_sections
    HOME_SECTIONS.keys
  end
end
