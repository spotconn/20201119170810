class Category < ActiveRecord::Base
  
  extend FriendlyId
  
  # Attributes
  attr_accessible :name, :description, :parent_id, :file, :status, :sort_by, :type, 
                  :photo, :remove_photo, :background, :remove_background, :product_count

  # Associations
  has_ancestry
  has_and_belongs_to_many :products
  has_and_belongs_to_many :users
  
  # Validations
  validates :name, :presence => true
  
  mount_uploader :photo, CategoryPhotoUploader
  mount_uploader :background, BackgroundUploader
  
  # Others
  translates :name, :slug
  friendly_id :name, use: :globalize
  
  scope :active, where("exists (select 1 from categories_products cp join products p on cp.product_id = p.id where cp.category_id = categories.id and p.status='active')")
  
  # Methods

  # Find all public products
  def public_products
    self.products.is_public
  end
  
  # Find all instruments and sort by name
  def self.all_instruments
    Category.first.children.all :order => "name asc"
  end

  def self.build_hash
    @hash ||= self.full_hash
  end
  
  def self.full_hash
    lehash = Hash.new 
    Category.roots.arrange(:order => :sort_by).to_a.flatten.delete_if(&:blank?).each do |c|
      lehash[c.name] ||= c.children
    end
    lehash
  end
  
  def update_product_count
    self.update_attributes(:product_count => self.products.is_public.length)
  end
  
  def self.menu_sections
    self.roots.arrange(:order => :sort_by).to_a.flatten.delete_if(&:blank?)
  end
  
end
