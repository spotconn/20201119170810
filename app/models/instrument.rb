class Instrument < Category
  # attr_accessible :title, :body
  
  scope :all, where(:ancestry => 1)
  
  has_and_belongs_to_many :rhythms
  
  def parent_id
    1
  end
end
