xml.instruct!
xml.urlset "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do

  xml.url do
    xml.loc "http://oritmo.com"
    xml.priority 1.0
  end

  xml.url do
    xml.loc "http://oritmo.com/artistas"
    xml.priority 0.9
  end

  xml.url do
    xml.loc about_us_url
    xml.priority 0.9
  end

  xml.url do
    xml.loc termos_url
    xml.priority 0.3
  end

  @categories.each do |category|
    if category.products.size>0 || category.children.size>0
      xml.url do
        xml.loc category_url(category)
        xml.lastmod category.updated_at.to_date
        xml.priority 0.9
      end
    end
  end

  @users.each do |user|
    xml.url do
      xml.loc user_url(user)
      xml.lastmod user.updated_at.to_date
      xml.priority 0.9
    end
  end

  @products.each do |product|
    xml.url do
      xml.loc user_product_url(product.user, product)
      xml.lastmod product.updated_at.to_date
      xml.priority 0.9
    end
  end

end