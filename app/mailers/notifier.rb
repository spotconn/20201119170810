#encoding: utf-8

class Notifier < ActionMailer::Base
  default from: "from@example.com"
  default to: "tao@polomarte.com"

  def product_for_review(product)
    @product = product
    recipients = "play@oritmo.com"
    prefix = get_prefix()
    mail to: recipients, subject: "#{prefix} Review: #{product.name}"
  end
  
  # admin emails
    def new_signup(user) 
      @user = user
      @user_count = User.count
      recipients = "play@oritmo.com"
      prefix = get_prefix()
      mail to: recipients, subject: "#{prefix} Novo cadastro: #{user.name}"
    end
    
    def new_payment_received(payment)
      @payment = payment
      recipients = "play@oritmo.com"
      prefix = get_prefix()
      mail to: recipients, subject: "#{prefix} Pagamento recebido! #{payment.user.name}"
    end

    def new_order_received(order)
      @order = order
      recipients = "play@oritmo.com"
      prefix = get_prefix()
      mail to: recipients, subject: "#{prefix} Nova venda! #{order.product.name} by #{order.product.user.name} (R$ #{order.amount_paid})"
    end
  
  # user emails

    def new_signup_confirmation(user)
      @user = user
      recipients = "tao@polomarte.com" if Rails.env.staging?
      recipients = user.email unless Rails.env.staging?
      prefix = get_prefix()
      mail to: recipients, from: "play@oritmo.com", subject: "Bem vindo ao ORITMO"
    end

    def new_payment_confirmation(payment)
      @payment = payment
      recipients = "tao@polomarte.com" if Rails.env.staging?
      recipients = payment.user.email unless Rails.env.staging?
      prefix = get_prefix()
      mail to: recipients, from: "play@oritmo.com", subject: "#{prefix} Confirmação de pagamento"
    end

    def new_order_confirmation(order)
      @order = order
      recipients = "tao@polomarte.com" if Rails.env.staging?
      recipients = order.user.email unless Rails.env.staging?
      prefix = get_prefix()
      mail to: recipients, from: "play@oritmo.com", subject: "#{prefix} Confirmação de compra"
    end
    
    def new_sale(order)
      @order = order
      @buyer = @order.user
      @seller = @order.product.user
      @buyer_location = @buyer.country=="Brazil" ? @buyer.city : "#{@buyer.city}, #{@buyer.country}"
      recipients = "tao@polomarte.com" if Rails.env.staging?
      recipients = @seller.email unless Rails.env.staging?
      prefix = get_prefix()
      mail to: recipients, from: "play@oritmo.com", subject: "#{prefix} Nova venda [#{@order.product.name}]"
    end

    def product_rejected(product, message)
      @product = product
      case message
      when 'qualidade'
        @message = 'o arquivo enviado não possui a qualidade ideal para que outros artistas possam utilizá-lo em suas produções.'
      when 'volume'
        @message = 'o arquivo enviado não possui o volume ideal para que outros artistas possam utilizá-lo em suas produções.'  
      when 'tecnica'
        @message = 'o arquivo enviado não possui a qualidade técnica ideal para que outros artistas possam utilizá-lo em suas produções.'
      when 'plagio'
        @message = 'o arquivo enviado é uma composição de outro artista, com direitos autorais reservados.'
      when 'inadequado'
        @message = ' o arquivo enviado é inadequado para o nosso perfil de comercialização.'
      when 'classificacao'
        @message = 'Infelizmente, o arquivo enviado não foi classificado de maneira inconsistente, dificultando sua venda e busca por compradores. Pedimos, por favor, que você revise sua classificação e submeta novamente.'
      end
      recipients = "tao@polomarte.com" if Rails.env.staging?
      recipients = @product.user.email unless Rails.env.staging?
      prefix = get_prefix()
      mail to: recipients, from: "play@oritmo.com", cc: "play@oritmo.com", subject: "#{prefix} Arquivo rejeitado: #{product.name}"
    end

    def product_aproved(product)
      @product = product
      recipients = "tao@polomarte.com" if Rails.env.staging?
      recipients = @product.user.email unless Rails.env.staging?
      prefix = get_prefix()
      mail to: recipients, from: "play@oritmo.com", cc: "play@oritmo.com", subject: "#{prefix} Arquivo aprovado: #{product.name}"
    end
    

    ######

    def party_invitation(user)
      @user = user
      mail to: @user.email, from: "play@oritmo.com", subject: "Lançamento ORITMO"
    end

    protected
    def get_prefix()
      Rails.env.staging? ? "ORitmo STAGING:" : "ORitmo -"
    end
  
end
