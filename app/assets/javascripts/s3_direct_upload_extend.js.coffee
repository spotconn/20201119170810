
jQuery ($) ->
  
  $("#myS3Uploader").S3Uploader
    remove_completed_progress_bar: false
    remove_failed_progress_bar: false
    progress_bar_target: $("#uploaded-files")

    before_add: () ->
      all_match = true
      $(arguments).each (index, object) ->
        filename = object.name
        match = /(\.|\/)(wav|mp3|aif)$/i.test(filename)
        #match = true
        if(!match) 
          alert('Formato de arquivo não aceito: '+filename)
          all_match = false

      return all_match

  $('#myS3Uploader').bind 's3_uploads_start', (e, content) ->
    $(".instruction").hide()
    $("#fileupload").addClass("dropped")
    $("#next-multiple-product").hide()
    prevent_navigate_away()
    #   console.log

  $('#myS3Uploader').bind "s3_upload_complete", (e, content) ->
    progress_bar = $("#file-"+content.unique_id).find(".progress").first().find(".bar").first()
    progress_bar.addClass("bar-success")
    progress_bar.css("width", "100%")

    $("#file-"+content.unique_id).find(".file-message").first().text('processando...')

  
  $('#myS3Uploader').bind "s3_upload_failed", (e, content) ->  
    progress_bar = $("#file-"+content.unique_id).find(".progress").first().find(".bar").first()
    progress_bar.addClass("bar-danger")
    alert(content.filename+" failed to upload : "+content.error_thrown)
    $("#file-"+content.unique_id).find(".file-message").first().text('erro')
    prevent_navigate_away()
    
  $('#myS3Uploader').bind "ajax:success", (e, data) ->
    #console.log("server was notified of new file on S3; responded with ")
    #console.log(data)
    content = data[0]
    $('#file-'+content.unique_id).attr('rel', content.product_id)
    $("#file-"+content.unique_id).find(".file-message").first().text('')
    loadForm(content.product_id)
    

  $(".file-container").live 'click', () ->
    product_id = $(this).attr('rel')
    showForm(product_id)

  showForm = (product_id) ->
    hide_all_forms()
    $('#file-form-'+product_id).show()
    $("#file-forms .loading").hide()
    $('.file-container').each (k,v) ->
      if $(v).attr('rel') == product_id.toString()
        $(v).addClass('active')


  loadForm = (product_id) ->

    if $('#file-form-'+product_id).size()==0

      $("#file-forms").append('<div id="file-form-'+product_id+'" class="file-form-container"></div>')
      $("#file-forms").append('<div id="file-message-'+product_id+'" class="file-message-container" style="display: none"></div>')
      $('#file-form-'+product_id).html('<div class="loading" style="display: inline">Aguarde, carregando formulario...</div>')

      if any_forms_visible(product_id)
        $('#file-form-'+product_id).hide()
        #console.log('hide '+product_id)
      else
        #console.log('no forms visible')
        showForm(product_id)

      # Load product form by ajax
      $.ajax
        url: '/sons/' + product_id + '/form?position='+product_id
        type: 'GET'
        success: (data) ->
          #$("#next-multiple-product").show()

          current_form = $("#file-form-"+product_id)
          current_form.html(data)
          
          str = """
            <div id=\"copy_#{product_id}\" class=\"copy_form_container\">
              <a class=\"copy_form_data\" rel=\"#{product_id}\">copiar dados de outro arquivo</a>
              <div class=\"copy_form_select\" style=\"display:none\">
                <select id=\"copy_to_#{product_id}\" class=\"copy_ddwn\" rel=\"#{product_id}\"><option value=\"\"></option></select>
                <a class="copy_form_data_action" rel=\"#{product_id}\">ok</a>
              </div>
            </div>
            <div class="clearfix" />
            """
          current_form.prepend(str)
          
          current_form.find('.carousel').carousel({ interval: false })
          load_copy_dropdowns()
          hide_copy_dropdowns()
          
          current_form.on "ajax:success", (xhr, data, status)  -> 
            console.log(data)
            console.log($(this))
            form_name = $(this).attr('id').split('-')
            product_id = form_name[2]

            if data.responseText.indexOf("error-explanation")>=0
              $("#file-form-#{product_id}").show();
            else
              $("#file-message-#{product_id}").html('Arquivo salvo com sucesso')
              $("#file-message-#{product_id}").show();

          current_form.on "ajax:beforeSend",  (xhr, settings) -> 
            $("html, body").animate({ scrollTop: 200 }, 600)
            $('#file-forms .loading').show()
            $('#file-forms .loading').css('visibility','visible')
            $(this).hide()
            
          current_form.on "ajax:complete", (xhr )  ->  
            $('#file-forms .loading').hide()
            
          current_form.on "ajax:error", (xhr, status, error)  ->  
            $("#file-message-#{product_id}").html('Erro ao salvar arquivo. Tente novamente ou mande uma mensagem para play@oritmo.com relatando este erro.')
            $("#file-message-#{product_id}").show();

          current_form.find("form").live 'submit', (e) ->
            e.preventDefault()

            # Change the button value when submit the form
            current_form.find('#form-buttons input[type="submit"]').attr('disabled', 'disabled')
            current_form.find('#form-buttons input[type="submit"]').attr('value', 'Enviando...')
            


  hide_all_forms = () ->
    $('.file-form-container').each (k,v) ->
      $(v).hide()
    $('.file-container').each (k,v) ->
      $(v).removeClass('active')
    $('.file-message-container').each (k,v) ->
      $(v).hide()

  any_forms_visible = (product_id) ->
    isVisible = false
    $('.file-form-container').each (k,v) ->
      # console.log(v)
      if $(v).is(':visible') && $(v).attr('id') != 'file-form-'+product_id
        isVisible = true

    return isVisible
    
    
  all_file_names = (except) ->
    names = []
    $('.edit_product').each (k,v) ->
      name = $(v).find('#product_name').val()
      _id = $(v).attr('id').split('_')
      id = _id[2]
      if id && id!=except
        names.push [id, name]
        
    names
  
  hide_copy_dropdowns = () ->
    if $('.file-container').length <= 1
      $('.copy_form_container').hide()
    else
      $('.copy_form_container').show()
    
  load_copy_dropdowns = () ->
    files = all_file_names(0)
    $('.copy_ddwn').each (k,v) ->
      $(v).find('option').remove()
      $(v).append('<option value=""></option>')
      product_id = $(v).attr('rel')
      $(files).each (ix, file) ->
        if product_id != file[0]
          $(v).append('<option value="'+file[0]+'">'+file[1]+'</option>')

  prevent_navigate_away = () ->
    all_done = true
    possibilities = ["editado","erro"]
    $('.file-container').each (k,v) ->
      if !possibilities.include?($(v).html())
        window.onbeforeunload = () ->
          "You're about to end your session, are you sure?"
          
        all_done = false
    if all_done
      window.onbeforeunload = null
    
    
  $('.copy_form_data').live 'click', () ->
    product_id = $(this).attr('rel')
    container = $(this).parent().find('.copy_form_select')
    $(container).show()
    
  $('.copy_form_data_action').live 'click', (e) ->  
    e.preventDefault()
    target_product_id = $(this).attr('rel')
    source_product_id = $("#copy_to_#{target_product_id}").val()
    
    source_form = $("#edit_product_#{source_product_id}")
    target_form = $("#edit_product_#{target_product_id}")
    
    ignore_fields = ["utf8", "_method", "authenticity_token", "product[name]", "position", "commit", "product[bpm]", "product[key_note]", "product[key_sus]", "product[key_maj]"]
    
    copy_fields = $(source_form).find('input')
    Array::push.apply copy_fields, $(source_form).find('select')
    Array::push.apply copy_fields,  $(source_form).find('textarea')
    
    $(copy_fields).each (k,v) ->
      
      unless $(v).attr('name') in ignore_fields || $(v).attr('id')==undefined
        #console.log "#{$(v).attr('id')} : #{$(v).val()}"
        target_field = $(target_form).find("#"+$(v).attr('id'))
        if $(v).attr('type')=='checkbox' && $(v).attr('checked')
          $(target_field).attr("checked",true)
        else
          $(target_field).val($(v).val())





    
    






  