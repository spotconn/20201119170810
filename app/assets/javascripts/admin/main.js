jQuery(function($) {
  
  var toggleLoading = function(id) { $("#loading-"+id).toggle(); };

  $(".reprocess_sample")
    .bind("ajax:beforeSend",  function(xhr, settings) {toggleLoading($(this).attr('rel')); $(this).hide(); })
    .bind("ajax:complete", function(xhr, settings ){toggleLoading($(this).attr('rel'));})
    .bind("ajax:success", function(xhr, data, status) {
      if(data.create_sample) window.location.reload(); else alert('failed to create sample');
    });
    
  $(".status-product")
    .live("ajax:beforeSend",  function(xhr, settings) {
      id = $(this).attr('rel')
      $("#approve-"+id).hide();
      $("#reject-"+id).hide();
      $("#loadingst-"+id).show();
      $('#reject_modal_'+id).modal('hide');
    })
    .live("ajax:complete", function(xhr, settings ){
      id = $(this).attr('rel')
      $("#loadingst-"+id).hide();
    })
    .live("ajax:success", function(xhr, data, status) {
      id = $(this).attr('rel')
      switch(data.status){
        case 'active':
          $('#td-'+id).html('<span class="label label-success">active</span>');
          break;
        case 'rejected':
          $('#td-'+id).html('<span class="label label-inverse">rejected</span>');
          break;
        default:
          $('#td-'+id).html('<span class="label">'+data.status+'</span>');
          break;
      }
    });

  $(".admin-user-rating")  
    .bind("ajax:complete", function(xhr, data, status) {
      
      var response = jQuery.parseJSON(data.responseText);
        
      var id = response.id;
      var rating = response.params.user.internal_rating;
      
      for (var i=1; i<=6; i++){
        if(i<=rating){
          $("#star_"+id+"_"+i).toggleClass('icon-star-empty', false);
          $("#star_"+id+"_"+i).toggleClass('icon-star', true);
        }else{
          $("#star_"+id+"_"+i).toggleClass('icon-star-empty', true);
          $("#star_"+id+"_"+i).toggleClass('icon-star', false);
        }
      }
    });
     
  $('.preset-image-container')
    .mouseenter(function(){
      var id = $(this).attr('rel');
      $("#delete_"+id).show();
    })
    .mouseleave(function(){
      var id = $(this).attr('rel');
      $("#delete_"+id).hide();
    });
  
  $('.highlight-checkbox input[type="checkbox"]').change(function() {
      var that = $(this);
      $.post("sons/" + that.data("id") + "/highlight", { highlight: that.is(":checked") });
  });
  
  
  $(".destroy-product")
    .bind("ajax:beforeSend",  function(xhr, settings) {
      $(this).parents('tr').remove();
    });

  $(".datepicker").datepicker({ format: "dd/mm/yyyy" });
});
