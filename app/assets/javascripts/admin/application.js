/*
 *= require_self
 *= require jquery
 *= require jquery_ujs
//= require twitter/bootstrap
 *= require vendor/jquery.ui.widget
 *= require vendor/bootstrap-datepicker
 
 *= require admin/main
 *= require admin/homepage_highlight
 
 *= require "vendor/jplayer/jquery.jplayer.min"
 *= require "vendor/dateformat"
 *= require "player_control"
 *= require dataToggle
*/
