$(document).ready(function() {
  
  update_selects();
  
}) 

jQuery(function($) {
  $('.section_select').change(function(){
    update_selects();
  })
});

function update_selects(){
  reset_selects();
  var section = $('.section_select').val();
  $("."+section+"_select").prop('disabled', false);
  $("."+section+"_wrap").show();
}

function reset_selects(){
  var items = Array('MAIN','AUTHOR','MOOD','RHYTHM');
  for (var i=0; i<items.length; i++){
    $("."+items[i]+"_select").prop('disabled', true);
    $("."+items[i]+"_wrap").hide();
  }
}