/*
 *= require_self
 *= require jquery
 *= require jquery_ujs
//= require twitter/bootstrap
 *= require vendor/jquery.ui.widget
 
 *= require s3_direct_upload
 *= require s3_direct_upload_extend
 
 *= require vigoo
 *= require main
 *= require carousel
 *= require timer
 *= require dataToggle
	
 *= require vendor/jquery.autoellipsis-1.0.10.min.js
 
 *= require "vendor/jquery.movingboxes.min"
 *= require vendor/jquery.ba-hashchange.min
 
*/
