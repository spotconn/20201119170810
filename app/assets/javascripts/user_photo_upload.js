$(function () {
    $('#photoupload').fileupload({
      maxFileSize: 6000000,
      url: "/artistas/update_photo",
      acceptFileTypes: /(\.|\/)(png|gif|jpg|jpeg)$/i,
      fileInput: $('#upload-file-button'),
      dataType: 'json',
      
      complete: function (data, result) {
        // console.log(data);
        file = jQuery.parseJSON(data.responseText)
        // console.log(file);
        $("#photo_progress .bar").css('width', '100%');
        $("#photo_message").html('done');
        $("#photo_progress").addClass('progress-success');
        $("#photo_progress").removeClass('active');
        
        if(file.result){
          $('#user_avatar').attr('src',file.photourl);
          $('#ajax-modal').modal('hide');
          $('#ajax-modal').html('');
        }
      },
      done: function (e, data) {
        // console.log('done');
      },
      
      change: function (e, data){
        // console.log('change');
        
      }, 

      progress: function (e, data){
        // console.log('progress');
        $.each(data.files, function(index, file) {
          var progress = parseInt(data.loaded / data.total * 100, 10).toString();

          $("#photo_progress .bar").css('width', progress + '%');
          $("#photo_message").html(progress + '%');
          
          if (progress == '100')
            $("#photo_message").html('finishing up');
        });
      },

      fail: function (e, data){
        // console.log('fail');
        $("#photo_progress").addClass('progress-danger');
        $("#photo_progress").removeClass('active');
        $("#photo_message").html('Erro no upload');
        $("#photo_message").show();
      },
      
    }).bind('completed', function(e,data){
      // console.log('complete');
      // console.log(data.result);
    });
});