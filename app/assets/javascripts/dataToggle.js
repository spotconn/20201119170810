
jQuery(function() {
    var data_toggle = function(target, show) {
        $("#" + target).toggle(show);
    };

    var toggle_controllers = $("*[data-toggle]");
    toggle_controllers.change(function() {
        var that = $(this);
        data_toggle(that.data("toggle"), that.is(":checked"));
    });
    toggle_controllers.trigger("change");
});
