jQuery(function() {

    var showCorrectFields = function() {
        var country = $('#user_country').val();
        if (country=='Brazil'){
            $("#brazil_fieldset").show();
        } else {
            $("#brazil_fieldset").hide();
        }
    }

    $('#user_country').change(function(){
        showCorrectFields();
    })

    $('.ellipsis').ellipsis();
    
});
  
function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}