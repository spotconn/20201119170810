jQuery(function() {
    var highlights = $("#highlights");
    
    if (highlights.length==0) return false;
 
    highlights[0].setTransitionOptions = function() {
        var options = this.getTransitionOptions();
        var mbOptions = $(this).getMovingBoxes().options;
        mbOptions.delayBeforeAnimate = options["fadeTime"];
        mbOptions.speed = options["speed"];
    }

    highlights[0].getTransitionOptions = function(type) {
        var options = $(this).data("transition-options");
        var current;
        if (typeof type === "undefined") {
            current = options["current"];
        } else {
            current = type;
        }
        return options[current];
    }

    highlights[0].setCurrentTransition = function(current) {
        $(this).data("transition-options").current = current;
    }

    var timer = new Timer(6000, function() {
        highlights.getMovingBoxes().goForward();
    });

    highlights.data("transition-options", {
        normal: {
            fadeTime: 400,
            speed: 1000
        },
        user_interaction: {
            fadeTime: 100,
            speed: 200
        },
        current: "normal",
        timer: timer
    });

    highlights.movingBoxes({
        fixedHeight: true,
        reducedSize: 0.85,
        wrap: true,
        speed: 0,
        easing: "swing",
        buildNav: true,
        hashTags: false,
        delayBeforeAnimate: 100,
        navFormatter : function(){ return "&#9679;"; },
        initChange: function(e, slider, target) {
            if (target == 0) { target = slider.$panels.length - 2; }
            if (target == slider.$panels.length - 1) { target = 1; }
            var fadeTime = this.getTransitionOptions()["speed"];
            $(slider.$panels[slider.curPanel]).find(".show-when-current").fadeOut(fadeTime);
            $(slider.$panels[target]).find(".show-when-current").fadeIn(fadeTime);
        },
        initialized: function() {
            $("#highlights-bg").height($(".current").next().height());
        },
        beforeAnimation: function(e) {
            this.setTransitionOptions();
            $("#highlights-bg").fadeOut(this.getTransitionOptions()["fadeTime"]);
        },
        completed: function() {
            var options = this.getTransitionOptions();
            $("#highlights-bg").fadeIn(options["fadeTime"]);
            this.setCurrentTransition("normal");
            this.setTransitionOptions();
        }
    });

    $(".mb-scrollButtons").unbind("click");
    
    var userClicked = function() {
        highlights[0].setCurrentTransition("user_interaction");
        //timer.reset();
    }

    var mb = highlights.getMovingBoxes();

    $(".mb-left").click(function() {
        userClicked();
        mb.goBack();
        return false;
    });

    $(".mb-right").click(function() {
        userClicked();
        mb.goForward();
        return false;
    });

    //timer.start();
});
