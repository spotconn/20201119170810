var Timer = function(interval) {
        this.callbacks = [];
        for(var i = 1; i < arguments.length; i++) {
            this.callbacks.push(arguments[i]);
        }
        this.interval = arguments[0];
        this.intervalVariables = [];
        this.started = false;

    this.start = function() {
        if (!this.started) {
            for(var i = 0; i < this.callbacks.length; i++) {
                this.intervalVariables[i] = setInterval(this.callbacks[i], this.interval);
            }
            this.started = true;
        }
    }

    this.stop = function() {
        if (this.started) {
            for(i in this.intervalVariables) {
                clearInterval(this.intervalVariables[i]);
            }
            this.started = false;
        }
    }

    this.reset = function() {
        if (this.started) {
            this.stop();
        }
        this.start();
    }

    this.setInterval = function(interval) {
        this.interval = interval;
        if (this.started) {
            this.reset();
        }
    }
}
