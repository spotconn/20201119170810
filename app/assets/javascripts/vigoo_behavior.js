// Describes the Vigoo behaviors
function VigooBehavior() {
  // Initialize the Vigoo Behavior
  this.initialize = function() {
    //this.load_user_status();
    this.to_modal();
    this.upload_add_files();
    this.upload_add_files_direct();
  },
  
  this.load_user_status = function(){
    $("#account").load('/artistas/status');
  }

  // Define the upload button behavior
  this.upload_add_files = function() {
    // Checks if the elements exists
    if ($("#upload-add-files") && $("#upload-file-button")) {
      $("#upload-add-files").click(function() {
        $("#upload-file-button").click();
        return false;
      });
    }
  },
  this.upload_add_files_direct = function() {
    // Checks if the elements exists
    if ($("#upload-add-files") && $("#file")) {
      $("#upload-add-files").click(function() {
        $("#file").click();
        return false;
      });
    }
  },

  // Define the modal behavior
  this.to_modal = function () {
    $('.to_modal').click(function(e) {
      e.preventDefault();
      var href = $(this).attr('href');

      // Check the link, if is anchor link or url path
      if (href.indexOf('#') == 0) {
        $(href).modal('open');
      } else {
        $.get(href, function(data) {
          // Put the href return content into ajax-modal div
          $('#ajax-modal').html(data);
          // Define the modal behavior for ajax-modal div
          $('#ajax-modal').modal();

          // Check if form exists
          if ($('#ajax-modal .modal-body form').length > 0) {
            // If form exists defines the modal-submit button action
            $('#modal-submit').click(function() {
              // Submit the form when click the modal-submit button
              $('#ajax-modal .modal-body form').submit();
            });

            // When ajax return success put the return content into ajax-modal div
            $('#ajax-modal .modal-body form').bind('ajax:success', function(xhr, data, status){
              $('#ajax-modal .modal-body').html(data);
            });
          } else {
            // Hide the modal-submit button when the form doesn't exists
            $('#modal-submit').hide();
          }
        });
      }
    });
  }
}

// New VigooBehavior object
$(document).ready(function() {
  var vigoo_behavior = new VigooBehavior();
  vigoo_behavior.initialize();
});

jQuery(function($) {
  $(".remove-product")
    .bind("ajax:beforeSend",  function(xhr, settings) {
        id = $(this).attr('id');
        $('#product-'+id).hide();
      })
    .bind("ajax:success", function(xhr, data, status) {
      id = $(this).attr('id');
      if(data) $('#product-'+id).remove();
    });
    

    // delete videos - user/show
    $(".delete_video")
      .bind("ajax:beforeSend",  function(xhr, settings) { 
        var item = "#user_video_"+$(this).attr('rel');
        item = item.replace(" nofollow","")
        $(item).hide(); 
      });
      

  $('.product-image-select').live('click', function(e) {
    var ids, imageid, productid;
    ids = $(this).attr('rel').split('_');
    imageid = ids[0];
    productid = ids[1];
    $('#edit_product_' + productid).find('.carousel-inner').find('img').toggleClass('selected', false);
    $(this).addClass('selected');
    return $('#edit_product_' + productid).find('#product_image_id').val(imageid);
  });
  
  $('.currency-change')
    //.bind("ajax:beforeSend",  function(xhr, settings) {
    //  $(this).text('loading...');
    //})
    .bind("ajax:success", function(xhr, data, status) {
      window.location.reload()
    });
    
  
  
});

