class window.FileUpload
  isEditing = false
  isAllDone = false
  showAjaxLoader = true
  files = new Array()

  constructor: ->
    # console.log('constructor')

    @setup()
    @setEvents()
    @files = []
  
  setup: ->
    # console.log('setup')

    that = @
    
    doUpload = (e, data) ->
      # console.log('doUpload')

      #$('#upload-finish-button').hide()
      $.each data.files, (index, file) ->

        if files.length> 0 && jQuery.inArray(file.name, files) >= 0
          alert('Um arquivo com mesmo nome já foi transferido.')
          # console.log('aborted')
          data.cancel()
          return false

        files.push(file.name)
        
        ix = jQuery.inArray(file.name, files)
        that.files.push(new window.File(file, ix))

        str = '<div class="file-container" id="file_' + ix + '">'
        str += '  <a href="#" data-index="' + ix + '">'
        str += '  <div class="progress-container">'
        str += '    <div class="progress_bar" id="progress_' + ix + '">&nbsp;</div>'
        str += '  </div>'
        str += '  <div class="file-name">' + file.name 
        str += '  </div>'
        str += '  <div class="file-message" id="message_' + ix + '"/>'
        str += '  </a>'
        str += '</div>'
        str += '<div class="clear"/>'

        $("#uploaded-files").append(str)

        str = '<div class="file-form" id="file-form-' + ix + '"/>'
        str += '<div class="clear"/>'

        $("#file-forms").append(str)
     
      showed = false

      $.each data.files, (index, file) ->    
        ix = jQuery.inArray(file.name, files)
        if !$("#file-form-" + ix).hasClass('edited-form') && !showed && !isEditing
          that.edit(ix)
          showed = true
    
    
    @jqXHR = $('#fileupload').fileupload
      sequentialUploads: true
      maxFileSize: 45000000
      url: "/multiple_products"
      acceptFileTypes: /(\.|\/)(wav|mp3|aif)$/i
      dropZone: $('#fileupload')
      fileInput: $('#upload-file-button')
      
      done: (e, data) ->
        # console.log('done')
        
        for file, index in data.result          
          ix = jQuery.inArray(file.name, files)
          progress_ix = '#progress_' + ix
          message_ix = '#message_' + ix
          file_form_ix = '#file-form-' + ix

          if file.error != null
            $(progress_ix).addClass('error')
            $(message_ix).html(file.error.archive[0])
            $(file_form_ix).remove()
            that.files[ix].error()
          else           
            that.files[ix].done()
          
            $(progress_ix).addClass('complete')
            $(message_ix).html('done')
            $(progress_ix).css('width', '100%')

            # Load product form by ajax
            $.ajax
              url: '/sons/' + file.id + '/form?position=' + ix
              type: 'GET'
              success: (data) ->
                # Hide the ajax-loader
                $("#file-forms .loading").hide()
                showAjaxLoader = false
                
                current_form = $(file_form_ix)

                current_form.html(data)
                
                current_form.find('.carousel').carousel({ interval: false })

                current_form.find("form").live 'submit', (e) ->
                  e.preventDefault()

                  # Change the button value when submit the form
                  current_form.find('#form-buttons input[type="submit"]').attr('disabled', 'disabled')
                  current_form.find('#form-buttons input[type="submit"]').attr('value', 'Enviando...')

      send: (e, data) ->
        # console.log('send')

        # Show the ajax-loader
        $("#file-forms .loading").show() if showAjaxLoader

        # Add the css class dropped
        $("#fileupload").addClass("dropped")

      change: doUpload
      
      drop: doUpload

      progress: (e, data) ->
        # console.log('progress')

        progress = new Array()
        $.each data.files, (index, file) ->
          ix = jQuery.inArray(file.name, files)
          progress[ix] = parseInt(data.loaded / data.total * 100, 10).toString()

          $("#progress_" + ix).css('width', progress[ix] + '%')
          $("#message_" + ix).html(progress[ix] + '%')
          
          if progress[ix] == '100'
            $("#message_" + ix).html('finishing up')

      fail: (e, data) ->
        # console.log('fail')

        $.each data.files, (index, file) ->
          ix = jQuery.inArray(file.name, files)
          $("#progress_" + ix).addClass('error')
          $("#message_" + ix).html('erro')
          that.files[ix].error()
          $("#file-form-" + ix).remove()
          
          # console.log('fail: '+ix)
          

  closeCurrentFormGoToNext: (ix) ->
    # console.log('closeCurrentFormGoToNext')

    that = @
    $('#file-form-' + ix).hide()
    $('#file_' + ix).removeClass('active')
    $("#message_" + ix).html('edited')
    that.files[ix]['isEdited'] = true
    document.location.href = '#uptop'
    isEditing = false

    # Check if it is necessary to show the finish button
    that.showFinishButton()

    ixint = parseInt(ix)
    if ixint < files.length - 1
      that.edit(ixint + 1)

  showFinishButton: ->
    # console.log('showFinishButton')

    that = @
    countEdited = 0
    countError = 0

    for file in that.files
      if (file.isDone)
        if (file.isEdited)
          countEdited++
        else if (file.isError)
          countError++

    if (((countEdited + countError) == that.files.length) && that.files.length > 0)
      $('#upload-finish-button').show()

  setEvents: ->
    # console.log('setEvents')

    that = @
    $('#upload-finish-button').hide()
    $("[rel='product-form']").live 'submit', (e) ->
      
      e.preventDefault()
              
      checkAllDone = ->
        # console.log('checkAllDone')

        done = 0
        $.each that.files, (index, file) ->
          if !file.done && !file.error
            return false
          else if file.isDone
            done++

        if $('.edited-form').length < done
          return false

        return true
    
    $(".file-container a").live 'click', (e) ->
      a = $(this)
      that.edit(a.data("index"))
      e.preventDefault()
      
    $('#upload-finish-button').live 'click', (e) ->  
      e.preventDefault()

      # Show the ajax loader
      $("#next-multiple-product .loading").show()
      
      # if no forms were edited, redirect
      if $('.edited-form').length == 0
        window.location.href = $("#upload-finish-button").attr("href")
      
      isAllEdited = ->
        # console.log('isAllEdited')

        if $('.edited-form').length == 0
          return false
        window.location.href = $("#upload-finish-button").attr("href")
        
      setTimeout isAllEdited, 1000
      
    $('#form-buttons a').live 'click', (e) ->
      for item in $('.file-form') 
        $(item).hide()
      for item in $('.file-container')
        $(item).removeClass('active')
      isEditing = false
      

    #$('.product-image-select').live 'click', (e) ->
    #  ids = $(this).attr('rel').split('_')
    #  imageid = ids[0]
    #  productid = ids[1]
    #  #console.log('imageid: '+imageid+' / productid: '+productid)
    #  $('#edit_product_'+productid).find('.carousel-inner').find('img').toggleClass('selected', false)
    #  $(this).addClass('selected')
    #  $('#edit_product_'+productid).find('#product_image_id').val(imageid)
      

  edit: (ix) ->
    # console.log('edit')

    that = @
    for item in $('.file-form') 
      $(item).hide()
    for item in $('.file-container')
      $(item).removeClass('active')
      
    $("#file-form-" + ix).show() 
    $('#file_' + ix).addClass('active')
    isEditing = true

class window.File
  constructor: (fileObject, ix) ->
    @fileObject = fileObject
    @isDone = false
    @isError = false
    @isEdited = false
    @index = ix
  
  done: ->
    @isDone = true
    
  error: ->
    @isError = true
    

