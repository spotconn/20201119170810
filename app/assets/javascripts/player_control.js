function load_player(id, media){
	
	$("#jplayer_"+id).jPlayer({
	  ready: function () { $(this).jPlayer("setMedia", { mp3: media }); },
	  swfPath: "/assets/vendor/jplayer/",
	  supplied: "mp3",
	  cssSelectorAncestor:"#jp_interface_"+id,
	  backgroundColor: "transparent",
	  play:function(e){
	    show_player($("#jplayer_container_"+id));
	    $(this).jPlayer("pauseOthers");
	  },
	  pause:function(e){
	    hide_player($("#jplayer_container_"+id));
	  }
	});
	hide_player($("#jplayer_container_"+id))
}

function hide_player(player){
  player.parent().siblings(".product_price").show();
  player.find(".jp-progress-container").hide();
  player.find(".jp-volume-bar-container").hide();
  player.find("a.jp-mute").hide();
  player.find("a.jp-unmute").hide();
  player.find(".jp-text").fadeIn(100);
  player.find(".jp-audio").animate({ width: "90px" }, 150);
}

function show_player(player){
  player.parent().siblings(".product_price").hide();
  player.find(".jp-text").fadeOut(100);
  player.find(".jp-audio").animate({ width: "205px" }, {
    complete: function() {
      player.find(".jp-progress-container").fadeIn(250);
      player.find(".jp-volume-bar-container").fadeIn(250);
      player.find(".jp-mute").fadeIn(250);
      },
    duration: 150
    });
}

jQuery(function() {
    $(".jp-audio-image").click(function(evt) {
        var player = $(this).siblings(".jp-player").data("jPlayer");
        if (player.status.paused) {
            player.play();
        } else {
            player.pause();
        }
    });
});
