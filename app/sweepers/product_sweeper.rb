class ProductSweeper < ActionController::Caching::Sweeper

  observe Product # This sweeper is going to keep an eye on the Product model
 
  # If our sweeper detects that a Product was created call this
  def after_create(product)
    expire_cache_for(product)
  end
 
  # If our sweeper detects that a Product was updated call this
  def after_update(product)
    expire_cache_for(product)
  end
 
  # If our sweeper detects that a Product was deleted call this
  def after_destroy(product)
    expire_cache_for(product)
  end
 
  private
  def expire_cache_for(product)
    # Expire the index page now that we added a new product
    expire_page(:controller => '/pages', :action => 'index')
    expire_page(:controller => '/users', :action => 'index')

    pages = {'index' => nil, 'artistas' => ['highlight', 'sounds', 'name'], 'categories' => ['rhythm', 'instrument', 'type', 'mood']}
    expire_page_array(pages)

    #expire_page(:controller => '/categories', :action => 'all')
 
    # Expire a fragment
    #expire_fragment('all_available_products')
  end

  def expire_page_array(pages)
    locales = ['', '/en']
    locales.each do |locale| 
      pages.each do |k,v| 
        if !v
          expire_page "#{locale}/#{k}.html"
        else
          v.each_with_index do |w,i|
            expire_page "#{locale}/#{k}.html" if i==0
            expire_page "#{locale}/#{k}/#{w}.html"

            if k=='categories'
              expire_page "#{locale}/#{k}/#{w}/all.html"
            end

          end
        end
      end
    end
  end
end