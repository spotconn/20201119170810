class HomepageHighlightSweeper < ActionController::Caching::Sweeper
  observe HomepageHighlight 
 
  def after_create(highlight)
    expire_cache_for(highlight)
  end
 
  def after_update(highlight)
    expire_cache_for(highlight)
  end
 
  def after_destroy(highlight)
    expire_cache_for(highlight)
  end
 
  private
  def expire_cache_for(highlight)
    expire_page(:controller => '/pages', :action => 'index')
    expire_page '/index.html'
    expire_page '/en/index.html'
    
    # Expire a fragment
    #expire_fragment('all_available_products')
  end
end