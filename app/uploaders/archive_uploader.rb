# encoding: utf-8
require 'digest/sha1'
class ArchiveUploader < CarrierWave::Uploader::Base
  storage ((Rails.env.test? or Rails.env.cucumber? ) ? :file : :fog)
  #storage :fog
  
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}"
  end
  
  def filename
    rand = (0...6).map{65.+(rand(25)).chr}.join
    digest = Digest::SHA1.hexdigest("#{model.user.id}-#{Time.now.to_i}-#{rand}")
    #p " ######################"
    #p " File Upload"
    #p " User ID: #{model.user.id}"
    #p " Timestamp: #{Time.now.to_i}"
    #p " Rand: #{rand}"
    #p " Digest: #{digest}"
    #p " ######################"
    @name ||= "#{digest}.#{file.extension}" if original_filename
  end
  
  def extension_white_list
    %w(wav mp3 aif)
  end
  
end
