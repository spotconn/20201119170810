# encoding: utf-8
class PhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  
  storage ((Rails.env.test? or Rails.env.cucumber? or Rails.env.development?) ? :file : :fog)
  
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end
  
  def extension_white_list
    %w(jpg jpeg gif png)
  end
  
  process :resize_to_fill => [600, 341]
  
  version :thumb do
    process :resize_to_fill => [220,125]
  end
  
end
