# encoding: utf-8
class BackgroundUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  
  storage ((Rails.env.test? or Rails.env.cucumber? or Rails.env.development?) ? :file : :fog)
  
  def store_dir
    "uploads/category/#{model.id}/#{mounted_as}"
  end
  
  def extension_white_list
    %w(jpg jpeg gif png)
  end
  
=begin   
  process :resize_to_fill => [432, 332]
  
  version :thumb_mask do
    process :resize_to_fill => [188,203]
  end
=end
  
  version :thumb do
    process :resize_to_limit => [300, 300]
  end

end
