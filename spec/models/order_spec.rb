require 'spec_helper'

describe Order do
  
  it { should validate_presence_of :user }
  it { should validate_presence_of :price }
  it { should validate_presence_of :amount_paid }
  it { should validate_presence_of :status }
  it { should validate_presence_of :discount }
  it { should validate_presence_of :surcharge }
  
  it { should_not allow_value('a').for(:price) }
  it { should_not allow_value(-1).for(:price) }
  
  it { should_not allow_value('a').for(:amount_paid) }
  it { should_not allow_value(-1).for(:amount_paid) }
  
  describe "#create" do  
    before do
      @buyer_user = create :buyer_user
      @seller_user = create :user
      
      @payment = create :payment, amount: 100, amount_paid: 100, fee: 10, user: @buyer_user
        
      @product = build :product, user: @seller_user, price: 10
    end
    
    context "wrong amount paid" do
      before do
        @order = build :order, user: @buyer_user, product: @product, discount: 0, surcharge: 2, amount_paid: 10
        @order.save
      end
      
      it "should not create an order" do
        Order.count.should == 0
      end
      
      it "artist balance should be zero" do
        @seller_user.account.balance.should == 0
      end

      it "buyer balance should be 100" do
        @buyer_user.account.balance.should == 100
      end
      
    end
    
    context "correct amount paid" do
      before do
        @order = build :order, user: @buyer_user, product: @product, discount: 0, surcharge: 2, amount_paid: 12
        @order.save
      end

      it "should create an order" do
        Order.count.should == 1
      end
      
      it "should create 8 transactions" do
        # 3 transactions = deposit
        # 5 transactions = transfer
        Transaction.count.should == 8
      end
      
      it "artist balance should be 6" do
        @seller_user.account.balance.should == 6
      end

      it "buyer balance should be 88" do
        @buyer_user.account.balance.should == 88
      end
      
      
      describe "delete order" do
        before do
          @order.destroy
          @seller_user.reload
          @buyer_user.reload
        end

        it "should remove order" do
          Order.count.should == 0
        end

        it "should remove transfers" do
          # 3 transactions = deposit
          Transaction.count.should == 3
        end

        it "artist balance should be zero" do
          @seller_user.account.balance.should == 0
        end

        it "buyer balance should be 100" do
          @buyer_user.account.balance.should == 100
        end

      end
      
      
    end
    
  end
  
end
