require 'spec_helper'

describe Transfer do  
  
  it { should_not allow_value(nil).for(:source) }
  it { should_not allow_value("").for(:source) }
  it { should_not allow_value(nil).for(:destination) }
  it { should_not allow_value("").for(:destination) }
  it { should_not allow_value(nil).for(:order) }
  it { should_not allow_value("").for(:order) }

  describe "#create" do  
    before do
      @buyer_user = create :user
      @seller_user = create :user
      
      @buyer_account = @buyer_user.account
      @seller_account = @seller_user.account
      
      @product = build :product, user: @seller_user, price: 10
    end

    context "buyer does not have enough credit" do
      before do
        @order = build :order, user: @buyer_user, product: @product, discount: 0, surcharge: 2, amount_paid: 12
        @order.save
      end

      it "should not create transactions" do
        Transaction.count.should == 0
      end

      it "should not change the buyer's account" do
        @buyer_account.reload.balance.should == 0
      end
      
      it "should not change the seller's account" do
        @seller_account.reload.balance.should == 0
      end

      it "should not change marketplace account" do
        Account::marketplace.reload.balance.should == 0
      end

    end
 
    context "buyer has enough credit - with surcharge" do
      before do
        @payment = create :payment, amount: 100, amount_paid: 100, fee: 10, user: @buyer_user
        @order = build :order, user: @buyer_user, product: @product, discount: 0, surcharge: 2, amount_paid: 12
        @order.save
      end
      
      it "should change the Transaction count by 8" do
        # 4 for the transfer, 3 for the deposit, 1 for surcharge
        Transaction.count.should == 8
      end
      
      it "should change the buyer's account to 88" do
        # 100 - 12 (balance - price)
        @buyer_account.reload.balance.should == 88
      end
      
      it "should change the seller's account to 6" do
        # $10 - 40% commission
        @seller_account.reload.balance.should == 6
      end
      
      it "should change the marketplace's account to -4" do
        # after deposit, market place account is -10 because of the fee parameter in the payment
        # 40% commission + $2 surcharge
        Account::marketplace.reload.balance.should == -4
      end
    end

    context "buyer has enough credit - no surcharge" do
      before do
        @payment = create :payment, amount: 100, amount_paid: 100, fee: 10, user: @buyer_user
        @order = build :order, user: @buyer_user, product: @product, discount: 0, surcharge: 0, amount_paid: 10
        @order.save
      end

      it "should change the Transaction count by 7" do
        # 4 for the transfer, 3 for the deposit
        Transaction.count.should == 7
      end

      it "should change the buyer's account to 90" do
        @buyer_account.reload.balance.should == 90
      end

      it "should change the seller's account to 6" do
        @seller_account.reload.balance.should == 6
      end

      it "should change the marketplace's account to -6" do
        # after deposit, market place account is -10 because of the fee parameter in the payment
        Account::marketplace.reload.balance.should == -6
      end
    end

  end
end
