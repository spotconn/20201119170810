require 'spec_helper'

describe BaseTransaction do
  it { should_not allow_value(nil).for(:amount) }
  it { should_not allow_value("").for(:amount) }
  it { should_not allow_value('a').for(:amount) }
  it { should_not allow_value(-1).for(:amount) }
  
end