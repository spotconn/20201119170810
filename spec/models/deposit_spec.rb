require 'spec_helper'

describe Deposit do

  it { should_not allow_value(nil).for(:destination) }
  it { should_not allow_value("").for(:destination) }
  it { should_not allow_value(nil).for(:payment) }
  it { should_not allow_value("").for(:payment) }

  describe "create payment" do

    before do
      @payment = create :payment, amount: 100, amount_paid: 100, fee: 10
      @account = @payment.user.account
    end

    it "should create 3 transactions" do
      Transaction.count.should == 3
    end

    it "should add 100 into account" do
      @account.balance.should == 100
    end

    it "should debit 10 from marketplace account" do
      Account::marketplace.balance.should == -10
    end

    it "should credit 10 to paypal account" do
      Account::paypal.balance.should == 10
    end

  end
end
