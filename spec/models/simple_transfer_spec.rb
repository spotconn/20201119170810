require 'spec_helper'

describe SimpleTransfer do  
  
  it { should_not allow_value(nil).for(:source) }
  it { should_not allow_value("").for(:source) }
  it { should_not allow_value(nil).for(:destination) }
  it { should_not allow_value("").for(:destination) }
  it { should_not allow_value(nil).for(:amount) }
  it { should_not allow_value("").for(:amount) }

  describe "#create" do  
    before do
      @debit_user = create :user
      @credit_user = create :user
      
      @debit_account = @debit_user.account
      @credit_account = @credit_user.account
      
    end

    context "simple transfer" do
      before do
        @transfer = SimpleTransfer.new
        @transfer.source = @debit_account
        @transfer.destination = @credit_account
        @transfer.description = "simple transfer"
        @transfer.amount = 10
        @transfer.created_at = '2012-05-29'
        @transfer.create
      end

      it "should change the Transaction count by 2" do
        # 4 for the transfer, 3 for the deposit
        Transaction.count.should == 2
      end

      it "should change the debit's account to -10" do
        @debit_account.reload.balance.should == -10
      end

      it "should change the credit's account to 10" do
        @credit_account.reload.balance.should == 10
      end

    end

  end
end
