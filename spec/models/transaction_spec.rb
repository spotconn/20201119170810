require 'spec_helper'

describe Transaction do
  
  it { should validate_presence_of :account }
  it { should validate_presence_of :transaction_type }
  it { should validate_presence_of :debit_credit }
  it { should validate_presence_of :amount }
  it { should validate_presence_of :description }
  
  describe "#update_account" do
    context "when account has not enough balance" do
      before do
        @user = create :user
        @account = @user.account
      end
      
      it do
        expect { Transaction.create account: @account, debit_credit: "D", amount: "10" }.to_not change(@account, :balance).by(-10)
      end
      
    end
    
    context "when account has enough balance" do
      before do
        @user = create :user
        @account = @user.account
        @account.balance = 100
        @account.save
      end

      it do
        expect { Transaction.create account: @account, debit_credit: "D", amount: "10", transaction_type: "DEBIT", description: "TEST" }.to change(@account, :balance).by(-10)
      end

    end
    
  end
  
  
end
