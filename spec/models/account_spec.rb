require 'spec_helper'

describe Account do

  it { should have_many :transactions }
  it { should_not allow_value('a').for(:balance) }

  describe "#update_balance!" do
    before do
      @user = create( :user )
      @account = @user.account
      @account.update_balance!(5)
    end
    
    it "should add 5 to balance" do
      @account.balance.should == 5
    end
  end
end