require 'spec_helper'

describe Product do
  
  it { should validate_presence_of :name }
  it { should validate_presence_of :price }
  it { should validate_presence_of :archive }
  it { should validate_presence_of :user }
  
  it { should_not allow_value('a').for(:price) }
  it { should_not allow_value(-1).for(:price) }
  it { should_not allow_value(200.01).for(:price) }
  
  it "should have all root categories must have children" do
    category_child_1 = create :category_child_1

    product = create :product
    product.categories = []
    product.categories << category_child_1.parent
    product.save.should be_false
    product.should have(1).error_on(:category_ids)

    product = create :product
    product.categories = []
    for category_root in Category.roots
      product.categories << category_root.children
    end
    product.save.should be_true
  end

  it "should not validate category_ids when create" do
    product = build :product
    product.categories = []
    product.save.should be_true
  end

  it "should validate category_ids when update only" do
    product = create :product
    product.categories = []
    product.save.should be_false
  end

  describe "searching", search: true do
    before(:each) do
      @product = create :product, description: "DESCRIPTION"
      Product.solr_reindex
    end

    it "should search for the product name" do
      name = @product.name
      results = Product.search { fulltext name }.results
      results.first.should == @product
    end

    it "should search for the product's categories" do
      categories = @product.categories.collect(&:name)
      results = Product.search { fulltext categories }.results
      results.first.should == @product
    end

    it "should search for the author's name" do
      author = @product.user.name
      results = Product.search { fulltext author }.results
      results.first.should == @product
    end

    it "should search for the product's description" do
      description = @product.description
      results = Product.search { fulltext description }.results
      results.first.should == @product
    end
  end

  describe "create sample" do
    let(:product) { build :product }
    it "should not change the status when generating sample" do
      status = 'active'
      product.change_status(status)

      product.sample.should be_nil
      product.generate_sample.should be_true
      product.status.should == status
      product.sample.should_not be_nil
    end

    it "should change the status when creating sample" do
      product.change_status('review')

      product.sample.should be_nil
      product.create_sample.should be_true
      product.status.should == 'review'
      product.sample.should_not be_nil
    end
  end
end
