require 'spec_helper'

describe User do

  it { should have_many :products }
  it { should have_one :account }
  it { should validate_presence_of :name }
  it { should validate_presence_of :country }
  it { should validate_presence_of :city }

  describe "#accept_buyer_terms!" do
    before do
      @user = create :user
    end

    subject { @user.accept_buyer_terms! }
    it { expect { subject }.to change(@user, :accepted_buyer_terms_at) }
  end

  describe "#accepted_buyer_terms?" do
    context "terms accepted" do
      before do
        @user = create :user
        @user.accept_buyer_terms!
      end
      subject { @user.accepted_buyer_terms? }
      it { should be_true }
    end

    context "terms not accepted" do
      before do
        @user = create :user
      end
      subject { @user.accepted_buyer_terms? }
      it { should be_false }
    end
  end

  describe "#accept_seller_terms!" do
    before do
      @user = create :user
    end

    subject { @user.accept_seller_terms! }
    it { expect { subject }.to change(@user, :accepted_seller_terms_at) }
  end

  describe "#accepted_seller_terms?" do
    context "terms accepted" do
      before do
        @user = create :user
        @user.accept_seller_terms!
      end
      subject { @user.accepted_seller_terms? }
      it { should be_true }
    end

    context "terms not accepted" do
      before do
        pending "Changed"
        @user = create :user
      end
      subject { @user.accepted_seller_terms? }
      it { should be_false }
    end
  end

end
