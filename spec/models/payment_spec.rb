require 'spec_helper'

describe Payment do
  
  it { should validate_presence_of :user }
  it { should validate_presence_of :amount }
  it { should_not allow_value('a').for(:amount) }
  it { should_not allow_value(-1).for(:amount) }
end