require 'spork'
#uncomment the following line to use spork with the debugger
#require 'spork/ext/ruby-debug'

ENV["RAILS_ENV"] ||= 'test'
if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start :rails
end

Spork.prefork do
  # Loading more in this block will cause your tests to run faster. However,
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.
  require File.expand_path("../../config/environment.rb",  __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'
  require 'webmock/rspec'
  require 'database_cleaner'
  require 'shoulda/matchers/integrations/rspec'
  require 'factory_girl'
  require 'sunspot_test/rspec'
  require 'sidekiq/testing/inline'

  WebMock.disable_net_connect! :allow_localhost => true

  Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

  RSpec.configure do |config|
    config.filter_run :wip => true
    config.run_all_when_everything_filtered = true
    config.infer_base_class_for_anonymous_controllers = false
    config.include Devise::TestHelpers, :type => :controller
    config.include FactoryGirl::Syntax::Methods
    
    config.before :suite do
      DatabaseCleaner.strategy = :truncation
      DatabaseCleaner.clean_with :truncation
    end

    config.before do
      DatabaseCleaner.start
    end

    config.after do
      DatabaseCleaner.clean
    end
    
    config.after :suite do
      FileUtils.rm_rf(Rails.root.join("public/uploads/tmp"))
    end

  end

end

Spork.each_run do
  # This code will be run each time you run your specs.
  ActiveSupport::Dependencies.clear if ENV['DRB'] == 'true'
  Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }
end