
FactoryGirl.define do
  factory :order do
    association(:user, factory: :buyer_user)
    association :product
    price "10.00"
    amount_paid "10.00"
    payment_method "Paypal"
    status "A"
    discount "0.0"
    surcharge "0.0"
  end
end
