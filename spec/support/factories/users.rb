# encoding: UTF-8
FactoryGirl.define do
  factory :user do
    full_name "Alberto Roberto"
    name "alberto roberto"
    #email "teste@polomarte.com"
    sequence(:email) { |n| "test#{n}@polomarte.com" }
    password "123456"
    password_confirmation "123456"
    city "Rio de Janeiro"
    country "Brasil"
    accepted_seller_terms_at "2005-01-01"
    cpf "48932048746"
  end
  
  factory :another_user, :parent => :user do
    full_name "Paulo Marcio"
    name "paulo marcio"
    #email "paulo@polomarte.com"
    city "Macapá"
    cpf "30587480351"
  end

  factory :buyer_user, :parent => :user do
    full_name "José Silva"
    name "jose silva"
    #email "jose@polomarte.com"
    city "Sao Paulo"
    cpf "47277894340"
  end
end
