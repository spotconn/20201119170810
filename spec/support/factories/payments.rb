
FactoryGirl.define do
  factory :payment do
    association :user, factory: :buyer_user
    amount "100.00"
    amount_paid { |payment| payment.amount }
    payment_method "Paypal"
    merchant_data "id:100002"
    merchant_transaction_id "1000101"
    status "A"
  end
end