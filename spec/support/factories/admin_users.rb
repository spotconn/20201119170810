# encoding: UTF-8
FactoryGirl.define do
  factory :admin_user do
    sequence(:email) { |n| "test#{n}@polomarte.com" }
    password "123456"
    password_confirmation "123456"
  end
  
  factory :another_admin_user, parent: :admin_user do
    email "paulo@polomarte.com"
    password "123456"
    password_confirmation "123456"
  end
end
