# encoding: UTF-8
FactoryGirl.define do
  factory :product do    
    name "test product 1"
    price "10.00"
    association :user
    category_ids { [FactoryGirl.create(:category).id] }
    archive File.new(File.join(Rails.root, "spec", "support", "files", "sound.wav"))
    photo File.new(File.join(Rails.root, "spec", "support", "files", "image.jpg"))
    is_public true
    allow_commercial_use true
    bpm 150
    key "A"
  end
end