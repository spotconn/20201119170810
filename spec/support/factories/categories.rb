# encoding: UTF-8

FactoryGirl.define do
  factory :category_root, :class => Category do
    sequence(:name) { |n| "Root Category #{n}" }
    sort_by 1
  end

  factory :category do
    sequence(:name) { |n| "Category #{n}" }
    association(:parent, :factory => :parent_category)
  end
  
  factory :parent_category, :class => Category do
    sequence(:name) { |n| "Parent Category #{n}" }
    association :parent, :factory => :category_root
  end
  
  factory :guitar_category, :class => Category do
    name "Guitar"
    parent { |category| category.parent = Factory :parent_category, :name => "Instruments" }
  end

  factory :category_child_1, :class => Category do
    sequence(:name) { |n| "Child Category #{n}" }
    association :parent, :factory => :category_root
    sort_by 1
  end

  factory :category_child_2, :class => Category do
    sequence(:name) { |n| "Child Category #{n}" }
    association :parent, :factory => :category_root
    sort_by 2
  end
end