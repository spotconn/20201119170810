module RequestExtensions
  def get(*args)
    super *with_locale(args)
  end
  
  def post(*args)
    super *with_locale(args)
  end
  
  def with_locale(args)
    if args.length==1
      args << { locale: I18n.default_locale } 
    else
      args[1][:locale] = args[1][:locale] || I18n.default_locale
    end
    args
  end
end

RSpec.configure do |c|
  c.include RequestExtensions, :type => :controller
end