require 'spec_helper'

describe OrdersController do

  context "logged out" do
    describe "GET new" do
      before { get :new, :product_id => 1 }
      it { should redirect_to new_user_session_path }
    end

    describe "POST create" do
      before { post :create, :product_id => 1 }
      it { should redirect_to new_user_session_path }
    end
  end

  context "logged in" do
    before do
      
      @product = create(:product)
      @user = @product.user
      @user.confirm!
      sign_in(@user)
    end


    describe "GET new" do

      context "user did not accept buyer terms" do
        before do
          get :new, product_id: @product.id
        end

        it { should redirect_to new_buyer_term_path }
      end
      
      context "user accepted buyer terms - not enough balance" do
        before do
          @user.accept_buyer_terms!
          get :new, product_id: @product.id
        end
        it { should redirect_to purchase_payment_path(@product.id) }
        it { should assign_to(:product).with(@product) }
      end
      
      context "user accepted buyer terms - with enough balance" do
        before do
          @user.accept_buyer_terms!
          
          @payment = Payment.create(user: @user, amount: 100.0, amount_paid: 100.0)
          @deposit = Deposit.new
          @deposit.destination = @user.account
          @deposit.amount = 100.0
          @deposit.payment = @payment 
          @deposit.description = "hello"
          @deposit.create
          
          get :new, product_id: @product.id
        end
        
        it { should render_template :new }
        it { should assign_to(:product).with(@product) }
      end
    end

    describe "POST create" do

      context "user cancelled order" do
        before do
          @user.accept_buyer_terms!
          post :create, product_id: @product.id, cancel_button: true
        end
        it { should redirect_to user_product_path(@user, @product) }
      end

      context 'user confirmed order' do

        context "user accepted buyer terms" do
          before do
            @user.accept_buyer_terms!
          end
          
          context "when user doesn't have enough credit" do
            
            before do
              @order = double("order")
              controller.current_user.stub_chain(:orders, :new).with({product: @product, price: 10.0, amount_paid: 10.0, status: 'N', payment_method: 'None'}).and_return(@order)
              post :create, product_id: @product.id
            end
            
            it { should redirect_to purchase_payment_path(10.0, @product.id) }
          end
          
          context " when user has enough credit" do
            
            before do
              @payment = Payment.create(user: @user, amount: 100.0, amount_paid: 100.0)
              @deposit = Deposit.new
              @deposit.destination = @user.account
              @deposit.amount = 100.0
              @deposit.payment = @payment 
              @deposit.description = "hello"
              @deposit.create
              @order = double("order")
            end
            
            context "when the attributes are valid" do
              before do
                @order.should_receive(:send_emails).and_return(true)
                @order.should_receive(:save).and_return(true)
                controller.current_user.stub_chain(:orders, :new).with(hash_including({ product: @product, price: 10.0, status: 'N', payment_method: 'None', discount: 0})).and_return(@order)
                @order.should_receive(:surcharge=).with(0)
                @order.should_receive(:surcharge).and_return(0)
                @order.should_receive(:amount_paid=).with(10)
                @order.should_receive(:allow_commercial_use=).with(true)
                post :create, product_id: @product.id
              end

              it { should redirect_to user_path(@user) }

            end

            context "when the attributes are not valid" do
              before do
                @order = double("order")
                @order.should_receive(:save).and_return(false)
                @order.stub_chain(:errors, :any?)
                controller.current_user.stub_chain(:orders, :new).with(hash_including({ product: @product, price: 10.0, discount: 0.0, status: 'N', payment_method: 'None' })).and_return(@order)
                
                @order.should_receive(:surcharge=).with(0)
                @order.should_receive(:surcharge).and_return(0)
                @order.should_receive(:amount_paid=).with(10)
                @order.should_receive(:allow_commercial_use=).with(true)
                post :create, product_id: @product.id
              end
              it { should render_template :new }
            end
          end
        end

        context "user did not accept buyer terms" do
          before do
            post :create, product_id: @product.id
          end

          it { should redirect_to new_buyer_term_path }
        end
      end
    end

  end
end
