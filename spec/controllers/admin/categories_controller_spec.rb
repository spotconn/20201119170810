require 'spec_helper'

describe Admin::CategoriesController do

  context "logged out" do
    describe "GET index" do
      before do
        get :index
      end
      it { should redirect_to new_admin_user_session_path }
    end

    describe "GET new" do
      before { get :new }
      it { should redirect_to new_admin_user_session_path }
    end

    describe "POST create" do
      before { post :create }
      it { should redirect_to new_admin_user_session_path }
    end
  end

  context "logged in" do

    before do
      @admin_user = create(:admin_user)
      sign_in(@admin_user)
    end

    describe "GET roots" do
      before do
        @categories = double('categories')
        Category.should_receive(:roots).and_return(@categories)
        get :roots
      end

      it { should render_template :roots }
      it { should assign_to(:categories).with(@categories) }
    end

    describe "GET new" do
      before { get :new }
      it { should render_template :new }
      it { should assign_to(:category).with_kind_of(Category) }
    end

    describe "POST create" do
      context "when the attributes are valid" do
        before do
          @category = double('category')
          @category.should_receive(:save).and_return(true)

          Category.should_receive(:new).with("VALID").and_return(@category)

          post :create, category: "VALID"
        end
        it { should set_the_flash.to("category added") }
        it { should redirect_to admin_categories_path }
      end

      context "when the attributes are not valid" do
        before do
          @category = double('category')
          @category.should_receive(:save).and_return(false)

          Category.should_receive(:new).with("INVALID").and_return(@category)

          post :create, category: "INVALID"
        end
          it { should render_template :new }
          it { should_not set_the_flash.to("category added") }
      end
    end
  end
end
