require 'spec_helper'

describe SellerTermsController do
  context "logged out" do
    describe "POST create" do

      before { post :create }
      it { should redirect_to new_user_session_path }
    end

  end

  context "logged in" do

    before do
      @user = create(:user)
      @user.confirm!
      sign_in(@user)
    end

    context "session redirect_to is set" do

      before do
        session[:return_to] = '/1'
      end

      describe "POST create" do
        before { post :create }
        it { should redirect_to '/1' }
      end

    end

    context "session redirect_to is not set" do

      describe "POST create" do
        before { post :create }
        it { should redirect_to user_path(@user) }

      end

    end

  end
end
