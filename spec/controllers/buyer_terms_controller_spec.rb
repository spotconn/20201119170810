require 'spec_helper'

describe BuyerTermsController do

  context "logged out" do
    describe "POST create" do

      before { post :create }
      it { should redirect_to new_user_session_path }
    end

  end

  context "logged in" do

    before do
      @user = create(:user)
      @user.confirm!
      sign_in(@user)
    end

    describe "POST create" do
      context 'user declines terms' do

        before do
          request.env["HTTP_REFERER"] = 'http://google.com'
          post :create, decline_button: true
        end

        it { should redirect_to :back }

      end

      context 'user accpets terms' do

          context "session redirect_to is set" do

            before do
              session[:return_to] = '/1'
              post :create
            end

            it { should redirect_to '/1' }

          end

          context "session redirect_to is not set" do

            before { post :create }
            it { should redirect_to user_path(@user) }

          end

      end

    end
  end

end
