require 'spec_helper'

describe ProductsController do
  
  before do
    I18n.locale='en'
  end

  describe "GET show" do
    before do
      @user = double("user", name: "test user", :similar_user => nil)
      @product = double("product", name: "test product", :id => 1, user: @user, categories: ["category2", "category1"], archive_data: "{\"Channels\":\"1\"}")

      Category.should_receive(:sort_by_ancestry).with(["category2", "category1"]).and_return(["category1", "category2"])

      Product.should_receive(:find).with("1").and_return(@product)
      get :show, :id => @product.id
    end

    it { should render_template :show }
    it { should assign_to(:categories).with(["category1", "category2"]) }
    it { should assign_to(:product).with(@product) }
    it { should assign_to(:user).with(@user) }
    
  end

  context "logged out" do
    describe "GET new" do

      before { get :new }
      it { should redirect_to new_user_session_path }

    end

    describe "POST create" do

      before { post :create }
      it { should redirect_to new_user_session_path }

    end

  end

  context "logged in" do

    before do
      @user = create(:user)
      @user.confirm!
      sign_in(@user)
    end

    describe "FORM new" do
      before do
        @product = create :product
      end

      it "should accept xhr request" do
        xhr :get, :form, :product_id => @product.id
        should render_template :form
        response.should_not render_template("layouts/application")
      end

      it "should not accept GET request" do
        get :form, :product_id => @product.id
        should redirect_to root_path
      end
    end

    describe "GET new" do
      context "user accepted seller terms" do
        before do
          @user.accept_seller_terms!
          get :new
        end

        it { should render_template :new }
        it { should assign_to(:product).with_kind_of(Product) }

        it "should assign product with user as current_user" do
          assigns(:product).user.should == @user
        end
      end

      context "user has not accepted seller terms" do
        before do
          pending "changed"
          get :new
        end

        it { should redirect_to new_seller_term_path }

      end

    end

    describe "POST create" do
      context "user accepted seller terms" do
        before do
          @user.accept_seller_terms!
        end

        context "when the attributes are valid" do
          before do
            @product = double("product")
            @product.should_receive(:save).and_return(true)

            controller.current_user.stub_chain(:products, :new).with("VALID").and_return(@product)

            post :create, product: "VALID"
          end

          it { should redirect_to user_path(@user) }
          it { should set_the_flash.to(I18n.t(:product_added)) }
        end

        context "when the attributes aren't valid" do
          before do
            @product = double("product")
            @product.should_receive(:save).and_return(false)

            controller.current_user.stub_chain(:products, :new).with("INVALID").and_return(@product)

            post :create, product: "INVALID"
          end

          it { should render_template :new }
          it { should_not set_the_flash.to(I18n.t(:product_added)) }
        end
      end

      context "user has not accepted seller terms" do
        before do
          pending "Changed"
          post :create
        end

        it { should redirect_to new_seller_term_path }

      end
    end
  end

end
