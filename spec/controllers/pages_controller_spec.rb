require 'spec_helper'

describe PagesController do
  describe "GET index" do
    before do
      pending "changed"
      @products = double("product")
      Product.should_receive(:all).and_return(@products)
      get :index
    end
    
    it { should render_template :index }
    it { should assign_to(:products).with(@products) }
  end
end
