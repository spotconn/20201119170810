require 'spec_helper'

# Specs in this file have access to a helper object that includes
# the Admin::CategoriesHelper. For example:
#
# describe Admin::CategoriesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       helper.concat_strings("this","that").should == "this that"
#     end
#   end
# end


module Admin
  describe CategoriesHelper do
    describe "#nested_categories" do
      context "when the category has children" do
        it "should recusively render the categories" do
          pending "Not yet implemented"
        end
      end
    
      context "when the category does not have children" do
        it "should return an empty string" do
        end
      end
    end
  end
end
