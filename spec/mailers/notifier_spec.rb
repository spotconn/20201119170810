#encoding: utf-8

require "spec_helper"

describe Notifier do
  before(:each) do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
  end

  after(:each) do
    ActionMailer::Base.perform_deliveries = false
  end

  context "sending email to administrators for review" do
    before(:each) do
      #@admin = create :admin_user
      @product = create :product
      @mail = ActionMailer::Base.deliveries.last
    end

    it "should send email to administrators when created" do
      @mail.subject.should == "ORitmo - Review: #{@product.name}"
      @mail.to.should include "play@oritmo.com"
    end

    it "should include a link to the product" do
      @mail.body.should include admin_product_url(@product.id)
    end

  end

  context "sending emails after successful signup" do
    before(:each) do
      @user = create :user
      @mail = ActionMailer::Base.deliveries
    end

    it "should send email to administrators when created" do
      @mail[0].subject.should == "ORitmo - Novo cadastro: #{@user.name}"
      @mail[0].to.should include "play@oritmo.com"
    end

    it "should send email to user when created" do
      @mail[1].subject.should == "Bem vindo ao ORITMO"
      @mail[1].to.should include @user.email
    end
  end

end
