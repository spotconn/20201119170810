source "https://rubygems.org"

gem 'puma'
gem 'rails', '~> 3.2.22'
gem 'rake', '~> 10.0.0'
gem 'jquery-rails'
gem 'bson_ext'
gem 'haml'
gem 'devise'
gem 'omniauth'
gem 'inherited_resources'
gem 'simple_form'
gem 'country_select'
gem 'mysql2', '~> 0.3.11'
gem 'paranoia'
gem 'rails_config'
gem 'carrierwave'
gem 'fog'
gem 'ancestry'
gem 'activemerchant', :require => 'active_merchant'
gem 'exceptional'
gem 'video_info', '~> 2.5.0'
gem 'mini_magick'
gem 'friendly_id', "~> 4.0.9"
#gem 'resque' # replaced by sidekiq
#gem 'resque-retry'
gem 'aws-sdk'
#gem 'carrierwave_backgrounder'
gem 'compass-rails'
gem 'breadcrumbs_on_rails'
gem 'brazilian-rails'
gem 'sunspot_rails'
gem 'sunspot_solr'
gem 'exception_notification'
gem 'will_paginate', '~> 3.0'
gem 'bootstrap-will_paginate'
gem 'useragent'
gem 'active_paypal_adaptive_payment'
gem 'certified'
gem 'newrelic_rpm', '~> 3.5.0'
gem 'rails-translate-routes'
gem 'multi_xml', '0.5.2' # Security fail fix
gem 'globalize', '~>3.0.5'
gem "twitter-bootstrap-rails"
gem 'sidekiq'
gem 's3_direct_upload', git: "git://github.com/gbrun/s3_direct_upload.git"
gem 'test-unit', '~> 3.0'

group :development do
  gem 'rvm-capistrano'
  gem 'capistrano'
  gem 'capistrano-ext'
  gem 'quiet_assets'
  # gem 'vigoo', path: File.expand_path("../vigoo")
  #gem 'rack-mini-profiler'
  gem 'bullet'
  gem "letter_opener"
end

group :assets do
  gem 'sass-rails'
  gem 'coffee-rails'
  gem 'uglifier'
  gem 'therubyracer', '~>0.12.2'
end

group :development, :test, do
  gem 'simplecov', require: false
  gem 'jasmine'
  gem 'rspec-rails'
  gem 'foreman'
  gem 'thin'
  gem 'sunspot_test'
  gem 'guard-spork'
  gem 'guard-rspec'
  gem 'guard-cucumber'
  gem 'pry'
end

group :test do
  gem 'factory_girl'
  gem 'shoulda-matchers'
  gem 'database_cleaner'
  gem 'capybara'
  # gem 'capybara-webkit'
  gem 'pickle'
  gem 'webmock'
  gem 'timecop'
  gem 'cucumber-rails', require: false
end
