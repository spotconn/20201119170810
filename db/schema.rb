# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20200322183024) do

  create_table "accounts", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.float    "balance",    :default => 0.0
    t.datetime "deleted_at"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "accounts", ["deleted_at"], :name => "index_accounts_on_deleted_at"
  add_index "accounts", ["user_id"], :name => "index_accounts_on_user_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.datetime "locked_at"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.string   "role"
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "banners", :force => true do |t|
    t.string   "image_url"
    t.string   "link_url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "file"
    t.string   "status"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "ancestry"
    t.integer  "sort_by",       :default => 0, :null => false
    t.string   "photo"
    t.string   "background"
    t.string   "type"
    t.integer  "product_count", :default => 0
    t.string   "slug"
  end

  add_index "categories", ["ancestry"], :name => "index_categories_on_ancestry"
  add_index "categories", ["slug"], :name => "index_categories_on_slug", :unique => true

  create_table "categories_products", :id => false, :force => true do |t|
    t.integer "product_id"
    t.integer "category_id"
  end

  add_index "categories_products", ["category_id"], :name => "index_categories_products_on_category_id"
  add_index "categories_products", ["product_id", "category_id"], :name => "index_categories_products_on_product_id_and_category_id"
  add_index "categories_products", ["product_id"], :name => "index_categories_products_on_product_id"

  create_table "categories_users", :id => false, :force => true do |t|
    t.integer "category_id"
    t.integer "user_id"
  end

  create_table "category_translations", :force => true do |t|
    t.integer  "category_id"
    t.string   "locale"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "slug"
  end

  add_index "category_translations", ["category_id"], :name => "index_category_translations_on_category_id"
  add_index "category_translations", ["locale"], :name => "index_category_translations_on_locale"

  create_table "collections", :force => true do |t|
    t.string   "name"
    t.string   "slug"
    t.text     "description"
    t.string   "cover"
    t.string   "background"
    t.integer  "user_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "spotify"
    t.string   "rdio"
    t.string   "itunes"
    t.string   "deezer"
  end

  add_index "collections", ["slug"], :name => "index_collections_on_slug"
  add_index "collections", ["user_id"], :name => "index_collections_on_user_id"

  create_table "collections_products", :id => false, :force => true do |t|
    t.integer "collection_id"
    t.integer "product_id"
  end

  add_index "collections_products", ["collection_id", "product_id"], :name => "index_collections_products_on_collection_id_and_product_id"
  add_index "collections_products", ["product_id"], :name => "index_collections_products_on_product_id"

  create_table "friendly_id_slugs", :force => true do |t|
    t.string   "slug",                         :null => false
    t.integer  "sluggable_id",                 :null => false
    t.string   "sluggable_type", :limit => 40
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type"], :name => "index_friendly_id_slugs_on_slug_and_sluggable_type", :unique => true
  add_index "friendly_id_slugs", ["sluggable_id"], :name => "index_friendly_id_slugs_on_sluggable_id"
  add_index "friendly_id_slugs", ["sluggable_type"], :name => "index_friendly_id_slugs_on_sluggable_type"

  create_table "homepage_highlights", :force => true do |t|
    t.string   "section"
    t.integer  "object_id"
    t.string   "object_type"
    t.integer  "position"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "images", :force => true do |t|
    t.string   "name"
    t.string   "photo"
    t.datetime "deleted_at"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "instruments_rhythms", :id => false, :force => true do |t|
    t.integer "instrument_id"
    t.integer "rhythm_id"
  end

  create_table "orders", :force => true do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.float    "price"
    t.float    "amount_paid"
    t.string   "payment_method"
    t.string   "status"
    t.datetime "deleted_at"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.float    "discount"
    t.float    "surcharge"
    t.boolean  "allow_commercial_use", :default => false, :null => false
  end

  add_index "orders", ["deleted_at"], :name => "index_orders_on_deleted_at"
  add_index "orders", ["product_id"], :name => "index_orders_on_product_id"
  add_index "orders", ["user_id"], :name => "index_orders_on_user_id"

  create_table "payments", :force => true do |t|
    t.integer  "user_id"
    t.float    "amount"
    t.float    "amount_paid"
    t.float    "fee"
    t.string   "payment_method"
    t.text     "merchant_data"
    t.string   "merchant_transaction_id"
    t.string   "status"
    t.string   "message"
    t.datetime "deleted_at"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "payments", ["deleted_at"], :name => "index_payments_on_deleted_at"
  add_index "payments", ["merchant_transaction_id"], :name => "index_payments_on_merchant_transaction_id"
  add_index "payments", ["user_id"], :name => "index_payments_on_user_id"

  create_table "products", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "description"
    t.float    "price",                :default => 0.0,   :null => false
    t.string   "file"
    t.string   "status"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.datetime "deleted_at"
    t.string   "archive"
    t.string   "photo"
    t.string   "slug"
    t.text     "archive_data"
    t.boolean  "is_public",            :default => false, :null => false
    t.float    "bpm",                  :default => 0.0
    t.boolean  "allow_commercial_use", :default => true
    t.string   "key"
    t.integer  "image_id"
    t.boolean  "highlight"
  end

  add_index "products", ["deleted_at"], :name => "index_products_on_deleted_at"
  add_index "products", ["slug"], :name => "index_products_on_slug", :unique => true
  add_index "products", ["user_id"], :name => "index_products_on_user_id"

  create_table "regions_rhythms", :id => false, :force => true do |t|
    t.integer "region_id"
    t.integer "rhythm_id"
  end

  create_table "transactions", :force => true do |t|
    t.integer  "account_id"
    t.string   "transaction_type"
    t.string   "debit_credit"
    t.float    "amount"
    t.string   "description"
    t.string   "referable_type"
    t.integer  "referable_id"
    t.datetime "deleted_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "transactions", ["account_id"], :name => "index_transactions_on_account_id"
  add_index "transactions", ["deleted_at"], :name => "index_transactions_on_deleted_at"
  add_index "transactions", ["referable_id"], :name => "index_transactions_on_referable_id"
  add_index "transactions", ["transaction_type"], :name => "index_transactions_on_transaction_type"

  create_table "users", :force => true do |t|
    t.string   "email",                    :default => "", :null => false
    t.string   "username",                 :default => "", :null => false
    t.string   "city",                     :default => "", :null => false
    t.string   "country",                  :default => "", :null => false
    t.string   "encrypted_password",       :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.datetime "deleted_at"
    t.datetime "accepted_seller_terms_at"
    t.datetime "accepted_buyer_terms_at"
    t.string   "name"
    t.string   "paypal_account"
    t.string   "photo"
    t.string   "bio"
    t.string   "slug"
    t.string   "cpf"
    t.string   "cae"
    t.string   "full_name"
    t.string   "filiation"
    t.integer  "product_count",            :default => 0
    t.boolean  "featured"
    t.string   "referred_by"
    t.integer  "internal_rating",          :default => 0
    t.boolean  "musician"
  end

  add_index "users", ["deleted_at"], :name => "index_users_on_deleted_at"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["slug"], :name => "index_users_on_slug", :unique => true

  create_table "videos", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
