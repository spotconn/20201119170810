class AddStoresLinkToCollections < ActiveRecord::Migration
  def change
    add_column :collections, :spotify, :string
    add_column :collections, :rdio, :string
    add_column :collections, :itunes, :string
  end
end
