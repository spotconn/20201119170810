class AddProductCountToCategories < ActiveRecord::Migration
  def up
    add_column :categories, :product_count, :integer, :default => 0
    
    Category.all.each do |category|
      category.update_attributes( :product_count => category.products.is_public.length)
    end
  end

  def down
    remove_column :categories, :product_count
  end
end
