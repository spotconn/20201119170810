class AddAllowCommercialUseToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :allow_commercial_use, :boolean, default: false, null: false
  end
end
