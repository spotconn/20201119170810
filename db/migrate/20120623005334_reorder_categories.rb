class ReorderCategories < ActiveRecord::Migration
  def up
    Category.update_all(:sort_by => 0)
    
    c1 = Category.find_by_name("Ritmo")
    c1.update_attributes(:sort_by => 1) unless c1.nil?

    c2 = Category.find_by_name("Instrumento")
    c2.update_attributes(:sort_by => 2) unless c2.nil?

    c3 = Category.find_by_name("Tipo")
    c3.update_attributes(:sort_by => 3) unless c3.nil?

    c4 = Category.find_by_name("Mood")
    c4.update_attributes(:sort_by => 4) unless c4.nil?
  end

  def down
  end
end
