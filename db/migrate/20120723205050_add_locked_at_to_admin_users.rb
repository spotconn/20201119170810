class AddLockedAtToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :locked_at, :datetime
    add_column :admin_users, :failed_attempts, :integer, :default => 0
    add_column :admin_users, :unlock_token, :string
    add_column :admin_users, :role, :string
    # t.integer   # Only if lock strategy is :failed_attempts
    # t.string   :unlock_token # Only if unlock strategy is :email or :both
    # t.datetime :locked_at
  end
  
end
