class AddFeaturedToUser < ActiveRecord::Migration
  def change
    add_column :users, :featured, :boolean
    add_column :users, :referred_by, :string
  end
end
