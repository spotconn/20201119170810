class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.string :cover
      t.string :background
      t.references :user

      t.timestamps
    end
    add_index :collections, :user_id
    add_index :collections, :slug
  end
end
