class InstrumentsRhythms < ActiveRecord::Migration
  def change
    create_table :instruments_rhythms, :id => false do |t|
      t.references :instrument, :rhythm
    end
  end
end
