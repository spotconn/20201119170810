# This migration comes from vigoo_engine (originally 20120503163413)
class AddBioToUsers < ActiveRecord::Migration
  def change
    add_column :users, :photo, :string
    add_column :users, :bio, :string
    add_column :products, :photo, :string
  end
end
