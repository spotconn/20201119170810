class RegionsRhythms < ActiveRecord::Migration
  def change
    create_table :regions_rhythms, :id => false do |t|
      t.references :region, :rhythm
    end
  end
end
