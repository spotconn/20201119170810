class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :account
      t.string :transaction_type
      t.string :debit_credit
      t.float :amount
      t.string :description
      t.string  :referable_type
      t.integer :referable_id
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
