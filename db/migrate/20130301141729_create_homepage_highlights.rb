class CreateHomepageHighlights < ActiveRecord::Migration
  def change
    create_table :homepage_highlights do |t|
        t.string   :section
        t.integer  :object_id
        t.string   :object_type
        t.integer  :position
        t.timestamps
    end
  end
end
