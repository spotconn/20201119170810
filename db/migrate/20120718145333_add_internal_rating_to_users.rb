class AddInternalRatingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :internal_rating, :integer, :default => 0
  end
end
