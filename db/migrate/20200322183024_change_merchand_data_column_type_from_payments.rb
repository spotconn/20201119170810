class ChangeMerchandDataColumnTypeFromPayments < ActiveRecord::Migration
  def up
    change_column(:payments, :merchant_data, :text)
  end

  def down
    change_column(:payments, :merchant_data, :string)
  end
end
