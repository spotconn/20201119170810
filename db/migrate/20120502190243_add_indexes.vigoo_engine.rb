# This migration comes from vigoo_engine (originally 20120502185110)
class AddIndexes < ActiveRecord::Migration
  def change
    add_index :accounts, :user_id
    add_index :accounts, :deleted_at
    add_index :categories_products, [:product_id, :category_id]
    add_index :categories_products, :product_id
    add_index :categories_products, :category_id
    add_index :orders, :user_id
    add_index :orders, :product_id
    add_index :orders, :deleted_at
    add_index :payments, :user_id
    add_index :payments, :merchant_transaction_id
    add_index :payments, :deleted_at
    add_index :products, :user_id
    add_index :products, :deleted_at
    add_index :transactions, :account_id
    add_index :transactions, :referable_id
    add_index :transactions, :transaction_type
    add_index :transactions, :deleted_at
    add_index :users, :deleted_at
  end
end