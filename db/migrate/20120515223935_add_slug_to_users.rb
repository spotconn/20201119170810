class AddSlugToUsers < ActiveRecord::Migration
  def change
    add_column :users, :slug, :string
    add_index :users, :slug, unique: true
    
    add_column :products, :slug, :string
    add_index :products, :slug, unique: true
  end
end
