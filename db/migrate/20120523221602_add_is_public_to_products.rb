class AddIsPublicToProducts < ActiveRecord::Migration
  class Product < ActiveRecord::Base
  end
  
  def up
    add_column :products, :is_public, :boolean, default: false, null: false
    
    Product.update_all(is_public: true)
  end
  
  def down
    
  end
end