class ChangeColumnBpmToFloat < ActiveRecord::Migration
  def up
    change_column(:products, :bpm, :float)
  end

  def down
    change_column(:products, :bpm, :integer)
  end
end
