class AddSlugToCategoryTranslations < ActiveRecord::Migration
  def change
    add_column :category_translations, :slug, :string
  end
end
