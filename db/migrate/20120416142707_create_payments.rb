class CreatePayments < ActiveRecord::Migration
  def change
      create_table :payments do |t|
        t.references :user
        t.float :amount
        t.float :amount_paid
        t.float :fee
        t.string :payment_method
        t.string :merchant_data
        t.string :merchant_transaction_id
        t.string :status
        t.string :message
        t.datetime :deleted_at
        t.timestamps
      end
  end
end
