class CreateCollectionsProductsJoinTable < ActiveRecord::Migration
  def up
    create_table :collections_products, id: false do |t|
      t.integer :collection_id
      t.integer :product_id
    end
    add_index :collections_products, [:collection_id, :product_id]
    add_index :collections_products, :product_id
  end

  def down
    drop_table :collections_products
  end
end
