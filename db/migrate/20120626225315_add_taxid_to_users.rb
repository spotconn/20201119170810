class AddTaxidToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cpf, :string
    add_column :users, :cae, :string
    add_column :users, :full_name, :string
    add_column :users, :filiation, :string
  end
end
