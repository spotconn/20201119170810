class AddAllowCommercialUseToProducts < ActiveRecord::Migration
  def change
    add_column :products, :allow_commercial_use, :boolean, default: false, null: false
  end
end
