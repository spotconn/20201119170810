class AddHighlightToProducts < ActiveRecord::Migration
  def change
    add_column :products, :highlight, :boolean
  end
end
