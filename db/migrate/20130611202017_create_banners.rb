class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :image_url
      t.string :link_url

      t.timestamps
    end
  end
end
