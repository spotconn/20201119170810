class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user
      t.references :product
      t.float :price
      t.float :amount_paid
      t.string :payment_method
      t.string :status
      t.datetime :deleted_at
      t.timestamps
    end
  end
end
