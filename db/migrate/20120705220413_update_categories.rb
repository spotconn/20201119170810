class UpdateCategories < ActiveRecord::Migration
  def up
    
    Category.find_all_by_ancestry(1).each do |category|
      category.update_attributes(:type => 'Instrument') 
    end
    
    Category.find_all_by_ancestry(2).each do |category|
      category.update_attributes(:type => 'Rhythm') 
    end
    
    Category.find_all_by_ancestry(3).each do |category|
      category.update_attributes(:type => 'Mood') 
    end
    
    Category.find_all_by_ancestry(4).each do |category|
      category.update_attributes(:type => 'Region') 
    end
    
    Category.find_all_by_ancestry(5).each do |category|
      category.update_attributes(:type => 'SoundType') 
    end
    
  end

  def down
    Category.all.each do |category|
      category.update_attributes(:type => nil) 
    end
  end
end
