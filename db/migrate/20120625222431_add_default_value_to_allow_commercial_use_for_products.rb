class AddDefaultValueToAllowCommercialUseForProducts < ActiveRecord::Migration
  def up
    remove_column :products, :allow_commercial_use
    add_column :products, :allow_commercial_use, :boolean, :default => true
  end

  def down
    remove_column :products, :allow_commercial_use
    add_column :products, :allow_commercial_use, :boolean, :default => false
  end
end
