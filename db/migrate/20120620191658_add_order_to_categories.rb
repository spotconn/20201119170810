# encoding: utf-8
class AddOrderToCategories < ActiveRecord::Migration
  def up
    add_column :categories, :sort_by, :integer, default: 0, null: false
    
    Category.find_by_name('Instrumento').try(:update_attributes, :sort_by => 3)
    Category.find_by_name('Ritmo').try(:update_attributes, :sort_by => 2)
    Category.find_by_name('Mood').try(:update_attributes, :sort_by => 4)
    Category.find_by_name('Região').try(:update_attributes, :sort_by => 5)
    Category.find_by_name('Tipo').try(:update_attributes, :sort_by => 1)
    Category.find_by_name('Outro').try(:update_attributes, :sort_by => 1)
  end
  
  def down
    remove_column :categories, :sort_by
  end
end
