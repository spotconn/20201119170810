class AddBpmToProducts < ActiveRecord::Migration
  def change
    add_column :products, :bpm, :integer, :default => 0
  end
end
