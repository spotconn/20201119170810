class AddMorefieldsToCategories < ActiveRecord::Migration
  
  def change
    add_column :categories, :photo, :string
    add_column :categories, :background, :string
    add_column :categories, :type, :string
  end
  
end
