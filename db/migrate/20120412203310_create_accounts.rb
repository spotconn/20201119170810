class CreateAccounts < ActiveRecord::Migration
  def up
    create_table :accounts do |t|
      t.references :user
      t.string :name
      t.float :balance, default: 0
      t.datetime :deleted_at
      t.timestamps
    end
      
     execute <<-SQL
       ALTER TABLE accounts AUTO_INCREMENT=10000
     SQL
  end
  
  def down
    drop_table :accounts
  end
end
