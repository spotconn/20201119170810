class AddAcceptTermsUser < ActiveRecord::Migration
  def change
    add_column :users, :accepted_seller_terms_at, :datetime
    add_column :users, :accepted_buyer_terms_at, :datetime
  end
end
