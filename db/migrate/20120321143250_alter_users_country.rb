class AlterUsersCountry < ActiveRecord::Migration
  def change
    rename_column(:users, :nation, :country)
  end
end
