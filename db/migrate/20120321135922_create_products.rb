class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :user
      t.string :name
      t.string :description
      t.float :price, default: 0, null: false
      t.string :file
      t.string :status
      
      t.timestamps
    end
  end
end
