class AddProductCountTiUsers < ActiveRecord::Migration
  def up
    add_column :users, :product_count, :integer, :default => 0
    
    User.find(:all).each do |user|
      user.update_attributes( :product_count => user.products.is_public.length)
    end
  end

  def down
    remove_column :users, :product_count
  end
end
