class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name
      t.string :photo 
      t.datetime :deleted_at
      t.timestamps
    end
    
    add_column :products, :image_id, :integer
  end
end
