ORitmo::Application.configure do

  # Settings specified here will take precedence over those in config/application.rb

  # Code is not reloaded between requests
  config.cache_classes = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false

  # Compress JavaScripts and CSS
  config.assets.compress = true

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = false
  #config.assets.precompile += ['vigoo.js', 'player_control.js']
  config.assets.precompile += [ 'vigoo.css', 'users/show.css', 'products/form.css', 'users/new.css']

  # Generate digests for assets URLs
  config.assets.digest = true

  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.action_mailer.default_url_options = { :host => 'oritmo.com' }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address              => "smtp.gmail.com",
    :port                 => 587,
    :domain               => 'oritmo.com.br',
    :user_name            => 'no-reply@oritmo.com.br',
    :password             => 'marte123',
    :authentication       => 'plain',
    :enable_starttls_auto => true  }


  config.after_initialize do
     ActiveMerchant::Billing::PaypalExpressGateway.default_currency = 'BRL'
     ActiveMerchant::Billing::PaypalAdaptivePayment.default_currency = 'BRL'
     PAYPAL_OPTIONS = {
       login: "tao_api1.polomarte.com",
       password: "L2HEPWHV2KD2Y5VN",
       signature: "AC2DHFMBTkE9yGQWcMfM6X15uuOkAMqsv3Trq1Fve5FhRnd50mUS8HTO",
       appid: "APP-4T047824X04767421"
     }
  end

  config.middleware.use ExceptionNotifier,
    :email_prefix => "[oritmo error report] ",
    :sender_address => %{"oritmo" <no-reply@oritmo.com>},
    :exception_recipients => %w{ti@polomarte.com mail@theob.me}

  config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect'
end
