ORitmo::Application.configure do
  config.cache_classes = true
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true
  config.serve_static_assets = false
  config.assets.compress = true
  config.assets.compile = false
  #config.assets.precompile += ['vigoo.js', 'player_control.js']
  config.assets.precompile += [ 'vigoo.css']
  config.assets.digest = true
  config.threadsafe! unless $rails_rake_task
  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.action_mailer.default_url_options = { :host => 'staging.oritmo.com' }
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address              => "smtp.gmail.com",
    :port                 => 587,
    :domain               => 'oritmo.com.br',
    :user_name            => 'no-reply@oritmo.com.br',
    :password             => 'marte123',
    :authentication       => 'plain',
    :enable_starttls_auto => true  }
    

  config.after_initialize do
     ActiveMerchant::Billing::Base.mode = :test

     PAYPAL_OPTIONS = {
       login: "seller_1334241522_biz_api1.polomarte.com",
       password: "1334241547",
       signature: "AyvVZLk.8ArkqCP9jcxvIqd7y.7pAcSzB9RxIMNP3qYBTbAm9Ee7eftU"
     }
  end
end


