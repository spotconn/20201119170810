require 'bundler/capistrano'
begin
  require 'sidekiq/capistrano'
  rescue LoadError => e
    p "====================================="
    p "ERROR: can't find sidekiq/capistrano "
    p "====================================="
end

set :stages, %w(production beta staging)
set :default_stage, "staging"
set :application, "oritmo.com"
set :user, "ubuntu"
set :use_sudo, false
set :scm, :git
set :repository, "git@github.com:polomarte/oritmo.git"
set :deploy_to, proc { "/home/ubuntu/#{stage}.#{application}" }
set :deploy_via, :remote_cache
set :keep_releases, 10

ssh_options[:keys] = ["#{ENV['HOME']}/.ssh/id_rsa"]
ssh_options[:forward_agent] = true

default_environment["PATH"] = "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games"

namespace :deploy do
  task :start do ; end
  task :stop do ; end

  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
    run "#{sudo :as => 'ubuntu'} service puma-manager restart"

  end

  task :seed do
    run "cd #{current_path} && #{rake} RAILS_ENV=#{rails_env} db:seed"
  end

  namespace :db do
    task :create do
      run "cd #{current_path}; rake db:create RAILS_ENV=#{rails_env}"
    end

    task :migrate do
      run "cd #{current_path}; rake db:migrate RAILS_ENV=#{rails_env}"
    end

    task :migrate_reset do
      run "cd #{current_path}; rake db:migrate:reset RAILS_ENV=#{rails_env}"
    end

    task :drop do
      run "cd #{current_path}; rake db:drop RAILS_ENV=#{rails_env}"
    end

    task :seed do
      run "cd #{current_path}; RAILS_ENV=#{rails_env} bundle exec rake environment db:seed"
    end

  	task :yml, :except => { :no_release => true } do
      template = <<-EOF
      production:
        adapter: mysql2
        encoding: utf8
        host: localhost
        database: oritmo_production
        username: root
        password:
        reconnect: true
      staging:
        adapter: mysql2
        encoding: utf8
        host: localhost
        database: oritmo_staging
        username: root
        password:
        reconnect: true
      EOF

	  config = ERB.new(template)
	  run "mkdir -p #{shared_path}/config"
      put config.result(binding), "#{shared_path}/config/database.yml"
  	end

  	task :symlink do
      run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  	end
  end
end

namespace :db do
  task :dump do
    run "mysqldump -uroot oritmo_production > /tmp/oritmo.sql"
    get "/tmp/oritmo.sql", "/tmp/oritmo.sql"
    run_locally "mysql -uroot oritmo_development < /tmp/oritmo.sql"
  end
end

namespace :solr do

  task :reboot do
    begin
      stop
      rescue
    end
    kill
    clean
    start
    reindex
  end

  task :reindex do
    sleep 20
    run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} sunspot:reindex"
  end

  task :start, :roles => :app, :except => { :no_release => true } do
    run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} sunspot:solr:start"
  end

  task :stop, :roles => :app, :except => { :no_release => true } do
    run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} sunspot:solr:stop"
  end

  task :kill do
    kill_processes_matching "solr"
  end

  task :clean do
    run "rm -rf #{latest_release}/solr"
  end
end

def kill_processes_matching(name)
  run "ps -ef | grep #{name} | grep -v grep | awk '{print $2}' | xargs kill || echo 'no process with name #{name} found'"
end

before "deploy:assets:precompile", "deploy:db:symlink"
after "deploy:setup", "deploy:db:yml"

after "deploy:restart", "deploy:cleanup"

