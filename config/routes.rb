#require 'resque/server'

ORitmo::Application.routes.draw do

  get "accounts/create"
  get "accounts/update"
  get "transactions/create"
  get "payments/index"
  get "payments/confirm"
  get "payments/complete"
  get "termos" => 'pages#termos'
  get "upload" => 'products#upload'
  get "home" => 'pages#index'
  get "about-us" => 'pages#about_us', as: :about_us
  get "faq" => 'pages#faq', as: :faq
  get "sitemap" => 'pages#sitemap'

  devise_for :users, :controllers => { :registrations => "registrations" }
  devise_for :admin_users


  resources :users do
    resources :videos
    resources :products

    resources :collections do
      get 'details'
    end

    get 'details'
    get 'edit_password'

    member do
      get 'edit_photo'
    end

    collection do
      get 'status'
      post 'update_photo'
      put 'update_password'
    end
  end

  get 'users(/:order_by)' => 'users#index'

  get 'set_currency/:currency' => 'users#set_currency', :as => :set_currency

  resources :products do
    get 'form'
    collection do
      get 'upload'
      post 'upload_callback'
    end
    resources :orders
    member do
      get 'create_sample'
      get 'download'
      get 'license'
    end
  end

  resources :seller_terms, :only => [:new,:create]
  resources :buyer_terms, :only => [:new,:create]

  resources :payments do
    member do
      get :confirm
      get :cancel
    end
    collection do
      get :withdraw
      get :exchange_rate
    end
  end

  get 'payments/purchase/:product_id' => 'payments#purchase', :as => :purchase_payment

  namespace :admin do
    resources :categories do
      collection do
        get 'roots'
      end
    end
    resources :rhythms
    resources :instruments
    resources :moods
    resources :sound_types
    resources :regions
    resources :users do
      member do
        post :withdraw
      end
    end
    resources :admin_users
    resources :images
    resources :orders, only: :index
    resources :payments, only: :index
    resources :products do
      member do
        get 'reprocess'
        get 'approve'
        delete 'destroy'
        put 'reject'
        post 'highlight'
        get 'reject_confirm'
      end
    end
    resources :homepage_highlights
    root :to => 'admin#index'
    get 'reprocess_avatars' => 'users#reprocess_avatars'
    get 'become' => 'application#become'
  end

  #resources :multiple_products

  root :to => 'pages#index'

  #  mount Resque::Server.new, :at => "/resque"

  match "*categories" => "categories#index", as: :category

  unless Rails.application.config.consider_all_requests_local
    resources :errors do
      collection do
        get :error_404
        get :error_500
      end
    end
    match '*not_found', :to => 'errors#error_404'
  end
end

ActionDispatch::Routing::Translator.translate_from_file('config/i18n-routes.yml')
