Devise.setup do |config|
  require 'devise/orm/active_record'

  config.mailer_sender = "nao-responda@oritmo.com.br"
  config.case_insensitive_keys = [ :email ]
  config.strip_whitespace_keys = [ :email ]
  config.reset_password_within = 6.hours
  config.scoped_views = true
  config.allow_unconfirmed_access_for = 1.day
end
