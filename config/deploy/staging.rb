role :web, "#{stage}.#{application}"
role :app, "#{stage}.#{application}"
role :db,  "#{stage}.#{application}", :primary => true

set :deploy_env, 'staging'
set :rails_env, 'staging'

namespace :resque do
  task :start do
    run "cd #{latest_release} && RAILS_ENV=#{rails_env} QUEUE=* #{rake} environment resque:work  >/dev/null 2>&1 &"
  end
end

namespace :solr do
  task :setup do
    run "cp -Rf #{current_path}/solr #{shared_path}"
  end

  task :rebuild do
    run "rm -Rf #{shared_path}/solr"
    setup
    symlink
    start
  end

  task :symlink do
    run "rm -Rf #{current_path}/solr && ln -s #{shared_path}/solr #{current_path}/solr"
  end

  task :restart do
    stop
    start
  end

end


namespace :db do
  task :restore do
    upload "/tmp/oritmo.sql", "/tmp/oritmo.sql"
    run "mysql -uroot oritmo_#{rails_env} < /tmp/oritmo.sql"
  end
end


# after "deploy:restart", "solr:symlink"
# after "deploy:restart", "solr:restart"
# after "solr:start", "solr:reindex"
after "deploy:restart", "deploy:cleanup"