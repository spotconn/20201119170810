role :web, "#{stage}.#{application}"
role :app, "#{stage}.#{application}"
role :db,  "#{stage}.#{application}", :primary => true

set :deploy_env, 'production'
set :rails_env, 'production'

#namespace :resque do
#  task :start do
#    run "cd #{latest_release} && RAILS_ENV=#{rails_env} QUEUE=* #{rake} environment resque:work  >/dev/null 2>&1 &"
#  end
#end

namespace :solr do
  task :reindex do
    run "cd #{latest_release} && #{rake} RAILS_ENV=#{rails_env} reindex"
  end
end