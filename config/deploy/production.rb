role :web, application
role :app, application
role :db, application, :primary => true

set :deploy_env, 'production'
set :rails_env, 'production'

set :deploy_to, proc { "/home/ubuntu/#{application}" }

namespace :resque do
  task :start do
    run "cd #{latest_release} && RAILS_ENV=#{rails_env} QUEUE=* #{rake} environment resque:work  >/dev/null 2>&1 &"
  end
end

namespace :db do
  task :backup do

    filename = "oritmo_backup.sql"
    gzip_file = "#{filename}.#{Time.now.strftime '%Y%m%dT%:%H%M%S'}.tar.gz"
    on_rollback {
      run "rm /tmp/#{filename}"
      run "rm /tmp/#{gzip_file}"
    }
    run "mysqldump -uroot oritmo_#{rails_env} > \n
    /tmp/#{filename}" do |channel, stream, data|
      puts data
    end
    run "tar cvzf /tmp/#{gzip_file} /tmp/#{filename}"
    run_locally "mkdir -p /tmp/backups"
    get "/tmp/#{gzip_file}", "/tmp/backups/#{gzip_file}"
    run  "rm /tmp/#{gzip_file}"

    run_locally "cp /tmp/backups/#{gzip_file} /old_home/Dev/oritmo_dumps/#{gzip_file}"
  end
end
