#encoding: utf-8

namespace :oritmo do
  
  desc "check balances"
  task :balance => :environment do
    Account.includes(:user).each do |account|
      
      balance = 0
      account.transactions.each do |transaction|
        # p "#{transaction.debit_credit}: #{transaction.amount}"
        balance = balance + (transaction.debit_credit=="C" ? 1 : -1) * transaction.amount
      end
      
      next if account.balance == 0 && balance.round(2) == account.balance.round(2)
      
      if account.user
        p "#{account.user.id}. #{account.user.name}: #{account.balance.round(2)}"
      else
        p account
      end
      
      if balance.round(2) != account.balance.round(2)
        p "//////////////// Balance should be: #{balance.round(2)}"
      end
      
    end
  end
  
  desc "user transactions"
  task :transactions, [:user_id] => :environment do |t, args|
    p args
    args.with_defaults(user_id: 0)
    user = User.find(args[:user_id])
    p user.name
    user.account.transactions.each do |transaction|
      p "-------------------------------------------------"
      p transaction
      if transaction.referable_type == 'Payment'
        p transaction.referable
      end
    end
  end
  
  desc "manual payments"
  task :manual_payments => :environment do
    Payment.where("merchant_data is null and amount_paid>0").each do |payment|
      p "#{payment.user.name} - #{payment.amount}"
    end
  end
  
  desc "adjust balances and transactions"
  task :adjust_balances => :environment do
    
    oritmo_account = Account.find(37)
    
    # marcos ######################################
    user = User.find(48)
    account = user.account
    account.balance = 0
    account.save
    ##############################################
    
    # anabel #####################################
    user = User.find(23)
    account = user.account
    
    # delete fake payment
      t = Transaction.find(31)
      t.destroy
      p = Payment.find(18)
      p.destroy
    
    # create transfer from oritmo
      transfer = SimpleTransfer.new
      transfer.source = oritmo_account
      transfer.destination = user.account
      transfer.description = "Credito para sons do video do Posto Zero"
      transfer.amount = 1000 - 890.54
      transfer.created_at = "2012-12-03 17:59:03"
      transfer.create
      
    account.balance = 0
    account.save
    ##############################################
    
    
    # gigio #####################################
    user = User.find(2)
    account = user.account
    
    # delete fake payment
      t = Transaction.find(204)
      t.destroy
      p = Payment.find(49)
      p.destroy
    
    # create transfer from oritmo
      transfer = SimpleTransfer.new
      transfer.source = oritmo_account
      transfer.destination = user.account
      transfer.description = "Credito para sons para festa de lancamento"
      transfer.amount = 100
      transfer.created_at = "2013-04-18 20:14:30"
      transfer.create
      
    account.balance = 31.39
    account.save
    ##############################################
    
    
    # imperatore #####################################
    user = User.find(8)
    account = user.account
    
    # delete fake payment
      t = Transaction.find(238)
      t.destroy
      p = Payment.find(53)
      p.destroy
    
    # create transfer from oritmo
      transfer = SimpleTransfer.new
      transfer.source = oritmo_account
      transfer.destination = user.account
      transfer.description = "Credito para sons para festa de lancamento"
      transfer.amount = 150
      transfer.created_at = "2013-04-29 21:22:29"
      transfer.create
      
    account.balance = 122
    account.save
    ##############################################


    # dudam #####################################
    user = User.find(89)
    account = user.account

    # delete fake payment
      t = Transaction.find(237)
      t.destroy
      p = Payment.find(52)
      p.destroy

    # create transfer from oritmo
      transfer = SimpleTransfer.new
      transfer.source = oritmo_account
      transfer.destination = user.account
      transfer.description = "Credito para sons para festa de lancamento"
      transfer.amount = 150
      transfer.created_at = "2013-04-29 21:22:29"
      transfer.create
      
    account.balance = 19.12
    account.save
    ##############################################


    # jonas #####################################
    user = User.find(91)
    account = user.account

    # delete fake payment
      t = Transaction.find(239)
      t.destroy
      p = Payment.find(54)
      p.destroy

    # create transfer from oritmo
      transfer = SimpleTransfer.new
      transfer.source = oritmo_account
      transfer.destination = user.account
      transfer.description = "Credito para sons para festa de lancamento"
      transfer.amount = 150
      transfer.created_at = "2013-04-29 21:22:29"
      transfer.create
      
    account.balance = 45.02
    account.save
    ##############################################
    


    # ramon #####################################
    user = User.find(82)
    account = user.account

    # create transfer from oritmo
      transfer = SimpleTransfer.new
      transfer.source = oritmo_account
      transfer.destination = user.account
      transfer.description = "Credito para sons para festa de lancamento"
      transfer.amount = 14
      transfer.created_at = "2013-04-17 05:15:04"
      transfer.create
      
    account.balance=0
    account.save
    ##############################################
    
  end
  
  desc "reprocess category slugs"
  task :category_slugs => :environment do
    
    Category.all.each do |category|
      I18n.locale = :pt
      p "pt: #{category.name}"
      category.name = category.name
      category.set_friendly_id(category.name, :pt)
      category.save
      
      I18n.locale = :en
      p "en: #{category.name}"
      category.name = category.name
      category.set_friendly_id(category.name, :en)
      category.save
    end
    
    
    #I18n.locale = :pt
    #Category.find_each(&:save) 
    #
    #I18n.locale = :en
    #Category.find_each(&:save)
  end

  desc "Send email invitation to all users"
  task :invite => :environment do
    User.all.each do |user|

        puts "invite #{user.name}"
        Notifier.party_invitation(user).deliver

        sleep(10)
    end
  end

  desc "tapume"
  task :invite_tapume => :environment do
    list = [{ _id: ObjectId("4f5a26a06670060003000001"), EMAIL: "tao@oritmo.com", FNAME: "Gastão", LNAME: "Brun", CITY: "Rio De Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f5a5fde6670060003000002"), EMAIL: "dpereirabr@gmail.com", FNAME: "Daniel", LNAME: "Pereira", CITY: "Rio de Janeiro", COUNTRY: "Brazil", HOWUSE: "songs", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f5a9e5ead78c40003000001"), EMAIL: "sha@rodrigosha.com.br", FNAME: "Rodrigo", LNAME: "Sha", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "sax,Flute,Guitar,Clarinete,Vocal", RHYTHMS: "Bossa Nova,House Music,Samba,Reggae", TYPE: "PROD" },
{ _id: ObjectId("4f69ea616f34d60003000001"), EMAIL: "camillemh@gmail.com", FNAME: "Camille", LNAME: "Menaei Herchenhorn", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f69eb9e6f34d60003000002"), EMAIL: "KARENALIAGA@UOL.COM.BR", FNAME: "KAREN", LNAME: "QUESADA", CITY: "GUARULHOS", COUNTRY: "BRASIL", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f69ec986f34d60003000003"), EMAIL: "gustavoalbernaz@gmail.com", FNAME: "Gustavo", LNAME: "Albernaz", CITY: "Barcelona", COUNTRY: "Espanha", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f69efb96f34d60003000004"), EMAIL: "carla.yared@cajaarquiteturacultural.com.br", FNAME: "Carla ", LNAME: "Yared", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Produtora", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f69f21d6f34d60003000005"), EMAIL: "luciok@gmail.com", FNAME: "LUCIO", LNAME: "K", CITY: "Oakland", COUNTRY: "US", HOWUSE: "sampleio e manipulo", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f69f29e6f34d60003000006"), EMAIL: "banda.metafora@gmail.com", FNAME: "Beto", LNAME: "Márcio", CITY: "Salvador", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f69f5446f34d60003000007"), EMAIL: "telma0980@yahoo.com", FNAME: "Telma", LNAME: "Lemos", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Sou DJ", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f69f5fd6f34d60003000008"), EMAIL: "danielmarquesviolao@gmail.com", FNAME: "Daniel", LNAME: "Aguiar", CITY: "RIO DE JANEIRO", COUNTRY: "Brasil", HOWUSE: "shows", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f69f6fa6f34d60003000009"), EMAIL: "jlins@me.com", FNAME: "Joao", LNAME: "Lins", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Violão,percussão,violão percussão cavaquinho,violão,violão percussão,v", RHYTHMS: "todas", TYPE: "PROD" },
{ _id: ObjectId("4f69f81a6f34d6000300000a"), EMAIL: "danistain@gmail.com", FNAME: "Daniel ", LNAME: "Sztajnberg", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Guitar,Guitarra,guitarra,gui,guitar", RHYTHMS: "Jazz", TYPE: "PROD" },
{ _id: ObjectId("4f69fa1d6f34d6000300000b"), EMAIL: "joao.beyssac@gmail.com", FNAME: "João", LNAME: "Beyssac", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Crio", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f69fd1d6f34d6000300000c"), EMAIL: "romero-sr@hotmail.com", FNAME: "Romero", LNAME: "Soriano", CITY: "Recife -Pe", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f69fe4d6f34d6000300000d"), EMAIL: "rodrigueswashington@hotmail.com", FNAME: "Washington", LNAME: "Rodrigues", CITY: "Vitória-ES", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Guitarra e violão", RHYTHMS: "MBP,Samba Rock", TYPE: "PROD" },
{ _id: ObjectId("4f69ff846f34d6000300000e"), EMAIL: "info@rhythmcraft.net", FNAME: "Marcelo ", LNAME: "Vig", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Criação de ritmos, trilhas, produção musical: www.rhythmcraft.net", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a06896f34d6000300000f"), EMAIL: "kadukid@gmail.com", FNAME: "Kadu", LNAME: "Menezes", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Sou produtor, engenheiro de som, baterista e percussionista", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a0d726f34d60003000010"), EMAIL: "mauzioliveira@gmail.com", FNAME: "Mauricio", LNAME: "Oliveira", CITY: "Rio", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "baixo", RHYTHMS: "mpb,Maracatu", TYPE: "PROD" },
{ _id: ObjectId("4f6a143e6f34d60003000011"), EMAIL: "feernandodiss@hotmail.com", FNAME: "fernando", LNAME: "dias", CITY: "rio de janeiro", COUNTRY: "brasil", HOWUSE: "", INSTRUMENT: "bateria,percussao,violao,voz", RHYTHMS: "reggae,soul,rock,blues,pop rock,metal", TYPE: "PROD" },
{ _id: ObjectId("4f6a144c6f34d60003000012"), EMAIL: "rachplat@gmail.com", FNAME: "Rachel ", LNAME: "Platenik", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Viajo... crio, trabalho, dirijo, reflito, conto histórias, e um dia, quem sabe, tocarei tb :) ", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a16556f34d60003000014"), EMAIL: "Pedroveiga1@gmail.com", FNAME: "pedro", LNAME: "veiga", CITY: "rio de Janeiro", COUNTRY: "brazil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6a188d6f34d60003000015"), EMAIL: "isa.hirsch.alcantara@gmail.com", FNAME: "isabel", LNAME: "alcantara", CITY: "Rio De Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6a18ae6f34d60003000016"), EMAIL: "ANDREINFANTINE@HOTMAIL.COM", FNAME: "ANDRÉ", LNAME: "INFANTINE", CITY: "CACONDE", COUNTRY: "BRASIL", HOWUSE: "", INSTRUMENT: "TECLADO,VIOLÃO,TECLADO E VIOLÃO", RHYTHMS: "ROCK,ELETRONICO BLUES RITHIM&BLUES,rock r&b eletronico blues mpb", TYPE: "PROD" },
{ _id: ObjectId("4f6a1aaf6f34d60003000017"), EMAIL: "rodrigosa101@hotmail.com", FNAME: "Rodrigo ", LNAME: "Sá", CITY: "São Paulo", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Berimbau,Didgeridoo,Pandeiro,Berimbau Didgeridoo Pandeiro Perc Geral Violão Berimbau duas cordas", RHYTHMS: "Musica Brasileira de Qualidade", TYPE: "PROD" },
{ _id: ObjectId("4f6a1eca6f34d60003000019"), EMAIL: "kpalhano@ig.com.br", FNAME: "Kaka", LNAME: "palhano", CITY: "rio de janeiro", COUNTRY: "brasil", HOWUSE: "toco, ouço, componho", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a20bd6f34d6000300001a"), EMAIL: "aingles@gmail.com", FNAME: "Alain", LNAME: "Ingles", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Violão,Baixo Elétrico,Guitarra,Bateria", RHYTHMS: "Samba canção,Forró,Maracatu", TYPE: "PROD" },
{ _id: ObjectId("4f6a213a6f34d6000300001b"), EMAIL: "luiscarlinhos@luiscarlinhos.com.br", FNAME: "Luis Carlinhos", LNAME: "Xavier", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "voz,violão", RHYTHMS: "MPB,Reggae", TYPE: "PROD" },
{ _id: ObjectId("4f6a23806f34d6000300001c"), EMAIL: "thais@moutinho.com.brq", FNAME: "Thais", LNAME: "Moutinho", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6a2dd16f34d6000300001d"), EMAIL: "izabel.mattos@gmail.com", FNAME: "Izabel", LNAME: "Mattos", CITY: "Niterói", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6a31116f34d6000300001e"), EMAIL: "fabioduarte@yahoo.com", FNAME: "Fabio", LNAME: "Duarte", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Guitarra,Violão", RHYTHMS: "Rock,Pop,Blues", TYPE: "PROD" },
{ _id: ObjectId("4f6a3d286f34d6000300001f"), EMAIL: "sergioberrini@hotmail.com", FNAME: "Sergio", LNAME: "Berrini", CITY: "RIO DE JANEIRO", COUNTRY: "Brasil", HOWUSE: "Composição e gravação", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a3d5f6f34d60003000020"), EMAIL: "kafer.dhw@gmail.com", FNAME: "alexandre", LNAME: "kafer", CITY: "rio de janeiro", COUNTRY: "brasil", HOWUSE: "", INSTRUMENT: "bateria", RHYTHMS: "rock", TYPE: "PROD" },
{ _id: ObjectId("4f6a5ae91c3cf20003000001"), EMAIL: "cesardyon@oi.com.br", FNAME: "César", LNAME: "Dyonísio", CITY: "Niterói", COUNTRY: "Brasil", HOWUSE: "Toco profissionalmente com a minha banda, Último Reduto, mas respiro música o tempo todo. Penso em batidas, grooves, crio o tempo inteiro e componho sempre alguma coisa a cada momento: FRases de guitarra, linhas de baixo, backing vocals e também músicas inteiras com letras. Uso depois as idéias na prática já que hoje em dia gravo tudo que crio no celular.", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a61221c3cf20003000002"), EMAIL: "andre@xdbproductions.com.br", FNAME: "andre", LNAME: "silva", CITY: "são paulo", COUNTRY: "brasil", HOWUSE: "produção", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6a63df1c3cf20003000003"), EMAIL: "yarruda@gmail.com", FNAME: "yvana", LNAME: "arruda", CITY: "niteroi", COUNTRY: "brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6a656b1c3cf20003000004"), EMAIL: "mfarias08@uol.com.br", FNAME: "mauro ", LNAME: "farias", CITY: "belem", COUNTRY: "pará", HOWUSE: "", INSTRUMENT: "bateria e percussão", RHYTHMS: "mpb,rock blues", TYPE: "PROD" },
{ _id: ObjectId("4f6a6ef11c3cf20003000005"), EMAIL: "mauro.pinheiro@gmail.com", FNAME: "mauro", LNAME: "pinheiro", CITY: "vitória", COUNTRY: "brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6b38801c3cf20003000006"), EMAIL: "nevesleonardo@hotmail.com", FNAME: "Leonardo", LNAME: "Neves", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Produção/DJ", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6b456c1c3cf20003000007"), EMAIL: "contato@helenaklang.com", FNAME: "Helena", LNAME: "Klang", CITY: "Rio", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f6bb3ef0ac61b0003000001"), EMAIL: "tao@brun.srv.br", FNAME: "Gastão", LNAME: "Brun", CITY: "Rio De Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Guitarra,Piano,Agogô,clarinete", RHYTHMS: "Samba canção,rua", TYPE: "PROD" },
{ _id: ObjectId("4f6bd0770ac61b0003000002"), EMAIL: "michael@michaelmariano.com.br", FNAME: "Michael", LNAME: "Mariano", CITY: "Sao Paulo", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Violao,Piano,Baixo Elétrico,Ukulele", RHYTHMS: "MPB,Rock,Reggae", TYPE: "PROD" },
{ _id: ObjectId("4f6cb2c80ac61b0003000003"), EMAIL: "emaildofreud@gmail.com", FNAME: "Gustavo", LNAME: "Freudenfeld", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Videos", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f6cba460ac61b0003000004"), EMAIL: "melgluz@hotmail.com", FNAME: "melissa", LNAME: "gluz", CITY: "rio de janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f70b2baaa2cdf0003000001"), EMAIL: "prmk9@yahoo.com.br", FNAME: "Paulo", LNAME: "Kastrup", CITY: "Paraty", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f71b810b3bd460003000001"), EMAIL: "caterinevilardo@gmail.com", FNAME: "Caterine", LNAME: "Vilardo", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f71c807b3bd460003000002"), EMAIL: "renatagebara@gmail.com", FNAME: "Renata ", LNAME: "Gebara", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Cantora", RHYTHMS: "samba,Bossa Nova,mpb,pop,jazz", TYPE: "PROD" },
{ _id: ObjectId("4f71c8fbb3bd460003000003"), EMAIL: "cantoratamy@gmail.com", FNAME: "Tamy", LNAME: "Macedo", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "I am singer", RHYTHMS: "Brazillian Music", TYPE: "PROD" },
{ _id: ObjectId("4f71c908b3bd460003000004"), EMAIL: "luizcarlossax@hotmail.com", FNAME: "Luiz Carlos", LNAME: "Soares Santos", CITY: "Teresina", COUNTRY: "Piauí/Brasil", HOWUSE: "", INSTRUMENT: "Saxofone", RHYTHMS: "Bossa nova,Jazz,Instrumental,Forró Samba MPB Rock Clássicos,etc...", TYPE: "PROD" },
{ _id: ObjectId("4f71ccdfb3bd460003000005"), EMAIL: "marcellebraga@yahoo.com", FNAME: "Marcelle", LNAME: "Braga", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE" },
{ _id: ObjectId("4f71cdbfb3bd460003000007"), EMAIL: "luizcarlossax@hotmail.com", FNAME: "Luiz Carlos", LNAME: "Soares Santos", CITY: "Teresina ", COUNTRY: "Piauí/Brasil", HOWUSE: "solos de saxofone", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER" },
{ _id: ObjectId("4f71d912c573640003000003"), EMAIL: "alexfonseca@globo.com", FNAME: "alexandre", LNAME: "fonseca", CITY: "rio de janeiro", COUNTRY: "brasil", HOWUSE: "", INSTRUMENT: "bateria,baixo,teclado,tabla", RHYTHMS: "musica brasileira,rock", TYPE: "PROD", updated_at: ISODate("2012-03-27T15:13:22Z"), created_at: ISODate("2012-03-27T15:13:22Z") },
{ _id: ObjectId("4f71ebdec573640003000004"), EMAIL: "production@zeluis.com", FNAME: "Zé Luis", LNAME: "Oliveira", CITY: "New York", COUNTRY: "USA", HOWUSE: "", INSTRUMENT: "Saxofone tenor,Saxofone soprano,Flauta,Flauta em Sol,Flautas de bambu,Violão,Percussão", RHYTHMS: "MPB,Samba,Samba Jazz,Jazz,Brazilian Jazz,Pop", TYPE: "PROD", updated_at: ISODate("2012-03-27T16:33:34Z"), created_at: ISODate("2012-03-27T16:33:34Z") },
{ _id: ObjectId("4f720c8fc573640003000005"), EMAIL: "candidapinto@remax.com.br", FNAME: "Candida", LNAME: "Pinto", CITY: "Curitiba", COUNTRY: "Brasil", HOWUSE: "Sonho bem alto, porque sonhar nos torna livres de espirito.", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-03-27T18:53:03Z"), created_at: ISODate("2012-03-27T18:53:03Z") },
{ _id: ObjectId("4f73c30a8f2b340003000001"), EMAIL: "paabreu@globo.com", FNAME: "pa", LNAME: "abreu", CITY: "rj", COUNTRY: "brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-03-29T02:03:54Z"), created_at: ISODate("2012-03-29T02:03:54Z") },
{ _id: ObjectId("4f7765552b16560003000001"), EMAIL: "crepaldibellangero@msn.com", FNAME: "Alessandro", LNAME: "Crepaldi", CITY: "São Paulo", COUNTRY: "Brasil", HOWUSE: "Faço dela a Minha Vida ", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-03-31T20:13:09Z"), created_at: ISODate("2012-03-31T20:13:09Z") },
{ _id: ObjectId("4f787c852b16560003000002"), EMAIL: "arildinho@gmail.com", FNAME: "Arildo", LNAME: "Barcelar", CITY: "Joinville", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Vocal,guitarra,violão", RHYTHMS: "Rock,Samba,Blues,Soul", TYPE: "PROD", updated_at: ISODate("2012-04-01T16:04:21Z"), created_at: ISODate("2012-04-01T16:04:21Z") },
{ _id: ObjectId("4f79b305413d360003000001"), EMAIL: "dmmarkus@uol.com.br", FNAME: "Dani", LNAME: "Markus", CITY: "SAO PAULO", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-04-02T14:09:09Z"), created_at: ISODate("2012-04-02T14:09:09Z") },
{ _id: ObjectId("4f7c3c0c2b087c0003000001"), EMAIL: "roberto_rensi@yahoo.com.br", FNAME: "Roberto", LNAME: "Rensi", CITY: "São Paulo", COUNTRY: "Brazil", HOWUSE: "My life", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-04-04T12:18:20Z"), created_at: ISODate("2012-04-04T12:18:20Z") },
{ _id: ObjectId("4f7dd44fb46fd60003000001"), EMAIL: "gabrielopitz@yahoo.com.br", FNAME: "Gabriel", LNAME: "Opitz", CITY: "Santa Maria", COUNTRY: "Brazil", HOWUSE: "", INSTRUMENT: "Bandolim,Violão 7 Cordas,Cavaquinho,Pandeiro", RHYTHMS: "Samba,Baião,Chamamé,Milonga", TYPE: "PROD", updated_at: ISODate("2012-04-05T17:20:15Z"), created_at: ISODate("2012-04-05T17:20:15Z") },
{ _id: ObjectId("4f7f2b0af1bea40003000001"), EMAIL: "elinsings@gmail.com", FNAME: "elin", LNAME: "Melgarejo", CITY: "Washington, DC", COUNTRY: "USA", HOWUSE: "", INSTRUMENT: "vocals,percussion,flute,piano", RHYTHMS: "Samba canção,Samba-de-roda,Bossa Nova,Baião,Samba,samba,Forró", TYPE: "PROD", updated_at: ISODate("2012-04-06T17:42:34Z"), created_at: ISODate("2012-04-06T17:42:34Z") },
{ _id: ObjectId("4f95b98cb5080b0003000001"), EMAIL: "musiczinho@gmail.com", FNAME: "Josemar ", LNAME: "Santos Ribeiro", CITY: "São Paulo", COUNTRY: "Brasil", HOWUSE: "Base de Hip Hop (Rap)", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-04-23T20:20:28Z"), created_at: ISODate("2012-04-23T20:20:28Z") },
{ _id: ObjectId("4fb655025048340003000001"), EMAIL: "magrusmusic@yahoo.com", FNAME: "Magrus", LNAME: "Borges", CITY: "Belem", COUNTRY: "Brazil", HOWUSE: "", INSTRUMENT: "Drums,Percussion,keyboards,guitar", RHYTHMS: "Amazon Jungle,Carimbo,Afro Brasilian,Bossa Nova,Funk", TYPE: "PROD", updated_at: ISODate("2012-05-18T13:56:18Z"), created_at: ISODate("2012-05-18T13:56:18Z") },
{ _id: ObjectId("4fbe365b11d2aa0003000001"), EMAIL: "amartinsn@gmail.com", FNAME: "Alexandre", LNAME: "Martins", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "", INSTRUMENT: "Drums", RHYTHMS: "latin,Jazz,samba", TYPE: "PROD", updated_at: ISODate("2012-05-24T13:23:39Z"), created_at: ISODate("2012-05-24T13:23:39Z") },
{ _id: ObjectId("4fd646b3759a340003000001"), EMAIL: "max73max@yahoo.com", FNAME: "Max", LNAME: "Mor", CITY: "Milano", COUNTRY: "Italy", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-06-11T19:27:47Z"), created_at: ISODate("2012-06-11T19:27:47Z") },
{ _id: ObjectId("4fe4e4c5a43fa40003000001"), EMAIL: "fracazo@gmail.com", FNAME: "Alexandre", LNAME: "Gomes", CITY: "rio de janeiro", COUNTRY: "Brazil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-06-22T21:33:57Z"), created_at: ISODate("2012-06-22T21:33:57Z") },
{ _id: ObjectId("5005db179f482c0002000001"), EMAIL: "davidvillefort2@gmail.com", FNAME: "David", LNAME: "Villefort", CITY: "Ibiza", COUNTRY: "Espana", HOWUSE: "", INSTRUMENT: "Percussao", RHYTHMS: "Various !", TYPE: "PROD", updated_at: ISODate("2012-07-17T21:37:27Z"), created_at: ISODate("2012-07-17T21:37:27Z") },
{ _id: ObjectId("5012b02c0219f30002000001"), EMAIL: "faganw@hotmail.com", FNAME: "fagner", LNAME: "silva", CITY: "teresina", COUNTRY: "brasil", HOWUSE: "faço música", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-07-27T15:13:48Z"), created_at: ISODate("2012-07-27T15:13:48Z") },
{ _id: ObjectId("5037380b9214fb0002000001"), EMAIL: "bobbylicious@gmail.com", FNAME: "Robert", LNAME: "Lee", CITY: "Tegucigalpa", COUNTRY: "Honduras", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-08-24T08:15:07Z"), created_at: ISODate("2012-08-24T08:15:07Z") },
{ _id: ObjectId("5052102f5ff8770002000001"), EMAIL: "micoanderson@hotmail.com", FNAME: "mayco", LNAME: "moreira", CITY: "sao paulo", COUNTRY: "brasil", HOWUSE: "curto", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-09-13T16:56:15Z"), created_at: ISODate("2012-09-13T16:56:15Z") },
{ _id: ObjectId("50521ac95ff8770002000002"), EMAIL: "fabricioserafim@hotmail.com", FNAME: "Fabricio", LNAME: "Serafim", CITY: "Atibaia", COUNTRY: "Brazil", HOWUSE: "", INSTRUMENT: "Guitar,bass,acoustic guitar", RHYTHMS: "Bossa nova,MPB,Classical rock", TYPE: "PROD", updated_at: ISODate("2012-09-13T17:41:29Z"), created_at: ISODate("2012-09-13T17:41:29Z") },
{ _id: ObjectId("5061ca4683f3790002000001"), EMAIL: "claupiza@gmail.com", FNAME: "claudia", LNAME: "piza", CITY: "buenos aires", COUNTRY: "argentina", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-09-25T15:14:14Z"), created_at: ISODate("2012-09-25T15:14:14Z") },
{ _id: ObjectId("508572592bd1c10002000001"), EMAIL: "tao@oqdar.com", FNAME: "Gastao", LNAME: "Brun", CITY: "Rio de Janeiro", COUNTRY: "Brazil", HOWUSE: "", INSTRUMENT: "", RHYTHMS: "", TYPE: "LOVE", updated_at: ISODate("2012-10-22T16:20:41Z"), created_at: ISODate("2012-10-22T16:20:41Z") },
{ _id: ObjectId("50919f9f4c698e0002000001"), EMAIL: "valle.andre@gmail.com", FNAME: "André ", LNAME: "Valle", CITY: "Rio de Janeiro", COUNTRY: "Brasil", HOWUSE: "Trabalho", INSTRUMENT: "", RHYTHMS: "", TYPE: "USER", updated_at: ISODate("2012-10-31T22:01:03Z"), created_at: ISODate("2012-10-31T22:01:03Z") }]


    list.each do |k| 
      puts k
      user = User.new
      user.email = k[:EMAIL]
      user.name = k[:FNAME]

      Notifier.party_invitation(user).deliver

      sleep(10)
    end

  end
end

def ObjectId(str)
  str
end

def ISODate(str)
  str
end