namespace :coverage do
  desc "Check the coverage status for tests"
  task :check do
    ENV["COVERAGE"] = "true"
    Rake::Task["spec"].invoke
  end
end