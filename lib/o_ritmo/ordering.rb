module ORitmo
  module OrderHelper
    
    def place_order
      hash = { product: @product, price: @product.price, status: 'N', payment_method: 'None', discount: 0 }
      @order = current_user.orders.new(hash)
      
      if @product.price==0
        @order.surcharge = 0
      else
        @order.surcharge = @has_credit ? 0 : @order.buynow_surcharge
      end
      
      @order.amount_paid = @product.price + @order.surcharge
      
      @order.allow_commercial_use = @product.allow_commercial_use
      
      if @order.save
        flash[:notice]='Order completed!'
        @order.send_emails
        redirect_to user_path(current_user)
      else
        if @order.errors.any?
          error_message=""
          @order.errors.full_messages.each do |msg|
            error_message += "#{msg}"
          end
          flash[:error]=error_message
        end
        render :new
      end
    end
    
  end
end