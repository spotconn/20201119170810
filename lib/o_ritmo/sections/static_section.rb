require "o_ritmo/sections/static_category"

module ORitmo
  module Sections
    class StaticSection
      attr_reader :name

      def initialize(name)
        @name = name
      end

      def children
        @children ||= []
      end
    end
  end
end