module ORitmo
  module Sections
    class StaticCategory   
      attr_reader :name, :url

      def initialize(name, url)
        @name, @url = name, url
      end
    end
  end
end