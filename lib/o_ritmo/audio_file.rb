#!/usr/bin/env ruby
require 'fileutils'
require 'open3'
require 'o_ritmo/aws'

module ORitmo
  class AudioFile
    #@source_dir="/mnt/oritmo/uploads"
    SOURCE_DIR="#{Settings.audio.sox_root_dir}/#{Settings.audio.upload_dir}"         #"#{Rails.root}/public/uploads"
    SAMPLE_DIR="#{Settings.audio.sox_root_dir}/#{Settings.audio.samples_dir}"
    WATERMARK_FILE="#{Settings.audio.watermark_file}"
    SAMPLE_LENGTH=30 #seconds

    attr_reader :name, :opts
    attr_accessor :file_to_stereo, :file_to_process, :sample_filename

    def initialize(name, opts={})
      @name = name
      @opts = opts
      name_array = name.split('.')
      self.sample_filename = "#{name_array[0]}.mp3"
    end

    def name=(name)
      @name = name
      @info = nil
    end

    def path
      "#{SOURCE_DIR}/#{name}"
    end

    def process
      Rails.logger.warn "name: #{@name}"
      Rails.logger.warn "path: #{path}"
      Rails.logger.warn "sample_filename: #{self.sample_filename}"
      if valid?
        Rails.logger.warn info
        check_channel
        check_sampling_rate
        if (opts[:no_watermark] ? downsample : add_watermark)
          clean_up
          # move_source
        else
          clean_up
          return false
        end
        return @info
      else
        Rails.logger.warn "The specified files does not exist"
        return false
      end

    end

    def info
      @info ||= get_info
    end

    def check_channel
      #p "Checking file channels (#{info["Channels"]})"
      if info["Channels"]!="2"
        self.file_to_stereo = "stereo.#{name}"
        #p "Convert into 2 channels and save as #{self.file_to_stereo}"
        sox_command = "sox #{path} -c 2 #{SOURCE_DIR}/#{self.file_to_stereo}"
        Rails.logger.warn "COMMAND: #{sox_command}"
        result=system `#{sox_command}`
        #p "RESULT: #{result}"
      else
        self.file_to_stereo = name
        #p "2 channels ok - #{self.file_to_stereo}"
      end
    end

    def check_sampling_rate
      #p "Checking sample rate (#{info["Sample Rate"]})"
      if info["Sample Rate"]!="44100"
        self.file_to_process = "sr44100.#{self.file_to_stereo}"
        #p "Convert into 44100 and save as #{self.file_to_process}"
        sox_command="sox #{SOURCE_DIR}/#{self.file_to_stereo} -r 44100 #{SOURCE_DIR}/#{self.file_to_process}"
        Rails.logger.warn "COMMAND: #{sox_command}"
        result=system `#{sox_command}`
        #p "RESULT: #{result}"
      else
          self.file_to_process = self.file_to_stereo
          Rails.logger.warn "sample rate ok - #{self.file_to_process}"
      end
    end

    def add_watermark
      sample_length = info["Duration"][0]>SAMPLE_LENGTH ? SAMPLE_LENGTH : info["Duration"][0]
      #p "will add watermark and create sample (#{sample_length}s)"
      sox_command = "sox -m #{SOURCE_DIR}/#{self.file_to_process} #{WATERMARK_FILE} #{SAMPLE_DIR}/#{self.sample_filename} trim 0 #{sample_length}"
      Rails.logger.warn "COMMAND: #{sox_command}"
      stdin, stdout, stderr = Open3.popen3("#{sox_command}")
      result = stderr.gets.to_s
      if result.length>0 && !result.starts_with?("sox WARN")
        Rails.logger.warn "Error: #{result}"
        return false
      end
      return true
    end

    def downsample
      sox_command = "sox #{SOURCE_DIR}/#{self.file_to_process} -r 8000 #{SAMPLE_DIR}/#{self.sample_filename}"
      Rails.logger.warn "COMMAND: #{sox_command}"
      stdin, stdout, stderr = Open3.popen3("#{sox_command}")
      result = stderr.gets.to_s
      if result.length>0 && !result.starts_with?("sox WARN")
        Rails.logger.warn "Error: #{result}"
        return false
      end
      return true
    end

    def clean_up
      Rails.logger.warn "will clean up"
      File::unlink("#{SOURCE_DIR}/#{self.file_to_process}") if self.file_to_process!=name && File::exists?("#{SOURCE_DIR}/#{self.file_to_process}")
      File::unlink("#{SOURCE_DIR}/#{self.file_to_stereo}") if self.file_to_stereo!=name && File::exists?("#{SOURCE_DIR}/#{self.file_to_stereo}")

      if ArchiveUploader::storage == CarrierWave::Storage::Fog
        Rails.logger.warn "set_public_acl #{Settings.audio.samples_dir}/#{self.sample_filename}"
        ORitmo::AwsHelper::set_public_acl("#{Settings.audio.samples_dir}/#{self.sample_filename}")
      end
    end

    #def move_source
    #  FileUtils::mv("#{SOURCE_DIR}/#{name}", "#{PROCESSED_DIR}/#{name}")
    #end

    def valid?
      File.exists? path
    end

    def get_info
      file_info_str = `sox --info #{path}`
      #file_info_str = "\nInput File     : '/mnt/oritmo/uploads/guitar.wav'\nChannels       : 2\nSample Rate    : 44100\nPrecision      : 16-bit\nDuration       : 00:00:08.73 = 384860 samples = 654.524 CDDA sectors\nFile Size      : 1.54M\nBit Rate       : 1.41M\nSample Encoding: 16-bit Signed Integer PCM\n\n"

      file_info = file_info_str.split("\n").slice(1..file_info_str.size).inject({}) do |hash, x|
        key, value = x.split(":", 2).map { |v| v.to_s.strip }
        hash[key] = value
        hash
      end

      file_info["Duration"] = file_info["Duration"].split(' = ')
      s = file_info["Duration"][0].split(':')
      seconds = s[0].to_i*3600+s[1].to_i*60+s[2].to_i
      file_info["Duration"] = [seconds] + file_info["Duration"]
      file_info.delete "Input File"
      return file_info

    end
  end

end
