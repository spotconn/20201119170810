module ORitmo
  class AwsHelper

    # This method can be used to set a public acl on any object. The parameter file_path
    # will be the path to the file in the bucket minus the domain info, so if your full url was
    # http://s3.amazonaws.com/<your-bucket>/images/image1.png, file path would be
    # images/image1.png
    def self.set_public_acl(file_path)

      @bucket_path = Settings.audio.s3_bucket

      Rails.logger.warn "===> Loading S3"
      s3 = AWS::S3.new

      if(s3)
        bucket = s3.buckets[@bucket_path]

        if(bucket.exists?)
          Rails.logger.warn "===> Bucket '#{@bucket_path}' FOUND"

          key = bucket.objects[file_path]

          if(key.exists?)
            Rails.logger.warn "===> Key '#{file_path}' FOUND"

            key.acl = :public_read

            Rails.logger.warn "===> ACL Set to public read:"
            #key.acl.grants.each { |grant| Rails.logger.warn "grantee => #{grant.grantee.group_uri}, permission => #{grant.permission.name}"}

            return key
          end
        end
      end
    end

  end
end