# encoding: utf-8

Given /^I am logged in as a seller$/ do
  @user = FactoryGirl.create :user
  #@user.confirm!
  @user.accept_seller_terms!
  step %Q{I go to the home page}
  step %Q{I follow "#{I18n.t(:sign_in)}"}
  step %Q{I fill in "Email" with "#{@user.email}"}
  step %Q{I fill in "#{I18n.t("simple_form.labels.user.password")}" with "#{@user.password}"}
  step %Q{I press "#{I18n.t(:sign_in)}"}
  step %Q{I should see "#{I18n.t("devise.sessions.signed_in")}"}
end

Given /^I am logged in as a user$/ do
  @user = FactoryGirl.create :user
  #@user.confirm!
  step %Q{I go to the home page}
  step %Q{I follow "#{I18n.t(:sign_in)}"}
  step %Q{I fill in "Email" with "#{@user.email}"}
  step %Q{I fill in "#{I18n.t("simple_form.labels.user.password")}" with "#{@user.password}"}
  step %Q{I press "#{I18n.t(:sign_in)}"}
  step %Q{I should see "#{I18n.t("devise.sessions.signed_in")}"}
end

Given /^I register$/ do
  step %Q{I fill in "#{I18n.t("simple_form.labels.user.name")}" with "José da Silva"}
  step %Q{I fill in "Email" with "jose@polomarte.com"}
	step %Q{I fill in "#{I18n.t("simple_form.labels.user.password")}" with "123456"}
	step %Q{I fill in "#{I18n.t("simple_form.labels.user.password_confirmation")}" with "123456"}
	step %Q{I fill in "#{I18n.t("simple_form.labels.user.city")}" with "Amsterdam"}
	step %Q{I select "Netherlands" from "#{I18n.t("simple_form.labels.user.country")}"}
	step %Q{I press "#{I18n.t("sign_up")}"}
  step %Q{I should see "#{I18n.t("devise.registrations.signed_up")}"}
end

Given /^I have already registered$/ do
  @user = FactoryGirl.create :user
end

Given /^I sign in$/ do
  step %Q{I fill in "Email" with "#{@user.email}"}
  step %Q{I fill in "#{I18n.t("simple_form.labels.user.password")}" with "#{@user.password}"}
  step %Q{I press "#{I18n.t(:sign_in)}"}
  step %Q{I should see "#{I18n.t("devise.sessions.signed_in")}"}
end

Given /^I confirm my account$/ do
  @user.confirm!
end

Given /^I have accepted buyer terms$/ do
  @user.accept_buyer_terms!
end


