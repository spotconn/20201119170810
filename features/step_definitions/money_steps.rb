Given /^I have money$/ do
  @user ||= User.find_by_email("jose@polomarte.com")
  FactoryGirl.create :payment, user: @user, amount: 1000
end