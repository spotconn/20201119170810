Given /^Someone creates a product$/ do
  @another_user = FactoryGirl.create :another_user
  @another_user.confirm!
  @another_user.accept_seller_terms!
  @product = FactoryGirl.create :product, user: @another_user  
end
