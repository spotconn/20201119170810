Feature: Purchasing
  # Scenario: Purchase Product - First Time Purchase - Not Registered
  #   Given Someone creates a product
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I follow "Sign up"
  #   And I register
  #   And I have money
  #   And I press "Accept"
  #   And I press "Confirm"
  #   Then I should see "Order completed!"
  #   
  # Scenario: Purchase Product - First Time Purchase - Existing user - Logged Off
  #   Given Someone creates a product
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I have already registered
  #   And I have money
  #   And I sign in
  #   And I press "Accept"
  #   And I press "Confirm"
  #   Then I should see "Order completed!"
  # 
  # Scenario: Purchase Product - Second Purchase - Existing user - Logged Off
  #   Given Someone creates a product
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I have already registered
  #   And I have money
  #   And I have accepted buyer terms
  #   And I sign in
  #   And I press "Confirm"
  #   Then I should see "Order completed!"
  # 
  # Scenario: Purchase Product - First Time Purchase - Logged On
  #   Given Someone creates a product
  #   And I am logged in as a user
  #   And I have money
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I press "Accept"
  #   And I press "Confirm"
  #   Then I should see "Order completed!"
  #   
  # Scenario: Purchase Product - Second Time Purchase - Logged On
  #   Given Someone creates a product
  #   And I am logged in as a user
  #   And I have money
  #   And I have accepted buyer terms
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I press "Confirm"
  #   Then I should see "Order completed!"
  #   
  # Scenario: Purchase Product - First Time Purchase - Logged On - Decline Buyer Terms
  #   Given Someone creates a product
  #   And I am logged in as a user
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I press "Decline"
  #   Then I should see "Buyer terms declined"
  # 
  # Scenario: Purchase Product - Second Time Purchase - Logged On - Cancel Order
  #   Given Someone creates a product
  #   And I am logged in as a user
  #   And I have accepted buyer terms
  #   And I go to products page
  #   And I follow "Purchase"
  #   And I press "Cancel"
  #   Then I should see "Order cancelled!"