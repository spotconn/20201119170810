require 'spork'

Spork.prefork do
  require 'cucumber/rails'
  require 'webmock/cucumber'
  require 'factory_girl'
  require 'sunspot_test/cucumber'
  require 'thin'

  WebMock.disable_net_connect! allow_localhost: true
  Capybara.default_selector = :css
  ActionController::Base.allow_rescue = false
  Cucumber::Rails::Database.javascript_strategy = :truncation
  SunspotTest.solr_startup_timeout = 60
  if ENV["SELENIUM"]
    Capybara.javascript_driver = elenium
  else
    Capybara.javascript_driver = :webkit
  end

  Before do
    DatabaseCleaner.start
  end

  After do |scenario|
    DatabaseCleaner.clean
  end

  module Thin::Logging
    def log_error(e=$!)
      STDERR.print "#{e}\n\t" + e.backtrace.join("\n\t")
    end
  end

  Capybara.server do |app, port|
    require 'rack/handler/thin'
    Rack::Handler::Thin.run(app, ort => port)
  end
end

Spork.each_run do
  Dir[File.dirname(__FILE__) + '/../../spec/support/factories/*.rb'].each { |f| require f }
end