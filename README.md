# O Ritmo

## Instalação do ambiente

	bundle install
	sudo apt-get install imagemagick redis-server sox libsox-dev

## Execução do ambiente
É importante seguir estes passos tanto no ambiente de desenvolvimento quanto no de testes.

	foreman start # Inicia o servidor e o Solr

## Usando o S3
Adicionar chaves ao HTTP Header que o S3 responde:

    # Adiciona a chave Content-Disposition
    product.archive_url(:query => {"response-content-disposition" => "attachment; filename=\"#{filename}\""})

## Observação sobre retirada/withdraw via PayPal
A transfer para o Ricardo Imperatore teve que ser feita diretamente pelo dashboard do PayPal.
Foi usado o código abaixo (extraído de `PaymentsController`) para tentar fazer diretamente pelo Rails.
Porém `transfer_response.success?` retornou `false`, e a mensagem
`We're sorry, but your PayPal account isn't currently eligible to send mass payments`.

Tentamos ativar a opção `Mass Payments` no dashboard do PayPal, conforme a documentação.
Porém não temos um app `REST API` ainda. __TODO__

Links úteis:
[https://developer.paypal.com/developer/applications]()
[https://www.paypal-apps.com/user/my-account/applications]()

    user = User.find_by_paypal_account('ricardoimperatore@botecoeletro.com.br')

    return if user.paypal_account.blank?
    return if user.account.balance <= 0

    payment        = user.payments.new
    payment.status = 'N'
    payment.amount = user.account.balance

    if payment.save!
      gateway = ActiveMerchant::Billing::PaypalExpressGateway.new(PAYPAL_OPTIONS)
      transfer_response = gateway.transfer(payment.amount * 100, user.paypal_account, subject: 'O Ritmo Withdrawal', note: 'Thank you for your business')

      payment.merchant_transaction_id = transfer_response.params['correlation_id']
      payment.merchant_data = transfer_response.params
      payment.status = 'A'

      if transfer_response.success?
        payment.amount_paid = -1 * payment.amount
        payment.save
      else
        payment.status = 'D'
        payment.save
      end
    end


